#!/usr/bin/env pythoh3

"""Programa para ordenar enteros que se reciben como argumentos"""

import sys

def find_lower(numbers: list, pivot: int) -> int:
    lower: int = pivot
    for pos in range(pivot, len(numbers)):
        if numbers[pos] < numbers [lower]:
            lower = pos
    return lower

def main():
    numbers: list = sys.argv[1:]
    for pivot_pos in range(len(numbers)):
        lower_pos: int = find_lower(numbers, pivot_pos)
        if lower_pos != pivot_pos:
            numbers[pivot_pos], numbers[lower_pos] = numbers[lower_pos], numbers[pivot_pos]
    for number in numbers:
        print(number, end=" ")
    print()

if __name__ == '__main__':
    main()
