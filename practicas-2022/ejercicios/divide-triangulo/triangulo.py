#!/usr/bin/env pythoh3

'''
Program to produce a triangle with numbers
'''

import sys

def line(number: int):
    """Return a string corresponding to the line for number"""

    text = str(number) * number
    return text

def triangle(number: int):
    """Return a string corresponding to the triangle for number"""

    if number > 9:
        raise ValueError
    text = ""
    for count in range(1, number+1):
        text = text + line(count) + '\n'
    return text

def main():
    number: int = sys.argv[1]
    try:
        text = triangle(int(number))
    except ValueError:
        text = triangle(9)
    print(text, end="")

if __name__ == '__main__':
    main()
