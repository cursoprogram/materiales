#!/usr/bin/env pythoh3

'''
Maneja un diccionario con cantidades
'''

import sys

items = {}

def op_add():
    item = sys.argv.pop(0)
    quantity = int(sys.argv.pop(0))
    items[item] = quantity

def op_items():
    print("Items:", end='')
    for item in items:
        print(f" {item}", end='')
    print()

def op_all():
    print("All:", end='')
    for item, q in items.items():
        print(f" {item} ({q})", end='')
    print()

def op_sum():
    sum = 0
    for q in items.values():
        sum = sum + q
    print(f"Sum: {sum}")

def main():
    sys.argv.pop(0)
    while len(sys.argv) > 0:
        command = sys.argv.pop(0)
        if command == 'add':
            op_add()
        elif command == 'items':
            op_items()
        elif command == 'all':
            op_all()
        elif command == 'sum':
            op_sum()

if __name__ == '__main__':
    main()