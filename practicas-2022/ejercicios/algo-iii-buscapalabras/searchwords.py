#!/usr/bin/env pythoh3

'''
Busca una palabra en una lista de palabras
'''

import sys

import sortwords

def search_word(word, words_list):

    left_pivot = 0
    right_pivot = len(words_list) - 1
    found = False

    while not found:
        test_index = (right_pivot + left_pivot) // 2
        test_word = words_list[test_index]
        if sortwords.equal(word, test_word):
            found = True
        elif sortwords.is_lower(word, test_word):
            right_pivot = test_index - 1
        else:
            left_pivot = test_index + 1
        if left_pivot > right_pivot:
            raise Exception("Word not found")
    return test_index

def main():
    if len(sys.argv) < 3:
        sys.exit(f"Usage: {sys.argv[0]} <word_to_search> <word> [<word>]")
    word = sys.argv[1]
    words_list = sys.argv[2:]
    ordered_list = sortwords.sort(words_list)
    try:
        position = search_word(word, ordered_list)
    except:
        sys.exit("Word not found")
    sortwords.show(ordered_list)
    print(position)

if __name__ == '__main__':
    main()