## Arte ASCII: texto

Antes de los emoticones y los memes hubo arte ASCII. Muestra imágenes de una línea o texto en formato cartel al estilo [ASCII Art](https://en.wikipedia.org/wiki/ASCII_art): [artdemo.py](arte-ascii/artdemo.py).

* Dependencias: [art](https://github.com/sepandhaghighi/art#1-line-art)

```commandline
pip install art
```

* Listado de fonts disponibles:

```commandline
python -m art fonts
```
