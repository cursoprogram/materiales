## Asistentes de IA para PyCharm

Hay varios plugins para PyCharm que proporcionan un asistente virtual. Por ejemplo, tenemos:

* [Cody](https://docs.sourcegraph.com/cody) ([plugin para PyCharm](https://plugins.jetbrains.com/plugin/9682-sourcegraph-cody--code-search/)). Requiere cuenta en [Sourcegraph](https://sourcegraph.com), y entrar en ella una vez instalado el plugin (seguir instrucciones). Una vez instalado, aparecen nuevos opciones en los menús contextuales, una nueva vista "Cody" (en la barra de la izquierda), y sugerencias cada vez que se escribe código.

* [Codeium](https://codeium.com/chat) ([plugin para PyCharm](https://plugins.jetbrains.com/plugin/20540-codeium-ai-autocomplete-and-chat-for-python-js-ts-java-go-), [tutorial](https://codeium.com/jetbrains_tutorial)). Requiere cuenta en [Codeium](https://codeium.com).  Cuando se instala el plugin, aparece un mensaje en la esquina baja derecha, solicitando entrar en la cuenta para terminar la configuración. El chat está accesible en una nueva entrada en la barra de la derecha. También incluye nuevos menús contextuales en el código.
