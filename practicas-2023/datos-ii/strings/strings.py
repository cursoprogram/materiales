#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Tratamiento de Strings en Python"""

character = 'a' #Cadena longitud 1
print(type(character))
print(character)

valor = 10
print(type(valor))
character = str(valor)
print(type(character))

valor2 = 'hola'
#valor3 = int(valor2)
print(valor2)
#print(type(valor3))

s = "esto es una comilla \" de ejemplo"
print(s)

s = "Primer linea\nSegunda linea"
print(s)
#Primer linea
#Segunda linea

print("\112\110 \65 ") #JH  imprime ASCI

print(r"\110\110") #\110\110  raw string r
print("""La siguiente
cadena ocupa
varias lineas""")

##Formateando cadenas

#Nótese que str() convierte en string
x = 5
s = "El número es: " + str(x)
print(s) #El número es: 5

x = 5
s = "El número es: %d" % x
print(s) #El número es: 5

s = "Los números son {} y {}".format(5, 10)
print(s) #Los números son {} y {}".format(5, 10)

s = "Los números son {a} y {b}".format(a=5, b=10)
print(s) #Los números son 5 y 10

a = 5; b = 10
s = f"Los números son {a} y {b}"
print(s) #Los números son 5 y 10

a = 5; b = 10
s = f"a + b = {a+b}"
print(s) #a + b = 15

def funcion():
    return 20
s = f"El resultado de la función es {funcion()}"
print(s) #El resultado de la funcion es 20

# Sumando Strings
s1 = "Parte 1"
s2 = "Parte 2"
print(s1 + " " + s2) #Parte 1 Parte 2

s = "Hola "
print(s*3) #Hola Hola Hola

print("mola" in "Python mola") #True

print(chr(8364)) #€
print(ord("€"))  #110

print(len("Esta es mi cadena"))

x = str(10.4)
print(x)       #10.4
print(type(x)) #<class 'str'>

x = "abcde"
print(x[0])  #a
print(x[-1]) #e
x = "abcde"
print(x[0:2])

x = "abcdefghijk"
print(x[0::2]) #ace

# Métodos

print("\n ** Métodos String ** \n")

s = "mi cadena"
print(s.capitalize()) #Mi cadena

s = "MI CADENA"
print(s.lower()) #mi cadena

# Método upper() convierte todos los caracteres alfabéticos en mayúsculas.
s = "mi cadena"
print(s.upper())

s = "el bello cuello "
print(s.count("llo")) #2

# Caracteres alfanuméricos
s = "correo@dominio.com"
print(s.isalnum())
# Método strip() elimina a la izquierda y derecha el carácter
# que se le introduce. Si se llama sin parámetros elimina los espacios.
# Muy útil para limpiar cadenas.
s = "  abc  "
print(s)
print(s.strip()) #abc

#Método zfill() rellena la cadena con ceros a la izquierda hasta llegar
# a la longitud pasada como parámetro.
s = "123789"
print(s)
print(s.zfill(10)) #00123

# Devuelve la primera cadena unida a cada uno de los elementos de la lista
s = " y ".join(["1", "2", "3"])
print(s) #1 y 2 y 3

# Método split() divide una cadena en subcadenas y las devuelve
# almacenadas en una lista.
s = "Python,Java,C"
print(s.split(",")) #['Python', 'Java', 'C']

# Convierte caracteres a lista
input = 'ABC'
print(input)
chars = list(input)
print(chars)  # ['A', 'B', 'C']

# Comprension de listas (lists comprenhension)
input = 'ABC'

chars = [c for c in input]
print(chars)  # ['A', 'B', 'C']

# Divides strings según un delimitador
input = 'A,B,C'
delim = ','

x = input.split(delim)
print(x)  # ['A', 'B', 'C']


# Separa un string por delimitador devoviendo una lista
input2 = 'sergio@urjc.es'
delim2 = '@'
correo = input2.split(delim2)
print(correo)




