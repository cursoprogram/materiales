## Inventario de una tienda

Crea un programa que gestione el inventario de una tienda, usando un diccionario. En este diccionario, la cla ve será el código de cada producto (un string). Deberá guardar para cada código de producto el nombre del mismo, su precio, y cantidad disponible.

Deberás implementar las siguientes funcionalidades:

* Insertar los datos en el programa. Pedirá los datos de un producto (código, nombre, precio y cantidad) al usuario, y los almacenará en el diccionario de inventario.
* Listar todos los productos. Mostrará en pantalla todos los productos del diccionario de inventario junto con su nombre, precio y cantidad. 
* Consultar un producto dada su clave. Mostrará su nombre, precio y cantidad. 
* Mostrar todos los productos que cuya cantidad sea cero.

Al arrancar, el programa mostrará un menú que permitirá elegir entre las cuatro funcionalidades, o terminar, eligiendo un número (por ejemplo, 1 para insertar datos, 2 para listar, etc.)

Por ejemplo, la siguiente podría ser una ejecución completa:

```
1. Insertar un artículo
2. Listar artículos
3. Consultar artículo
4. Artículos agotados
0. Salir
Opción: 1
Código de artículo: ZAPATOS01
Nombre: Zapatos de gamuza azul
Precio: 67.95
Cantidad: 10
1. Insertar un artículo
2. Listar artículos
3. Consultar artículo
4. Artículos agotados
0. Salir
Opción: 2
ZAPATOS01: Zapatos de gamuza azul, precio: 67.95, cantidad: 10
1. Insertar un artículo
2. Listar artículos
3. Consultar artículo
4. Artículos agotados
0. Salir
Opción: 1
Código de artículo: BOTAS02
Nombre: Botas camperas
Precio: 89.95
Cantidad: 4
1. Insertar un artículo
2. Listar artículos
3. Consultar artículo
4. Artículos agotados
0. Salir
Opción: 2
ZAPATOS01: Zapatos de gamuza azul, precio: 67.95, cantidad: 10
BOTAS02: Botas camperas, precio: 89.95, cantidad: 4
1. Insertar un artículo
2. Listar artículos
3. Consultar artículo
4. Artículos agotados
0. Salir
Opción: 3
Código del artículo a consultar: ZAPATOS01
ZAPATOS01: Zapatos de gamuza azul, precio: 67.95, cantidad: 10
1. Insertar un artículo
2. Listar artículos
3. Consultar artículo
4. Artículos agotados
0. Salir
Opción: 4
1. Insertar un artículo
2. Listar artículos
3. Consultar artículo
4. Artículos agotados
0. Salir
Opción: 1
Código de artículo: ZAPAS06
Nombre: Zapatillas deportivas
Precio: 120.00
Cantidad: 0
1. Insertar un artículo
2. Listar artículos
3. Consultar artículo
4. Artículos agotados
0. Salir
Opción: 4
El articulo ZAPAS06 se ha agotado
1. Insertar un artículo
2. Listar artículos
3. Consultar artículo
4. Artículos agotados
0. Salir
Opción: 0
```

Para escribir tu programa, copia el contenido de `plantilla.py` en `inventario.py`, y completa luego `inventario.py` hasta que funcione como se especifica en este enunciado.

Recuerda que el programa tendrás que entregarlo en un repositorio de acceso público o interno (no privado), creado bifurcando (forking) el repositorio de plantilla de este ejercicio.

En este ejercicio, para subir tu programa al repositorio usaremos `git`. El proceso resumido para hacerlo es el mismo que hemos seguido en ejercicios anteriores: bifurca (fork) el repositorio plantilla, creando así el repositorio de entrega. Clónalo, copia el código de `plantilla.py` a  `inventario.py`, y modifica su contenido hasta que el programa funcione como debe, haciendo commits según vayas avanzando. Sube esos commits (push) al repositorio frecuéntemente. La versión que se evaluará será la que haya en el repositorio de entrega 

Al terminar, comprueba en EIF GitLab que el fichero `inventario.py` tiene el contenido que debe tener. No olvides configurar el acceso a tu repositorio en EIF GitLab como "público" o "interno", para que podamos recoger la práctica.

Para esta práctica, hemos preparado algunos tests, que te permitirán saber si se ejecutan bien los casos que esos tests prueban. Para ejecutar los tests, desde PyCharm ejecuta el fichero `tests/test_inventario.py`, o desde la línea de comandos:

```commandline
python3 -m unittest tests/test_inventario.py
```
