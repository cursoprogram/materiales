## Clasificador de cadenas de texto

Desarrolla un programa que permita ingresar una cadena (un string) y evalúe si es un correo electrónico, un entero, un real u otra cosa.

```commandline
python3 strings.py correo@gmail.com
Es un correo electrónico
```

El programa se llamará `clasificador.py`, y tendrá la estructura que se presenta en el fichero `plantilla.py` del repositorio plantilla:


* La función `es_correo_electronico()` recibirá un string y devolverá `True` si es un correo electrónico (bastará detectar que la cadena tiene un carácter `@`, que hay caracteres a su izquierda y a su derecha, y que entre los que hay a  su derecha hay al menos un `.`) y `False` si no lo es.
* La función `es_entero()` recibirá un string y devolverá `True` o `False` según sea un entero o no.
* La función `es_real()` recibirá un string y devolverá `True` o `False` según sea un real (`float`) o no.
* La función `evaluar_entrada()` recibirá un string. Si el string es una cadena vacía devolverá `None` y mostrará un mensaje. Si la cadena no es vacía irá llamando (en este orden) a las funciones: `es_correo_electronico()`, `es_entero()`, `es_real()`, y retornará` un mensaje apropiado según el resultado de la evaluación.
* La función `main()` recibirá un string como argumentos en la  línea de comandos, y escribirá en pantalla según corresponda, si es un correo electrónico, un entero, un real, ninguno de los anteriores, o no se ha ingresado nada, llamando a la función `evaluar_entrada()` y mostrando el valor que devuelva.

Como verás en el fichero `plantilla.py` el código está estructurado según esas funciones. El "programa principal" (el código que va después de `if __name__ ...`) servirá simplemente para llamar a `main()`, que hará todo el trabajo. Para empezar a escribir tu programa, copia el contenido de `plantilla.py` en `clasificador.py`, y completa luego `clasificador.py` hasta que funcione como se especifica en este enunciado. El fichero `plantilla.py` no tiene que estar en el repositorio en el momento de la entrega de la práctica.

Ejemplos de ejecución:

```commandline
$ pythonn3 clasificador.py 
Error: se espera al menos un argumento
$ python3 clasificador.py ""
No ha ingresado un ningún string
None
$ python3 clasificador.py 3
Es un entero.
$ python3 clasificador.py 3.1
Es un número real.
$ python3 clasificador.py maria@sitio.org
Es un correo electrónico.
r$ python3 clasificador.py maria
No es ni un correo, ni un entero, ni un número real.
$ python3 clasificador.py maria@sitio
No es ni un correo, ni un entero, ni un número real.
```

Recuerda que el programa tendrás que entregarlo en un repositorio de acceso público o interno (no privado), creado bifurcando (forking) el repositorio de plantilla de este ejercicio.

En este ejercicio, para subir tu programa al repositorio usaremos `git`. El proceso resumido para hacerlo es el siguiente:

* Cuando hayas creado el repo bifurcado (fork) del repositorio plantilla, consigue su URL HTTPS de clonado, usando el botón "Clone" del repositorio en EIF GitLab.
* Utiliza, en tu ordenador, esa url para clonar el repositorio en un directorio local, que será el proyecto de PyCharm donde realizarás el ejercicio.
* Copia el código de `plantilla.py` a  `clasificador.py`, y modifica su contenido hasta que el programa funcione como debe.
* Borra del repositorio el programa `plantilla.py` que no tendrá que estar en el repositorio de entrega.
* Crea al menos tres commits, durante este proceso, según vas haciendo cambios a tu programa.
* Sube los commits a tu repositorio en EIF GitLab.

Al terminar, comprueba en EIF GitLab que el fichero `clasificador.py` tiene el contenido que debe tener. No olvides configurar el acceso a tu repositorio en EIF GitLab como "público" o "interno", para que podamos recoger la práctica.

Para esta práctica, hemos preparado algunos tests, que te permitirán saber si se ejecutan bien los casos que esos tests prueban. Para ejecutar los tests, desde PyCharm ejecuta el fichero `tests/test_clasificador.py`, o desde la línea de comandos:

```commandline
python3 -m unittest tests/test_clasificador.py
```

