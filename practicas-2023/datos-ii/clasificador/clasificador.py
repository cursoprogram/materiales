#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Desarrolla un programa que permita ingresar una cadena
(un string) y evalúe si es un correo electrónico, un entero,
un real u otra cosa.
"""

import sys


def es_correo_electronico(string):
    # Divide la cadena en dos partes en el símbolo '@' si existe
    partes = string.split('@')
    if len(partes) == 2:
        usuario, dominio = partes
        if usuario and dominio:
            # Verifica que el dominio contiene al menos un punto
            if '.' in dominio:
                return True
    return False


def es_entero(string):
    try:
        int(string)  # Intenta convertir la entrada a un entero.
        return True
    except ValueError:
        return False


def es_real(entrada):
    try:
        float(entrada)  # Intenta convertir la entrada a un número real.
        return True
    except ValueError:
        return False


def evaluar_entrada(string):
    if not (string):  # si no ingresa ningún string devuelve un mensaje y termina el programa
        print("No ha ingresado un ningún string")
        return None

    elif es_correo_electronico(string):
        return "Es un correo electrónico."
    elif es_entero(string):
        return "Es un entero."
    elif es_real(string):
        return "Es un número real."
    else:
        return "No es ni un correo, ni un entero, ni un número real."


def main():
    if len(sys.argv) < 2:
        sys.exit("Error: se espera al menos un argumento")
    string = sys.argv[1]
    resultado = evaluar_entrada(string)
    print(resultado)


if __name__ == '__main__':
    main()
