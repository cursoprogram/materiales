#!/usr/bin/python3
# -*- coding: utf-8 -*-

def es_float(string):
    """Recibe una cadena y devuelva True si puede ser convertido a float"""
    try:
        float(string)
        return True
    except ValueError:
        return False

def main():
    resultado = es_float(input("Ingrese una cadena: "))

    if resultado:
        print("La cadena puede convertirse a float")
    else:
        print("La cadena no puede convertirse a float")

if __name__ == '__main__':
    main()
