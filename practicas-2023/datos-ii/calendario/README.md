## Calendario

Crea un programa que gestione un calendario, usando un diccionario. En este diccionario, la cla ve será una fecha. El valor correspondiente a esa fecha serán las actividades que tienen lugar esa fecha. Para cada actividad se almacenará la hora y el nombre de la actividad.

Las fechas se almacenarán en el formato "aaaa-mm-dd". Por ejemplo, `2023-11-22`. El año de la fecha estará entre `0000` y `2100` (ambos incluidos). El mes entre `01` y `12`. El día entre `01` y`31` (no se pide comprobar que algunos meses tienen 28 o 30 días).

Las horas se almacenarán en le formato "hh:mm". Por ejemplo `14:34`. La hora podrá estar entre `00` y `23`. 

Las actividades serán cadenas de texto (strings).

Deberás implementar las siguientes funcionalidades:

* Introducir una nueva actividad
* Listar todas las actividades disponibles
* Mostrar el día más ocupado
* Mostrar todas las actividades que tienen lugar a una hora dada

Al arrancar, el programa mostrará un menú que permitirá elegir entre las cuatro funcionalidades, o terminar, eligiendo una letra (de la A a la D para las funcionalidades anteriores, X para terminar). El menú admitirá que se elija con letras mayúsculas o minúsculas.

Siempre que se muestren listados de actividades (bien sea para mostrar todas las actividades, o las de una cierta hora) se mostrarán en orden, empezando por la más antigua hasta la más moderna.

Por ejemplo, la siguiente podría ser una ejecución completa:

```
A. Introduce actividad
B. Lista todas las actividades
C. Día más ocupado
D. Lista de las actividades de cierta hora dada
X. Terminar
Opción: b
A. Introduce actividad
B. Lista todas las actividades
C. Día más ocupado
D. Lista de las actividades de cierta hora dada
X. Terminar
Opción: A
Fecha: 2023-10-11
Hora: 9:00
Fecha: 2023-10-11
Hora: 09:00
Actividad: Desayuno
A. Introduce actividad
B. Lista todas las actividades
C. Día más ocupado
D. Lista de las actividades de cierta hora dada
X. Terminar
Opción: B
2023-10-11. 09:00: Desayuno
A. Introduce actividad
B. Lista todas las actividades
C. Día más ocupado
D. Lista de las actividades de cierta hora dada
X. Terminar
Opción: a
Fecha: 2023-11-22
Hora: 9:00
Fecha: 2023-11-22
Hora: 09:00
Actividad: Clase
A. Introduce actividad
B. Lista todas las actividades
C. Día más ocupado
D. Lista de las actividades de cierta hora dada
X. Terminar
Opción: a
Fecha: 2023-11-22
Hora: 11:00
Actividad: Otra clase
A. Introduce actividad
B. Lista todas las actividades
C. Día más ocupado
D. Lista de las actividades de cierta hora dada
X. Terminar
Opción: b
2023-10-11. 09:00: Desayuno
2023-11-22. 09:00: Clase
2023-11-22. 11:00: Otra clase
A. Introduce actividad
B. Lista todas las actividades
C. Día más ocupado
D. Lista de las actividades de cierta hora dada
X. Terminar
Opción: c
El día más ocupado es el 2023-11-22, con 2 actividad(es).
A. Introduce actividad
B. Lista todas las actividades
C. Día más ocupado
D. Lista de las actividades de cierta hora dada
X. Terminar
Opción: x
```

Otra ejecución posible:

```commandline
A. Introduce actividad
B. Lista todas las actividades
C. Día más ocupado
D. Lista de las actividades de cierta hora dada
X. Terminar
Opción: a
Fecha: 2023-11-22
Hora: 09:00
Actividad: Clase
A. Introduce actividad
B. Lista todas las actividades
C. Día más ocupado
D. Lista de las actividades de cierta hora dada
X. Terminar
Opción: a
Fecha: 2023-11-01
Hora: 09:00
Actividad: Desayuno
A. Introduce actividad
B. Lista todas las actividades
C. Día más ocupado
D. Lista de las actividades de cierta hora dada
X. Terminar
Opción: a
Fecha: 2023-11-22
Hora: 11:00
Actividad: Café
A. Introduce actividad
B. Lista todas las actividades
C. Día más ocupado
D. Lista de las actividades de cierta hora dada
X. Terminar
Opción: d
Hora: 09:00
2023-11-01. 09:00: Desayuno
2023-11-22. 09:00: Clase
A. Introduce actividad
B. Lista todas las actividades
C. Día más ocupado
D. Lista de las actividades de cierta hora dada
X. Terminar
Opción: x
```

Para escribir tu programa, copia el contenido de `plantilla.py` en `calendario.py`, y completa luego `calendario.py` hasta que funcione como se especifica en este enunciado.

Recuerda que el programa tendrás que entregarlo en un repositorio de acceso público o interno (no privado), creado bifurcando (forking) el repositorio de plantilla de este ejercicio.

En este ejercicio, para subir tu programa al repositorio usaremos `git`. El proceso resumido para hacerlo es el mismo que hemos seguido en ejercicios anteriores: bifurca (fork) el repositorio plantilla, creando así el repositorio de entrega. Clónalo, copia el código de `plantilla.py` a  `calendario.py`, y modifica su contenido hasta que el programa funcione como debe, haciendo commits según vayas avanzando. Sube esos commits (push) al repositorio frecuéntemente. La versión que se evaluará será la que haya en el repositorio de entrega 

Al terminar, comprueba en EIF GitLab que el fichero `calendario.py` tiene el contenido que debe tener. No olvides configurar el acceso a tu repositorio en EIF GitLab como "público" o "interno", para que podamos recoger la práctica.

Para esta práctica, hemos preparado algunos tests, que te permitirán saber si se ejecutan bien los casos que esos tests prueban. Para ejecutar los tests, desde PyCharm ejecuta el fichero `tests/test_calendario.py`, o desde la línea de comandos:

```commandline
python3 -m unittest tests/test_calendario.py
```
