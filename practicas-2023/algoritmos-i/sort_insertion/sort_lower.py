import sys

def find_lower(numbers: list, pivot: int) -> int:
    lower: int = pivot
    for pos in range(pivot, len(numbers)):
        if numbers[pos] < numbers[lower]:
            lower = pos
    return lower

def print_lista(numbers):
    for number in numbers:
        print(number, end=" ")
    print()

def main():
    numbers: list = sys.argv[1:]
    for pivot_pos in range(len(numbers)):
        # Utiliza la función find_lower para encontrar el índice del número más pequeño
        lowest_index = find_lower(numbers, pivot_pos)

        # Intercambia los números
        numbers[pivot_pos], numbers[lowest_index] = numbers[lowest_index], numbers[pivot_pos]

    print_lista(numbers)

if __name__ == '__main__':
    main()
