### Ordenación de vector mediante inserción

Realiza un programa, que se llame `sort_ins.py`, que ordene de manera ascendente una lista o un vector de n posiciones que se le proporcionen como argumento en la línea de comandos, igual que en el ejercicio anterior, pero usando el método de inserción. Por ejemplo:

```commandline
python3 sort_ins.py 9 5 7 1 3 2
1 2 3 5 7 9
```


**Solución:** [sort_ins.py](sort_ins.py)


