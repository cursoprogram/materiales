Vamos a calcular el número óptimo de árboles, de forma que produzcan la mayor cantidad
de fruta posible. Para ello, consideremos un campo con árboles frutales. Si hay `base_tree`
árboles, producen `fruit_per_tree` kilos de fruta cada uno.
Por cada árbol más que se planta, la producción de cada árbol en el campo
se reduce en `reduction` kilos de fruta.
Calcular el número óptimo de árboles entre dos números dados (`min` y `max`).

Realiza un programa, que se llame `aprox.py` que resuelva este problema. Para ello,
utiliza la plantilla `plantilla.py` que encontrarás en el repositorio de plantilla,
y ten en cuenta el programa `aprox_simple.py` que es una versión simplificada de lo
que tienes que hacer.

El programa `aprox.py` se llamará de la siguiente forma:

```commandline
python3 aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>
```

Por ejemplo, si se le llama así:

```commandline
python3 aprox.py 25 400 10 27 34 
```

producirá como resultado:

```commandline
27 10260
28 10360
29 10440
30 10500
31 10540
32 10560
33 10560
34 10540
Best production: 10560, for 32 trees
```

Además, el programa comprobará que ha sido llamado con el número adecuado de argumentos,
y si no fuera así el programa terminará y mostrará el mensaje:

```commandline
Usage: aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>
```

También comprobará que todos los argumentos que se le pasen son números enteros.
Si alguno no lo es el programa terminará y mostrará el mensaje:

```commandline
All arguments must be integers
```

Ten en cuenta que, según se indica en la plantilla, el programa principal no
hará nada salvo llamar a la función `main`. Esta llamará primero a 
'read_arguments' para obtener los argumentos de la línea de comandos, ya
como números enteros, y luego a `compute_all` para obtener una lista donde
cada elemento será una tupla (número de árboles, producción). `main` terminará
imprimiendo los valores de esa lista, calculando la mejor producción, y
mostrando el mensaje final con la mejor producción.

`compute_all`, por lo tanto, recibirá como argumento el número de árboles
mínimo y máximo, y calculará las producciones para cualquier número de árboles
entre ese mínimo y ese máximo, devolviendo la lista de tuplas que se menciona
anteriormente. Para hacer su trabajo, `compute_all` llamará a `compute_trees`
cuando le haga falta calcular la producción para un número de árboles.

Es importante notar que para que el código funcione como está en la plantilla,
las variables `base_trees`, `fruit_per_tree` y `reduction` serán variables
globales, por lo que tendrás que usar la palabra `global`, de forma conveniente,
en `main`.

Recuerda que el programa tendrás que entregarlo en un repositorio de acceso público
o interno (no privado), creado bifurcando (forking) el repositorio de
plantilla de este ejercicio. La entrega de la práctica se hará subiendo
commits a ese repositorio git, como hemos hecho en prácticas anteriores.

Al terminar, comprueba en ETSIT GitLab que está el fichero con tu solución.

Para esta práctica, hemos preparado algunos tests, que te permitirán saber si se ejecutan bien los casos que esos tests prueban. Para ejecutar los tests, desde PyCharm ejecuta el fichero `tests/test_aprox.py`, o desde la línea de comandos:

```commandline
python3 -m unittest tests/test_aprox.py
```

En el repositorio plantilla hay además un fichero `.gitlab-ci.yml`, que hará que el sistema CI (Continuous Integration) de GitLab ejecute automáticamente los tests cada vez que se suba un nuevo commit. Puedes ver el resultado en la opción correspondiente del menú vertical de GitLab (cuando estés viendo tu repositorio).
