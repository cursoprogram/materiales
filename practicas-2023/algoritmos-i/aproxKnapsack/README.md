# Optimización Knapsack mediante búsqueda exhaustiva

Dada una mochila con una capacidad máxima de `max_weigh`, y un conjunto de n objetos, cada uno con un peso `weight` y un valor `value`, encontrar la combinación de objetos que maximiza el valor total y que no excede la capacidad de la mochila.

```commandline
python3 aprox_mochila.py 10 10 2 35 5 8 1 25 3
[(25, 3), (8, 1), (25, 3), (25, 3)]
```

Utiliza el algoritmo de aproximación de búsqueda exhaustiva.

Solución: [aprox_mochila.py](aprox_mochila.py)