import sys


def order_items(songs: list, i: int, gap: int) -> list:
    key = songs[i][0]
    value = songs[i][1]
    j = i
    while j >= gap and songs[j - gap][1] < value:
        songs[j] = songs[j - gap]
        j -= gap
        songs[j] = (key, value)
    return songs


def sort_music(songs: list) -> list:
    n = len(songs)
    gap = n // 2

    while gap > 0:
        for i in range(gap, n):
            songs = order_items(songs, i, gap)
        gap //= 2

    return songs


def create_dictionary(arguments: list) -> dict:
    songs: dict = {}
    i = 0
    while i < len(arguments):
        song_name = arguments[i]
        i += 1
        if i < len(arguments):
            try:
                reproductions = int(arguments[i])
                songs[song_name] = reproductions
            except ValueError:
                sys.exit(f"Error: El argumento '{arguments[i]}' no es un número de reproducciones válido.")
        else:
            sys.exit("Error: Debes proporcionar un número de reproducciones para cada canción.")
        i += 1
    return songs


def main():
    args = sys.argv[1:]

    if len(args) % 2 != 0 or len(args) == 0:
        sys.exit("Error: Debes proporcionar pares de canción y número de reproducciones.")

    songs: dict = create_dictionary(args)

    sorted_songs: list = sort_music(list(songs.items()))

    sorted_dict: dict = dict(sorted_songs)
    print(sorted_dict)


if __name__ == '__main__':
    main()
