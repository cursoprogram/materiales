### Ordenar de canciones por número de reproducciones empleando shell

Escribe un programa que ordene de mayor a menor un diccionario formado por el título de la canción y el número de reproducciones en Spotify, usando para la ordenación el número de reproducciones de cada canción.

El programa recibe pares de datos (título de la canción, número de reproducciones) como argumentos de línea de comandos, los cuales serán almacenados en un diccionario para posteriormente, ordenar el diccionario de canciones de mayor a menor según el número de reproducciones, para finalmente mostrar el diccionario resultante en la salida estándar.

El programa tendrá que usar el algoritmo de ordenación shell que hemos visto en clase, y no podrá usar funciones de ordenación proporcionadas por Python (como `sort()`) o por módulos de terceros.

Por ejemplo, una ejecución típica será como sigue:

```commandline
python3 sortmusic.py BohemianRhapsody 2274 Thunder 2153 Closer 2656 Memories 1833 Shallow 2229 Believer 2717 Sunflower 2980 DanceMonkey 2926

{'Sunflower': 2980, 'DanceMonkey': 2926, 'Believer': 2717, 'Closer': 2656, 'BohemianRhapsody': 2274, 'Shallow': 2229, 'Thunder': 2153, 'Memories': 1833}
```

**IMPORTANTE**: Fijate que si la canción son varias palabras, se escriben juntas. Recuerda que si hay un espacio es un nuevo argumento y esto generará problemas. Por ejemplo, si la canción es "Bohemian Rhapsody" y el número de reproducciones es 2274, el programa deberá almacenar el par ("BohemianRhapsody", 2274) en el diccionario.

El programa se llamará `sortmusic.py`, y tendrá la estructura que se presenta en el fichero `plantilla.py` del repositorio plantilla:

* La función `sort_music()` recibirá una lista de canciones y número de reproducciones como argumentos, y devolverá la lista ordenada.
* La función `create_dictionary()` recibirá la lista de argumentos y devolverá un diccionario con los pares de canciones y número de reproducciones.
* La función `order_items()` recibirá la lista de canciones, el gap actual y el índice para comparar y, devolverá la lista de canciones ordenadas. 
* La función `main()` recibirá conjunto de pares de canciones y número de reproducciones, como argumentos en la línea de comandos, y escribirá en pantalla el diccionario de canciones ordenadas.

Como verás en el fichero `plantilla.py`, en ese esquema, el código está estructurado en funciones y, la función principal para ordenar la lista (`sort_music()`) será llamada desde la función `main()`. El "programa principal" (el código que va después de `if __name__ ...`) servirá simplemente para llamar a `main()`, que hará todo el trabajo. Para empezar a escribir tu programa, copia el contenido de `plantilla.py` en `sortmusic.py`, y completes luego `sortmusic.py` hasta que funcione como se especifica en este enunciado. El fichero `plantilla.py` no tiene que estar en el repositorio en el momento de la entrega de la práctica.

Recuerda que el programa tendrás que entregarlo en un repositorio de acceso público o interno (no privado), creado bifurcando (forking) el repositorio de plantilla de este ejercicio.

En este ejercicio, para subir tu programa al repositorio usaremos `git`. El proceso resumido para hacerlo es el siguiente:

* Cuando hayas creado el repo bifurcado (fork) del repositorio plantilla, consigue su URL HTTPS de clonado, usando el botón "Clone" del repositorio en EIF GitLab.
* Utiliza, en tu ordenador, esa url para clonar el repositorio en un directorio local, que será el proyecto de PyCharm donde realizarás el ejercicio.
* Copia el código de `plantilla.py` a  `sortmusic.py`, y modifica su contenido hasta que el programa funcione como debe.
* Borra del repositorio el programa `plantilla.py` que no tendrá que estar en el repositorio de entrega.
* Crea al menos tres commits, durante este proceso, según vas haciendo cambios a tu programa.
* Sube los commits a tu repositorio en EIF GitLab.

Al terminar, comprueba en EIF GitLab que el fichero `sortmusic.py` tiene el contenido que debe tener. No olvides configurar el acceso a tu repositorio en EIF GitLab como "público" o "interno", para que podamos recoger la práctica.

Para esta práctica, hemos preparado algunos tests, que te permitirán saber si se ejecutan bien los casos que esos tests prueban. Para ejecutar los tests, desde PyCharm ejecuta el fichero `tests/test_sortmusic.py`, o desde la línea de comandos:

```commandline
python3 -m unittest tests/test_sortmusic.py
```

