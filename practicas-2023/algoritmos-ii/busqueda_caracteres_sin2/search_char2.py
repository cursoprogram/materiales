#!/usr/bin/env pythoh3

"""Programa para buscar un caracter en una palabra cuyos caracteres están desordenadas y que se reciben como
argumentos"""

import sys
import sortword
def search_char(char: str, word: str) -> int:
    left_pivot = 0
    right_pivot = len(word) - 1
    found = False
    test_index = -1

    while not found and left_pivot <= right_pivot:
        test_index = (right_pivot + left_pivot) // 2
        test_char = word[test_index].lower()
        if char == test_char:
            found = True
        elif char < test_char:
            right_pivot = test_index - 1
        else:
            left_pivot = test_index + 1

    if not found:
        test_index = -1
    return test_index


def main():
    char: str = sys.argv[1].lower()
    word: str = sys.argv[2]

    word_chars: list = list(word)

    sort_str: list = sortword.sorted_string(word_chars)

    sorted_word = ''.join(sort_str)
    print(sorted_word)

    index = search_char(char, sorted_word)
    if index == -1:
        print("Caracter no encontrado")
    else:
        print(index)


if __name__ == '__main__':
    main()
