#!/usr/bin/env pythoh3

"""Programa para buscar un entero entre un conjunto de enteros que se reciben como argumentos"""

import sys


def main():
    number: int = int(sys.argv[1])
    numbers: list = sys.argv[2:]
    left_pivot = 0
    right_pivot = len(numbers) - 1
    found = False
    test_index = -1

    while not found and left_pivot <= right_pivot:
        test_index = (right_pivot + left_pivot) // 2
        test_number = numbers[test_index]
        if number == test_number:
            found = True
        elif number < test_number:
            right_pivot = test_index - 1
        else:
            left_pivot = test_index + 1

    if not found:
        test_index = -1
    if test_index == -1:
        print("Numero no Encontrado")
    else:
        print(test_index)


if __name__ == '__main__':
    main()
