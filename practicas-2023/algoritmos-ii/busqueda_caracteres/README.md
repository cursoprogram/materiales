### Búsqueda binaria en cadena de caracteres ordenada

Realiza un programa que busque un carácter en una cadena de caracteres ordenada. Tanto el carácter como la cadena de caracteres (string) se le proporcionarán como argumentos en la línea de comandos. Tanto para la ordenación como para la búsqueda ignoraremos la diferencia entre letra minúscula y letra mayúscula. Por ejemplo:

```commandline
python3 searchchars.py f acDeFhz
4
```

Utiliza el mismo algoritmo de búsqueda binaria que en el ejercicio anterior.

<!--Solución: [searchchars.py](searchchars.py)-->
