### Búsqueda binaria en cadena de caracteres sin ordenar

Realiza un programa que busque un carácter en una cadena de caracteres sin ordenar. Tanto el carácter como la cadena de caracteres (string) se le proporcionarán como argumentos en la línea de comandos.  Tanto para la ordenación como para la búsqueda ignoraremos la diferencia entre letra minúscula y letra mayúscula. Por ejemplo:

```commandline
python3 searchchars2.py h HolaQueTal
3
```

En este ejemplo, la cadena, ordenada, queda como `aaeHlloQTu`.

Para resolver el ejercicio, primero ordenaremos la cadena (podemos utilizar el código de [sortchars.py](../../algoritmos-i/ordCharSeleccion/sortchars.py)), y luego aplicamos el mismo algoritmo de búsqueda binaria que en el ejercicio anterior.

Recuerda que las cadenas de caracteres (strings) son inmutables (no pueden cambiar). Por eso, convendrá que antes de ordenar los caracteres, obtengas una lista de caracteres a partir del string. Por ejemplo, si la cadena se llama `word`, puedes usar:

Para realizar la operación inversa (obtener una cadena de caracteres a partir de una lista de caracteres) puedes usar:

```python
word = ''.join(word_chars)
```

```python
word_chars = list(word)

```

<!--Solución: [searchchars2.py](searchchars2.py)-->
