#!/usr/bin/env pythoh3

"""Programa para buscar un caracter en una palabra cuyos caracteres están desordenadas y que se reciben como
argumentos"""

import sys


def search_minimum(word: str, current_pos: int, lower_pos: int) -> int:
    if word[current_pos].lower() < word[lower_pos].lower():
        lower_pos = current_pos
    return lower_pos


def find_lower(word: str, pivot: int) -> int:
    lower: int = pivot
    for pos in range(pivot, len(word)):
        lower = search_minimum(word, pos, lower)
    return lower


def sorted_string(word_chars: list) -> list:
    for pivot_pos in range(len(word_chars)):
        lower_pos: int = find_lower(word_chars, pivot_pos)
        if lower_pos != pivot_pos:
            word_chars[pivot_pos], word_chars[lower_pos] \
                = word_chars[lower_pos], word_chars[pivot_pos]

    return word_chars


def search_char(char: str, word: str) -> int:
    left_pivot = 0
    right_pivot = len(word) - 1
    found = False
    test_index = -1

    while not found and left_pivot <= right_pivot:
        test_index = (right_pivot + left_pivot) // 2
        test_char = word[test_index].lower()
        if char == test_char:
            found = True
        elif char < test_char:
            right_pivot = test_index - 1
        else:
            left_pivot = test_index + 1

    if not found:
        test_index = -1
    return test_index


def main():
    char: str = sys.argv[1].lower()
    word: str = sys.argv[2]

    word_chars: list = list(word)

    sort_str: list = sorted_string(word_chars)

    sorted_word = ''.join(sort_str)
    print(sorted_word)

    index = search_char(char, sorted_word)
    if index == -1:
        print("Caracter no encontrado")
    else:
        print(index)


if __name__ == '__main__':
    main()
