## Proyecto final (convocatoria de junio)

[ Enunciado final. Puede tener cambios si se detectan errores o hacen falta aclaraciones. ]

El proyecto final de la asignatura (convocatoria de junio) consiste en la creación de varios módulos de tratamiento digital de imágenes, y algunos programas que usan esos módulos para establecer cadenas de procesado digital.

**Atención:** Algunos requisitos son diferentes de los del proyecto final de la convocatoria de enero, lee este enunciado con cuidado.

**Atención:** Lee con atención las condiciones de entrega, al final de este enunciado. En particular, asegúrate de que comienzas realizando un fork del repositorio plantilla, clonando ese fork para trabajar en él, y que luego realizas commits frecuentemente.

El proyecto final está estructurada en:

* Requisitos mínimos. Cumplirlos es necesario para que el proyecto final sea considerado como aprobado, en cuyo caso seá puntuado con 2.0.
* Requisitos opcionales. Pueden ser puntuados con hasta 3.0 (adicionales a la puntuación de los requisitos mínimos).

El repositorio de plantilla de esta práctica tiene, entre otros ficheros:

* Módulo `imagenes.py`, que será el módulo básico necesario que importarán los módulos que necesiten leer imágenes, guardar imágenes, o crear imágenes vacías.

* Fichero `ejemplo.py`, que muestra un ejemplo muy sencillo de cómo crear un rectángulo de dos colores con `imagenes.py`.

* Directorio `tests`, con varios tests para comprobar parte de la funcionalidad mínima que se requiere.

* Fichero `requirements.txt` con las dependencias de los módulos, que habrá que instalar en un entorno virtual. Una vez el entorno virtual esté creado y activado, se podrá ejecutar con el comando:

```
pip install -r requirements.txt
```

### Introducción

Esta práctica consiste en la creación de un módulo llamado `transforms.py`, que contendrá las funciones necesarias para tratar imágenes digitales. Para construirlo, habrá que apoyarse en el módulo `images.py`, que se proporciona, y contiene las funciones necesarias para leer y guardar imágenes.

El módulo `images.py` representa imágenes como un diccionario, con tres claves:

* `width`: ancho de la imagen, en pixels
* `height`: alto de la imagen, en pixels
* `pixels`: lista de pixels de la imagen (longitud: `width` * `height`)

Cada pixel de la image será una tupla, con valores para cada componente de color RGB (rojo, verde, azul). Cada componente de color será un número entero entre 0 y 255 (la intensidad de ese color, 0 para ninguna, 255 para máxima). Así, un pixel rojo se  representará como `(255, 0, 0)` (máximo rojo, nada de verde, nada de azul).

Si en una imagen consideramos que la esquina superior izquierda tiene las coordenadas (x=0, y=0), y las coordenadas x crecen hacia la derecha, y las coordenadas y crecen hacia abajo, el índice de la lista de pixels que corresponde a unas coordenadas cualquiera `(x, y)` será `x + width * y`. Así, en la lista primero tendremos la fila superior de la imagen, luego la siguiente fila hacia abajo, y así sucesivamente hasta que al final de la lista tendremos la fila inferior de la imagen.

Por ejemplo, un cuadrado de dos por dos pixels, con los colores rojo (los dos pixels de arriba) y azul (los dos pixels de abajo) sería:

```python
{'width': 2,
 'height': 2,
 'pixels': [(255, 0, 0), (255, 0, 0), (0, 0, 255), (0, 0, 255)]
 }
```

Ejecutar el fichero `ejemplo.py` del repositorio plantilla, y ver su código fuente, para ver un rectángulo muy parecido a este cuadrado.

Todas las imágenes que se manejen en este proyecto tendrán ese formato.

### Requisitos mínimos

Se implementarán las siguientes funciones en un módulo llamado `transforms.py`, que manejarán
imágenes en el formato descrito anteriormente, utilizando las funciones necesarias de `images.py`. Las funciones (métodos) a implementar son:

* Método `mirror`, que producirá una nueva imagen "espejada" según un eje horizontal situado en la mitad de la imagen. Aceptará un parámetro, que será la imagen a espejar, y devolverá la imagen espejada. La signatura de esta función será:

```python
def mirror(image: dict) -> dict:
```

Por "espejada" se entiende que en la imagen resultante, la fila superior estará intercambiada con la inferior. La que está inmediatamente debajo de la superior, con la que está inmediatamente encima de la inferior, y así sucesivamente.

* Método 'grayscale', que creará una nueva imagen que contendrá la imagen original, pero en escala de grises. Aceptará un solo parámetros, que será la imagen a convertir. Devolverá una imagen. Para convertir un píxel RGB a gris, basta con calcular la media de los tres valores de la tupla RGB, y asignar ese valor a los tres valores de la tupla RGB del pixel equivalente en la imagen resultante. La signatura de esta función será:

```python
def grayscale(image: dict) -> dict:
```

* Método `blur` que creará una nueva imagen donde el valor RGB de cada pixel será el valor medio de los valores RGB de los pixels que tiene encima, debajo, a la derecha y a la izquierda (salvo que esté en los bordes, donde obviamente no se considerará el valor correspondiente: por ejemplo, si está arriba del todo, no se podrá usar el valor del pixel de encima, porque no lo hay). Aceptará un parámetro, que será la imagen a modificar, y devolverá la imagen modificada. La signatura de esta función será:

```python
def blur(image: dict) -> dict:
```

* Método `change_colors`, que producirá una nueva imagen con los colores cambiados. Aceptará tres parámetros, que serán: la imagen original, la lista de colores a cambiar, y la lista de colores por los que serán cambiados (el primer color de la lista a cambiar será cambiado por el primer color de la lista de cambiados, el segundo por el segundo, etc.). Cada lista de colores será una lista de tuplas RGB. El método devolverá una imagen. La signatura de esta función será:

```python
def change_colors(image: dict,
                  original: list[tuple[int, int, int]],
                  change: list[tuple[int, int, int]]) -> dict:
```

* Método `rotate`, que producirá una nueva imagen girada 90 grados hacia la derecha o hacia la izquierda. Aceptará dos parámetros, que serán la imagen a girar y la palabra `left` o `right` según se quiera rotar a la izquierda o a la derecha, y devolverá la imagen girada. La signatura de esta función será:

```python
def rotate(image: dict, direction: str) -> dict:
```

* Método `shift`, que desplazará la imagen el número de pixels que se indique, en el eje horizontal o vertical, según el parámetro que se indique. Aceptará tres parámetros, que serán: la imagen a modificar, la cantidad de pixels a desplazar horizontalmente hacia la derecha (será un número positivo), y la cantidad de pixels a desplazar verticalmente hacia abajo (será un número positivo). Si alguno de los parámetros de desplazamiento no se indica, no se desplazará en ese eje. El espacio que ocupaban los pixels que hayan sido desplazados se pondrá en negro. Devolverá una imagen de tamaño mayor o igual que el original. La signatura de esta función será:

```python
def shift(image: dict, horizontal: int = 0, vertical: int = 0) -> dict:
```

Por ejemplo, si la imagen original era del 80 de ancho por 100 de alto, y se desplaza 30 pixels a la derecha, y 3 hacia abajo, la imagen resultante tendrá 110 de ancho (80+30) por 103 de alto (100+3). Además, tendrá una banda negra de 30 pixels de ancho en su izquierda, y otra de 3 pixels de alto en su parte superior.

Para realizar ese desplazamiento, se llamaría la función como sigue:

```python
shift(imagen, horizontal=30, vertical=3)
```

* Método `crop`, que creará una nueva imagen que contendrá solo los pixels que se encuentren dentro de un rectángulo que se especifique. Aceptará cinco parámetros, que serán: la imagen a recortar, y cuatro parámetros para escpecificar el rectángulo a recortar. El rectángulo se especificará dando las coordenadas x, y de su esquina superior izquierda, su ancho, y su altura. Devolverá una imagen. La signatura de esta función será:

```python
def crop(image: dict, x: int, y: int, width: int, height: int) -> dict:
```


* Método `filter`, que creará una nueva imagen que contendrá la imagen original, pero con un filtro aplicado a todos sus pixels. El filtro se especificará como un multiplicador para cada valor RGB. Por lo tanto, la función aceptará cuatro parámetros, que serán la imagen a convertir, y el multiplicador de filtro para cada uno de los componentes RGB (que será un número real positivo). Cada pixel de la imagen resultante tendrá una tupla RGB igual a la de la imagen original, con cada componente RGB multiplicado por su multiplicador correspondiente y convertido a entero, teniendo en cuenta que si el resultado es mayor que el valor máximo posible para el componente, el valor será ese máximo posible (normalmente, 255). Devolverá una imagen. La signatura de esta función será:

```python
def filter(image: dict, r: float, g: float, b: float) -> dict:
```

Además, se crearán los siguientes programas, que utilizarán el módulo `transforms.py`:

* Programa `transform_simple.py`, que aceptará dos parámetros: el nombre de un fichero de imagen en formato RGB, y el nombre de una función de transformación entre las siguients: `mirror`, `blur`, `grayscale`. El programa deberá leer la imagen, aplicarle la transformación indicada, y producir un fichero con el nombre y extensión del fichero de entrada, pero con el sufijo `_trans` en el nombre. Por ejemplo, si el fichero original se llama `fichero.png`, el fichero de salida se llamará `fichero_trans.png`. Un ejemplo de ejecución será:

```python
python transform_simple.py fichero.png mirror
```
Este ejemplo producirá el fichero `fichero_trans.png` con la imagen que había en `fichero.png` espejada según el eje vertical.

* Programa `trasnsform_args.py`, que aceptará dos o más parámetros. El primer parámetro será el nombre de un fichero de imagen en formato RGB. El segundo parámetro será el nombre de una función de transformación cualquiera entre las que se hayan implementado. En caso de que la función admita parámetros, se incluirán a continuación:
  * Para la función `change_colors`, serán dos listas con los valores RGB a cambiar, y los valores RGB por los que se cambiarán. Cada lista de valores RGB se expresará como tuplas RGB (con sus tres valores separados por comas) separadas por `:`. Por ejemplo, la lista con los colores `(0,0,0)` y `(255,255,255)` se expresará como `0,0,0:255,255,255`. Si la lista tiene una sola tupla, no habrá `:`.
  * Para la función `rotate`, será la palabra `left` o la palabra `right`.
  * Para la función `shift`, serán dos números enteros, el número de pixels a desplazar horizontalmente y el número de pixels a desplazar verticalmente. El programa deberá leer la imagen, aplicarle la transformación indicada, y producir un fichero con el nombre y extensión del fichero de entrada, pero con el sufijo `_trans` en el nombre (igual que el programa anterior).
  * Para la función `crop`, serán cuatro números enteros, que corresponderán con sus parámetros (`x`, `y`, `width`, `height`).
  * Para la función `filter`, serán tres números reales (`float`), que corresponderán con sus parámetros.

Ejemplos de ejecución:

```python
python transform_args.py fichero.png change_colors 0,0,0:255,255,255:255,0,0:0,255,0
python transform_args.py fichero.png rotate right
python transform_args.py fichero.png filter 0.5 0.8 0.3
```

* Programa `transform_multi.py`, que funcionará como el anterior, pero admitiendo varios nombres de función transformadora, si es caso seguidos por sus parámetros. El primer argumento será la imagen a transformar, el siguiente la primera función de transformación, seguida si es caso por sus argumentos, seguidos de la segunda función de transformación (y sus argumentos), y así sucesivamente. El programa deberá leer la imagen, aplicarle las transformaciones indicadas en orden, y producir la imagen resultante, que tendrá el mismo nombre que se indica en los programas anteriores. Dos ejemplos de ejecución serán:

```python
python3 transform_multi.py fichero.png rotate right mirror blur
python3 transform_multi.py fichero.png rotate right mirror blur filter 0.5 0.8 0.3 shift 30 20
```

Todos los programas anteriores deberán tener la siguiente estructura, que pone el código principal en la función `main`, que podrá usar otras funciones auxiliares:

```python
from transforms import ...

## Puede haber una o más funciones definidas, que utilizará main()
...

def main():
    ...

if __name__ == '__main__':
    main()
```

### Notas sobre los requisitos mínimos

Todas las imágenes se leerán y se escribirán utilizando el módulo `images.py`. Una vez hayan sido leídas, se manejarán como diccionarios, con el formato que maneja `images.py`.

Cuando se realicen funciones de transformación, la imagen (diccionario, con su lista de pixels) que se devuelva será creada como una copia de la imagen original, que no se modificará. Para hacerlo, puede comenzarse por copiar el diccionario, con su lista de pixels en un diccionario con una lista de pixels completamente nueva, por ejemplo creando una imagen nueva con la función `create_blank` de `images.py`, y luego copiar con un bucle todos los pixels de la imagen original a la copia. Para hacer esto puede ser conveniente crear en `transforms.py` una función que lo realice, y sea llamada por las funciones de transformación cuando convenga.

En la versión de requisitos mínimos, solo se pide que los programas (`transform_simple.py`, `transform_args.py` y `transform_multi.py`) funcionen con argumentos correctos, y que todas las funciones también funcionen con parámetros correctos. Esto es, no hace falta que detecten que los argumentos y parámetros son incorrectos, ni que hagan nada específico si lo son.

### Requisitos opcionales

Cuando entregues la práctica, asegúrate de que cumple todos los requisitos mínimos anteriores. Cuando ya los tengas, puedes añadir código para que además cumpla alguno de los requisitos siguientes. Si el código que añades hace que alguno de los requisitos mínimos deje de funcionar, utliza una carpeta diferente para ese o esos requisitos opcionales, asegurándote que en la carpeta principal tienes la versión que cumple bien todos los requisitos mínimos. Asegúrate, en cualquier caso, de documentar los requisitos opcionales que has realizado en el documento de entrega de la práctica (ver más abajo).

Algunos requisitos opcionales son:

* Filtros avanzados. Crear alguna función para filtros avanzados. Por ejemplo, se puede crear un filtro para pasar la imagen a tonos sepia, o para reducir o aumentar la luminosidad de la foto, o para aumentar su contraste. Para hacerlo, buscar en Internet el método matemático, y escribir el código que haga la transformación, operando directamente sobre los pixels de la lista de listas que estamos usando (esto es, sin usar módulos que ya hagan ese filtrado).

* Detección de argumentos y parámetros incorrectos. Detectar que los argumentos que reciben los programas en línea de comandos son incorrectos, y en caso de que lo sean, mostrar mensajes explicando qué argumento es incorrecto y por qué. Igualmente, detectar que los parámetros de las funciones de transformación son incorrectos, y en caso de que lo sean, levantar excepciones con mensajes que expliquen qué parámetro es incorrecto y por qué.

* Uso directo de Pillow. Utilizar el módulo que utiliza el módulo `images.py` (Pillow) para crear otras funciones que hagan distintos procesamientos con las imágenes. Esas funciones deberán aceptar como parámetros imágenes en el formato que estamos usando (lista de listas de tuplas RGB), y devolver como resultado también imágenes en ese formato. Por lo tanto, internamente tendrán que transformar entre este formato y el que utliza Pillow.

* Programa interactivo. Crear un programa interactivo, que cargue una imagen, la muestre, y ofrezca como opciones aplicarle las funciones de transformación que hayas implementado. Será conveniente que también permita guardar en un fichero la imagen después de transforarla. Para mostrar la imagen, utiliza algún módulo Python que te permita pintar los pixels de la imagen en un lienzo (canvas) en una ventana que genere la aplicación.

* Versión orientada a objetos. Se puede rehacer el módulo `images.py` en uno que se llame `imagesclass.py`, que ofrezca la miosma funcionalidad, pero ortientada a objetos. Para ello, se puede construir una clase `Image`, que sirva para almacenar imágenes como diccionarios, como los maneja `images.py`, y que tenga (al menos) las mismas funciones que tiene el módulo `images,py`. Luego, las funciones que hay que implementar en la parte obligatoria de la práctica pueden implementarse como funciones de una clase hija de esa clase `Image` (llamada por ejemplo `ImageProcessing`). Y luego, se pueden implementar los programas que se pideen en la parte obligatoria usando esa nueva clase.

Además, puedes proponer tus propios requisitos opcionales. Si lo haces, háblalo con los profesores, para que te indiquesn aproximadamente cómo se van a valorar, y así puedas decidir mejor si te interesa o no realizarlos.

#### Entrega

El proyecto final ha de ser entregado en un repositorio de acceso público o interno (no privado), creado bifurcando (forking) el repositorio de  plantilla. La entrega de la práctica se hará subiendo commits a ese repositorio git, como hemos hecho en prácticas anteriores. El repositorio tendrá que tener al menos 10 commits, que muestren cómo se ha ido avanzando en el desarrollo de la práctica. Se recomienda hacer commits frecuentes según se va avanzando en el desarrollo del proyecto, incluso cuando aún no funcionen grandes partes de él.

La entrega deberá incluir un fichero `entrega.md`, que tendrá al menos los siguientes datos, en este orden:

* La primera línea del fichero ha de ser `# ENTREGA CONVOCATORIA JUNIO`. La segunda, el nombre completo del estudiante, y su dirección de correo de la Universidad.
* Enlace al video de demostración del proyecto. El vídeo será de una duración máxima de 2:00 minutos si se presenta solo la parte básica, de 3:00 minutos si también se presenta la parte adicional, y de 4:00 minutos si se presenta también la parte colectiva. El video constará de una captura de pantalla que muestre cómo se lanzan las aplicaciones, cómo se ven alguna de las imágenes resultantes, y en general que muestre lo mejor posible la funcionalidad del proyecto. Siempre que sea posible, el alumno comentará en el audio del vídeo lo que vaya ocurriendo en él. El video se subirá a algún servicio de subida de vídeos en Internet (por ejemplo, Vimeo, Twitch, o YouTube).
* Apartado "Requisitos mínimos": listado de los requisitos mínimos que se han implementado, indicando el nombre que aparece en el enunciado (por ejemplo "Método square").
* Apartado "Requisitos opcionales": listado de los requisitos opcionales que se han implementado, indicando el nombre que aparece en el enunciado (por ejemplo "Método readfile").
* Apartado "Requisitos opcionales propios": listado de los requisitos opcionales que se han implementado que no están en el listado del enunciado. En este caso, para cada uno de ellos, incluir una breve descripción, que incluya cómo se pueden ejecutar. 
* Comentarios. Cualquier otro comentario que se quiera realizar sobre la práctica entregada.

Al terminar, comprueba en tu repositorio en EIF GitLab que están correctamente todos los ficheros que quieres entregar en el repositorio de entrega.


