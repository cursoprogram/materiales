## Proyecto final (convocatoria de enero)

El proyecto final de la asignatura consiste en la creación de varios módulos de tratamiento digital de imágenes, y algunos programas que usan esos módulos para establecer cadenas de procesado digital.

El proyecto final está estructurada en:

* Requisitos mínimos. Cumplirlos es necesario para que el proyecto final sea considerado como aprobado, en cuyo caso seá puntuado con 2.0.
* Requisitos opcionales. Pueden ser puntuados con hasta 3.0 (adicionales a la puntuación de los requisitos mínimos).

El repositorio de plantilla de esta práctica tiene:

* Módulo `imagenes.py`, que será el módulo básico necesario que importarán los módulos que necesiten leer imágenes, guardar imágenes, o crear imágenes vacías.

* Ejemplos `ejemplo1.py`, `ejemplo2.py`

* Fichero `requirements.txt` con las dependencias de los módulos, que habrá que instalar en un entorno virtual. Una vez el entorno virtual esté creado y activado, se podrá ejecutar con el comando:

```
pip install -r requirements.txt
```

### Introducción

Esta práctica consiste en la creación de un módulo llamado `transforms.py`, que contendrá las funciones necesarias para tratar imágenes digitales. Para construirlo, habrá que apoyarse en el módulo `images.py`, que contendrá las funciones necesarias para leer y guardar imágenes. Para ello, representa imágenes como una matriz (lista de listas) de tuplas RGB, según el siguienet tipado Python:

```python
list[list[tuple[int, int, int]]]:
```

### Requisitos mínimos

Se implementarán las siguientes funciones en un módulo llamado `transforms.py`:

* Método `change_colors`, que producirá una nueva imagen con los colores cambiados. Aceptará tres parámetros, que serán: la imagen original, el color a cambiar, y color por el que será cambiado. devolverá una imagen. Las imágenes serán matrices (listas de listas) de tuplas (R, G, B), y los colores serán tuplas (R, G, B). La signatura de esta función será:

```python
def change_colors(image: list[list[tuple[int, int, int]]],
                  to_change: tuple[int, int, int],
                  to_change_to: tuple[int, int, int]) ->
    list[list[tuple[int, int, int]]]:
```

* Método `rotate_right`, que producirá una nueva imagen girada 90 grados hacia la derecha. Aceptará un parámetro, que será la imagen a girar, y devolverá la imagen girada. La signatura de esta función será:

```python
def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
```

* Método `mirror`, que producirá una nueva imagen "espejada" según un eje vertical situado en la mitad de la imagen. Aceptará un parámetro, que será la imagen a espejar, y devolverá la imagen espejada. La signatura de esta función será:

```python
def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
```

* Método `rotate_colors`, que producirá una nueva imagen con los colores cambiados. Para cambiarlos, sumará a cada uno de los componentes RGB el número que se le indique como incremento, teniendo en cuenta que si el valor llega a 255, tendrá que volver a empezar en 0 (esto es, se incrementará módulo 256). El incremento podrá ser también un valor negativo. Aceptará dos parámetros, que serán: la imagen original, y incremento a realizar a los valores RGB de cada pixel. Devolverá una imagen. La signatura de esta función será:

```python
def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) ->
    list[list[tuple[int, int, int]]]:
```

* Método `blur` que creará una nueva imagen donde el valor RGB de cada pixel será el valor medio de los valores RGB de los pixels que tiene encima, debajo, a la derecha y a la izquierda (salvo que esté en los bordes, donde obviamente no se considerá el valor correspondiente: pòr ejemplo, si está arriba del todo, no se podrá usar el valor del pixel de encima, porque no lo hay). Aceptará un parámetro, que será la imagen a modificar, y devolverá la imagen modificada. La signatura de esta función será:

```python
def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
```

* Método `shift`, que desplazará la imagen el número de pixels que se indique, en el eje horizontal o vertical, según el parámetro que se indique. Aceptará tres parámetros, que serán: la imagen a modificar, la cantidad de pixels a desplazar horizontalmente (hacia la derecha si es positivo, hacia la izquierda si es negativo), y la cantidad de pixels a desplazar verticalmente (hacia arriba si es positivo, hacia abajo si es negativo). Devolverá una imagen. La signatura de esta función será:

```python
def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) ->
    list[list[tuple[int, int, int]]]:
```

* Método `crop`, que creará una nueva imagen que contendrá solo los pixels que se encuentren dentro de un rectángulo que se especifique. Aceptará cinco parámetros, que serán: la imagen a recortar, y cuatro parámetros para escpecificar el rectángulo a recortar. El rectángulo se especificará dando las coordenadas x, y de su esquina superior izquierda, su ancho, y su altura. Devolverá una imagen. La signatura de esta función será:

```python
def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int) ->
    list[list[tuple[int, int, int]]]:
```
Además, se crearán los siguientes programas, que utilizarán el módulo `transforms.py`:

* Método 'greyscale', que creará una nueva imagen que contendrá la imagen original, pero en escala de grises. Aceptará un solo parámetros, que será la imagen a convertir. Devolverá una imagen. Para convertir un píxel RGB a gris, basta con calcular la media de los tres valores de la tupla RGB, y asignar ese valor a los tres valores de la tupla RGB del pixel equivalente en la imagen resultante. La signatura de esta funcón será:

```python
def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
```

* Método `filter`, que creará una nueva imagen que contendrá la imagen original, pero con un filtro aplicado a todos sus pixels. El filtro se especificará como un multiplicador para cada valor RGB. Por lo tanto, la función aceptará cuatro parámetros, que serán la imagen a convertir, y el multiplicador de filtro para cada uno de los componentes RGB. Cada pixel de la imagen resultante tendrá una tupla RGB igual a la de la imagen original, con cada componente RGB multiplicado por su multiplicador correspondiente, teniendo en cuenta que si el resultado es mayor que el valor máximo posible para el componente, el valor será ese máximo posible (normalmente, 255). Devolverá una imagen. La signatura de esta funcón será:

```python
def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> 
    list[list[tuple[int, int, int]]]:
```

* Programa `transform_simple.py`, que aceptará dos parámetros: el nombre de un fichero de imagen en formato RGB, y el nombre de una función de transformación entre las siguients: `rotate_right`, `mirror`, `blur`, `greyscale`. El programa deberá leer la imagen, aplicarle la transformación indicada, y producir un fichero con el nombre y extensión del fichero de entrada, pero con el sufijo `_trans` en el nombre. Por ejemplo, si el fichero original se llama `fichero.png`, el fichero de salida se llamará `fichero_trans.png`. Un ejemplo de ejecución será:

```python
python transform_simple.py fichero.png rotate_right
```

* Programa `trasnsform_args.py`, que aceptará dos o más parámetros. El primer parámetro será el nombre de un fichero de imagen en formato RGB. El segundo parámetro será el nombre de una función de transformación cualquiera entre las que se hayan implementado. En caso de que la función admita parámetros, se incluirán a continuación. Para la función `change_colors`, serán seis números enteros, que serán los valores RGB a cambiar, y los valores RGB por los que se cambiarán. Para la función `rotate_colors`, será un número entero, el incremento que se aplicará a todos los colores. Para la función `shift`, serán dos números enteros, el número de pixels a desplazar horizontalmente y el número de pixels a desplazar verticalmente. El programa deberá leer la imagen, aplicarle la transformación indicada, y producir un fichero con el nombre y extensión del fichero de entrada, pero con el sufijo `_trans` en el nombre (igual que el programa anterior). Un ejemplo de ejecución será:

```python
python transform_args.py fichero.png change_colors 255 0 0 0 255 0
```

* Programa `transform_multi.py`, que funcionará como el anterior, pero admitiendo varios nombres de función transformadora, si es caso seguidos por sus parámetros. El primer argumento será la imagen a transformar, el siguiente la primera función de transformación, seguida si es caso por sus argumentos, seguidos de la segunda función de transformación, y así sucesivamente. El programa deberá leer la imagen, aplicarle las transformaciones indicadas, y producir la imagen resultante, que tendrá el mismo nombre que se indica en los programas anteriores. Dos ejemplos de ejecución serán:

```python
python3 transform_multi.py fichero.png rotate_colors 90 mirror blur
python3 transform_multi.py fichero.png rotate_colors 90 change_colors 255 0 0 0 255 0 shift -20 20
```

Todos los programas anteriores deberán tener la siguiente estructura, que pone el código principal en la función `main`, que podrá usar otras funciones auxiliares:

```python
from transforms import ...

## Puede haber una o más funciones definidas, que utilizará main()
...

def main():
    ...

if __name__ == '__main__':
    main()
```

### Notas sobre los requisitos mínimos

Todas las imágenes se leerán y se escribirán utlizando el módulo `transforms.py`. Una vez hayan sido leidas, se manejarán como matrices (listas de listas) de tuplas (R, G, B), una por pixel, tal y como las proporcionan las funciones de `transforms.py`.

Cuando se realicen funciones de transformación, la imagen (matriz de tuplas) que se devuelva será creada como una copia de la imagen original, que no se modificará. Para hacerlo, puede comenzarse por copiar la matriz (lista de listas) de tuplas en una completamente nueva, por ejemplo creadndo una imagen nueva con la función `create_blank` de `transforms.py`, y luego copiar con dos bucles anidados todos los pixels (posiciones (x, y)) de la imagen original a la copia. Para hacer esto puede ser conveniente crear en `transforms.py` una función que lo realice, y sea llamada por las funciones de transformación cuando covnenga.

### Requisitos opcionales

Cuando entregues la práctica, asegúrate de que cumple todos los requisitos mínimos anteriores. Cuando ya los tengas, puedes añadir código para que además cumpla alguno de los requisitos siguientes. Si el código que añades hace que alguno de los requisitos mínimos deje de funcionar, utliza una carpeta diferente para ese o esos requisitos opcionales, asegurándote que en la carpeta principal tienes la versión que cumple bien todos los requisitos mínimos. Asegúrate, en cualquier caso, de documentar los requisitos opcionales que has realizado en el documento de entrega de la práctica (ver más abajo).

Algunos requisitos opcionales son:

* Filtros avanzados. Crear alguna función para filtros avanzados. Por ejemplo, se puede crear un filtro para pasar la imagen a tonos sepia, o para reducir o aumentar la lumninosidad de la foto, o para aumentar su contraste. Para hacerlo, buscar en Internet el método matemático, y escribir el código que haga la transformación, operando directamente sobre los pixels de la lista de listas que estamos usando (esto es, sin usar módulos que ya hagan ese filtrado).

* Uso directo de Pillow. Utilizar el módulo que utiliza el módulo `images.py` (Pillow) para crear otras funciones que hagan distintos procesamientos con las imágenes. Esas funciones deberán aceptar como parámetros imágenes en el formato que estamos usando (lista de listas de tuplas RGB), y devolver como resultado también imágenes en ese formato. Por lo tanto, internamente tendrán que transformar entre este formato y el que utliza Pillow.

* Programa interactivo. Crear un programa interactivo, que cargue una imagen, la muestre, y ofrezca como opciones aplicarle las funciones de transformación que hayas implementado. Será conveniente que también permita guardar en un fichero la imagen después de transforarla. Para mostrar la imagen, utiliza algún módulo Python que te permita pintar los pixels de la imagen en un lienzo (canvas) en una ventana que genere la aplicación.

* Versión orientada a objetos. Se puede rehacer el módulo `images.py` en uno que se llame `imagesclass.py`, que ofrezca la miosma funcionalidad, pero ortientada a objetos. Para ello, se puede construir una clase `Image`, que sirva para almacenar imágenes como listas de listas de pixels, y que tenga (al menos) las mismas funciones que tiene el módulo `images,py`. Luego, las funciones que hay que implementar en la parte obligatoria de la práctica pueden implementarse como funciones de una clase hija de esa clase `Image` (llamada por ejemplo `ImageProcessing`). Y luego, se pueden implementar los programas que se pideen en la parte obligatoria usando esa nueva clase.

Además, puedes proponer tus propios requisitos opcionales. Si lo haces, háblalo con los profesores, para que te indiquesn aproximadamente cómo se van a valorar, y así puedas decidir mejor si te interesa o no realizarlos.

#### Entrega

El proyecto final ha de ser entregado en un repositorio de acceso público o interno (no privado), creado bifurcando (forking) el repositorio de  plantilla. La entrega de la práctica se hará subiendo commits a ese repositorio git, como hemos hecho en prácticas anteriores.

La entrega deberá incluir un fichero `entrega.md`, que tendrá al menos los siguientes datos, en este orden:

* La primera línea del fichero ha de ser `# ENTREGA CONVOCATORIA ENERO`. La segunda, el nombre completo del estudiante, y su dirección de correo de la Universidad.
* Enlace al video de demostración del proyecto. El vídeo será de una duración máxima de 2:00 minutos si se presenta solo la parte básica, de 3:00 minutos si también se presenta la parte adicional, y de 4:00 minutos si se presenta también la parte colectiva. El video constará de una captura de pantalla que muestre cómo se lanzan las aplicaciones, cómo se ven alguna de las imágenes resultantes, y en general que muestre lo mejor posible la funcionalidad del proyecto. Siempre que sea posible, el alumno comentará en el audio del vídeo lo que vaya ocurriendo en él. El video se subirá a algún servicio de subida de vídeos en Internet (por ejemplo, Vimeo, Twitch, o YouTube).
* Apartado "Requisitos mínimos": listado de los requisitos mínimos que se han implementado, indicando el nombre que aparece en el enunciado (por ejemplo "Método square").
* Apartado "Requisitos opcionales": listado de los requisitos opcionales que se han implementado, indicando el nombre que aparece en el enunciado (por ejemplo "Método readfile").
* Apartado "Requisitos opcionales propios": listado de los requisitos opcionales que se han implementado que no están en el listado del enunciado. En este caso, para cada uno de ellos, incluir una breve descripción, que incluya cómo se pueden ejecutar. 
* Comentarios. Cualquier otro comentario que se quiera realizar sobre la práctica entregada.

Al terminar, comprueba en tu repositorio en EIF GitLab que están correctamente todos los ficheros que quieres entregar en el repositorio de entrega.


