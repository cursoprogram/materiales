# Estructura de datos

Actividades:

* Ejercicios de algoritmos simples
  * Algoritmo de selección
  * Algoritmo de insercción

## Ejercicios

**Ejercicio:** Ordena palabra según diccionario - método de selección
  * Ejercicio hecho en clase
  * [Enunciado](ordenapalabra/README.md)

**Ejercicio:** Ordena pixeles de un canvas
  * Ejercicio introducido en clase
  * **Fecha de entrega:** 14 de noviembre de 2024
  * [Repositorio plantilla](https://gitlab.eif.urjc.es/cursoprogram/plantillas2024/ordenapixeles/)
  * [Enunciado](ordenapixeles/README.md)

**Ejercicio:** Ordena pixeles de un canvas según brillo - método de selección
  * Ejercicio introducido en clase
  * **Fecha de entrega:** 21 de noviembre de 2024
  * [Repositorio plantilla](https://gitlab.eif.urjc.es/cursoprogram/plantillas2024/ordenabrillo/)
  * [Enunciado](ordenabrillo/README.md)

**Ejercicio:** Ordena palabra según diccionario - método de insercción
  * Ejercicio hecho en clase
  * [Enunciado](ordenapalabra-inserccion/README.md)

**Ejercicio:** Ordena pixeles de un canvas según brillo - método de insercción
  * Ejercicio introducido en clase
  * **Fecha de entrega:** 28 de noviembre de 2024
  * [Repositorio plantilla](https://gitlab.eif.urjc.es/cursoprogram/plantillas2024/ordenabrilloinserccion/)
  * [Enunciado](ordenabrillo-inserccion/README.md)

## Notas

### Selección vs Insercción

| Característica                                         | Algoritmo de Selección       | Algoritmo de Inserción        |
|--------------------------------------------------------|------------------------------|--------------------------------|
| **Funcionamiento**                                     | Selecciona el elemento mínimo de la parte no ordenada y lo coloca en la posición correcta al inicio de la parte ordenada. Repite hasta que la lista esté ordenada. | Inserta cada elemento en su posición correcta en la parte ordenada moviendo los elementos mayores hacia la derecha. |
| **Eficiencia en Listas Pequeñas (ojo, no en grandes)** | Buena                     | Muy buena                      |
| **Ideal para**                                         | Entender conceptos básicos de ordenación | Listas pequeñas o casi ordenadas |
| **Descripción**                                        | Encuentra el mínimo y lo coloca en su posición | Inserta elementos en la posición correcta moviendo los mayores |