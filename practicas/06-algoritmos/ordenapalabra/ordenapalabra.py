def find_minimum(word, start_pos):
    """
    Encuentra la posición del carácter mínimo en la subcadena desde `start_pos` hasta el final.
    Recibe la cadena `word` y la posición de inicio `start_pos`.
    Devuelve la posición del carácter mínimo encontrado.
    """
    lower_pos = start_pos
    for current_pos in range(start_pos, len(word)):
        if word[current_pos] < word[lower_pos]:
            lower_pos = current_pos
    return lower_pos

def selection_sort(word):
    """
    Ordena los caracteres de la palabra usando el algoritmo de ordenación por selección.
    Recibe una cadena `word` y devuelve la cadena ordenada alfabéticamente.
    """
    for start_pos in range(len(word)):
        # Encontrar la posición del carácter mínimo desde `start_pos`
        lowest_pos = find_minimum(word, start_pos)
        if lowest_pos != start_pos:
            # Intercambiar los caracteres creando un nuevo string
            char_at_start = word[start_pos]
            char_at_lowest = word[lowest_pos]
            word = (
                word[:start_pos] + char_at_lowest +  # Colocar el carácter mínimo en `start_pos`
                word[start_pos + 1:lowest_pos] +     # Mantener los caracteres intermedios
                char_at_start +                      # Colocar el carácter original de `start_pos` en `lowest_pos`
                word[lowest_pos + 1:]                # Mantener el resto del string
            )
    return word

def main():
    """
    Función principal que ordena una cadena de ejemplo y muestra el resultado.
    """
    mystring = "hola"
    sorted_string = selection_sort(mystring)
    print(sorted_string)  # Imprime la cadena ordenada, e.g., "ahlo"

if __name__ == "__main__":
    main()