from pixels import genera_canvas_aleatorio, tam_lienzo, pinta, espera

def contar_pixeles(canvas):
    """Cuenta cuántos píxeles de cada color hay en el canvas y los guarda en un diccionario."""
    conteo_colores = {"red": 0, "blue": 0, "black": 0}
    for fila in canvas:
        for pixel in fila:
            if pixel in conteo_colores:
                conteo_colores[pixel] += 1
    return conteo_colores

def pintar_colores_ordenados(conteo):
    """Pinta los píxeles en columnas verticales, primero los rojos, luego los azules, y finalmente los negros."""
    ancho, alto = tam_lienzo()  # Obtener el tamaño del lienzo
    x = 0
    y = 0

    for color, cantidad in conteo.items():
        for i in range(cantidad):
            pinta(x, y, color)  # Pinta el pixel en la posición (x, y)

            # Manejar el avance en el eje Y y el reinicio al cambiar de columna
            y += 1
            if y >= alto:  # Si se llega al final de la columna, avanzar a la siguiente
                y = 0
                x += 1

def main():
    canvas = genera_canvas_aleatorio()
    espera()
    conteo_colores = contar_pixeles(canvas)
    pintar_colores_ordenados(conteo_colores)
    espera()

if __name__ == "__main__":
    main()
