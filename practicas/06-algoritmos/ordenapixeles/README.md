### Analiza y Dibuja Píxeles de Colores con Diccionarios

El fichero `pixels.py` del repositorio plantilla ha sido modificado para incluir una función `genera_canvas_aleatorio()`, que genera un lienzo con píxeles pintados aleatoriamente en colores rojo, negro y azul. Solo se utilizará `pixels.py`, sin hacer ninguna llamada directa a otros módulos.

En esta práctica tendrás que completar un fichero llamado `ordenapixeles.py` que realice las siguientes tareas:

* Llamar a la función `genera_canvas_aleatorio()` de `pixels.py` para obtener un canvas con píxeles pintados de manera aleatoria.
* Recorrer el canvas fila a fila y contar cuántos píxeles hay de cada color (rojo, azul y negro), guardando la cuenta en un diccionario con las claves `"red"`, `"blue"`, y `"black"`.
* Pintar nuevamente los píxeles en columnas verticales: primero todos los píxeles rojos, luego los azules, y finalmente los negros, respetando el tamaño de 10 en 10 al usar la función `pinta()`.

Por ejemplo:

```commandline
python3 ordenapixeles.py
```

Esto generará un canvas con píxeles pintados aleatoriamente en rojo, negro y azul, recorrerá el canvas y contará los píxeles de cada color, y luego repintará el lienzo de manera ordenada en columnas verticales.

Además, cada una de las siguientes funciones ha de cumplir algunos detalles:

- `contar_pixeles(canvas)`:  Recorre el canvas y cuenta cuántos píxeles hay de cada color, devolviendo un diccionario con el conteo.
- `pintar_colores_ordenados(conteo)`: Pinta los píxeles de manera ordenada en columnas verticales, primero los rojos, luego los azules y finalmente los negros, utilizando dos bucles para manejar el eje X y el eje Y.

**[Ejemplo de solución](ordenapixeles.py)** (Recuerda que necesitas el fichero pixels del repositorio plantilla)