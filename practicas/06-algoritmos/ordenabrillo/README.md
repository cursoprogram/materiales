### Ordena el Brillo de los Píxeles en un Canvas

En esta práctica, se trabajará con el fichero `ordenabrillo.py`, donde implementarás un programa que ordene los píxeles de un canvas generado aleatoriamente en función de su brillo (usa el fichero `pixels.py` del repositorio plantilla). Utilizarás el algoritmo de ordenación por selección para ordenar los píxeles de menor a mayor brillo (ascendente) o de mayor a menor brillo (descendente) según el argumento proporcionado.

#### Descripción:

El objetivo es completar las funciones necesarias en `ordenabrillo.py` para que el programa pueda:
1. Leer un argumento de la línea de comandos que determine el orden de la ordenación (`"ascendente"` o `"descendente"`).
2. Generar un canvas de píxeles con colores aleatorios usando la función `genera_canvas_aleatorio()` de `pixels.py`.
3. Aplanar el canvas en una lista unidimensional con la función `aplanar_canvas()`.
4. Ordenar los píxeles por su brillo.
5. Repintar el canvas con los píxeles ordenados por brillo.

#### Detalles de las Funciones a Completar:

1. **`lee_orden()`**:
   - Valida los argumentos de la línea de comandos.
   - Devuelve el orden de ordenación (`"ascendente"` o `"descendente"`).
   - Si no se proporciona un argumento válido, imprime un mensaje de uso y termina el programa.

2. **`aplanar_canvas(canvas)`**:
   - Convierte un canvas bidimensional en una lista unidimensional de píxeles.
   - Devuelve la lista plana de píxeles.

3. **`encontrar_indice_seleccionado(pixels, indice_inicio, orden)`**:
   - Encuentra el índice del píxel con el brillo seleccionado (mínimo o máximo) desde `indice_inicio` hasta el final de la lista.
   - Devuelve el índice del píxel que debe ser intercambiado.

4. **`ordenar_pixeles_por_brillo(pixels, orden="ascendente")`**:
   - Utiliza `encontrar_indice_seleccionado()` para ordenar los píxeles por brillo en el orden especificado (ascendente o descendente).
   - Devuelve la lista de píxeles ordenados.

5. **`pintar_brillos_ordenados(pixels_ordenados)`**:
   - Pinta los píxeles ordenados por brillo en el canvas.

#### Ejemplo de Uso:

Ejecuta el programa desde la línea de comandos con uno de los siguientes argumentos:

```commandline
python3 ordenabrillo.py ascendente
```

Ordenará y pintará los píxeles del canvas de menor a mayor brillo.

```commandline
python3 ordenabrillo.py descendente
```

Ordenará y pintará los píxeles del canvas de mayor a menor brillo.

#### Consideraciones:

- Utiliza la función `calcula_brillo()` de `pixels.py` para obtener el brillo de cada color.
- Asegúrate de llamar a `espera()` al final del programa para que el canvas permanezca visible.
- Usa las funciones provistas y respeta los comentarios que explican su funcionamiento.
- No se permite el uso de métodos integrados de ordenación de Python (`sorted()` o `.sort()`).

**[Ejemplo de solución](ordenabrillo.py)**