import sys
from pixels import genera_canvas_aleatorio, tam_lienzo, pinta, espera, calcula_brillo

def lee_orden():
    """Valida los argumentos de la línea de comandos y devuelve el orden de ordenación."""
    if len(sys.argv) != 2 or sys.argv[1] not in ["ascendente", "descendente"]:
        print("Uso: python3 analiza_colores.py <ascendente|descendente>")
        sys.exit(1)
    return sys.argv[1]

def aplanar_canvas(canvas):
    """Devuelve una lista plana de píxeles a partir de un canvas bidimensional."""
    pixels_flat = []
    for row in canvas:
        for pixel in row:
            pixels_flat.append(pixel)
    return pixels_flat

def encontrar_indice_seleccionado(pixels, indice_inicio, orden):
    """Encuentra el índice del píxel con el brillo seleccionado (mínimo o máximo) según el orden."""
    indice_seleccionado = indice_inicio
    for j in range(indice_inicio + 1, len(pixels)):
        if orden == "ascendente" and calcula_brillo(pixels[j]) < calcula_brillo(pixels[indice_seleccionado]):
            indice_seleccionado = j
        elif orden == "descendente" and calcula_brillo(pixels[j]) > calcula_brillo(pixels[indice_seleccionado]):
            indice_seleccionado = j
    return indice_seleccionado

def ordenar_pixeles_por_brillo(pixels, orden="ascendente"):
    """Ordena los píxeles por brillo en orden ascendente o descendente usando el algoritmo de selección."""
    for i in range(len(pixels)):
        indice_seleccionado = encontrar_indice_seleccionado(pixels, i, orden)
        # Intercambiar los píxeles si es necesario
        pixels[i], pixels[indice_seleccionado] = pixels[indice_seleccionado], pixels[i]
    return pixels

def pintar_brillos_ordenados(pixels_ordenados):
    """Pinta los píxeles ordenados por brillo en el canvas."""
    ancho, alto = tam_lienzo()
    index = 0
    for y in range(alto):
        for x in range(ancho):
            if index < len(pixels_ordenados):
                pinta(x, y, pixels_ordenados[index])
                index += 1

def main():
    orden = lee_orden()
    canvas = genera_canvas_aleatorio()
    espera()
    pixels_flat = aplanar_canvas(canvas)
    pixels_ordenados = ordenar_pixeles_por_brillo(pixels_flat, orden)
    pintar_brillos_ordenados(pixels_ordenados)
    espera()

if __name__ == "__main__":
    main()
