import sys


def find_position(word, current_index):
    """
    Encuentra la posición correcta en la parte ordenada de la cadena donde se debe insertar el carácter actual.
    """
    current_char = word[current_index]
    pos = current_index - 1
    # Busca la posición correcta moviendo los caracteres mayores hacia la derecha
    while pos >= 0 and word[pos] > current_char:
        # Desplaza el carácter hacia la derecha en la cadena
        word = word[:pos + 1] + word[pos] + word[pos + 2:]
        pos -= 1
    return pos + 1, word

def insert_character(word, insert_pos, current_char):
    """
    Inserta el carácter actual en la posición correcta dentro de la cadena.
    """
    word = word[:insert_pos] + current_char + word[insert_pos + 1:]
    return word

def insertion_sort(word):
    """
    Ordena los caracteres de la palabra usando el algoritmo de ordenación por inserción.
    """
    for i in range(1, len(word)):
        insert_pos, updated_word = find_position(word, i)
        word = insert_character(updated_word, insert_pos, word[i])
    return word

def main():
    mystring = sys.argv[1]
    sorted_string = insertion_sort(mystring)
    print(sorted_string)  # Debería imprimir "ahlo"

if __name__ == "__main__":
    main()
