### Ordena Palabras con el Método de Inserción

El fichero `ordenapalabra-insert.py` de esta carpeta será el punto de partida para esta práctica. En esta actividad, completarás las funciones que implementan el algoritmo de ordenación por inserción para ordenar los caracteres de una palabra en orden alfabético.

#### Descripción:

El objetivo es implementar un programa que reciba una palabra y ordene sus caracteres utilizando el método de inserción. Este método inserta cada carácter en la posición correcta de la parte ya ordenada de la palabra, desplazando los caracteres mayores a la derecha.

#### Funciones a completar:

1. **`find_position(word, current_index)`**:
   - Busca la posición correcta en la parte ordenada de la cadena para el carácter en `current_index`.
   - Devuelve la posición donde se debe insertar el carácter y la cadena actualizada con los desplazamientos.

2. **`insert_character(word, insert_pos, current_char)`**:
   - Inserta el carácter actual en la posición `insert_pos` y devuelve la cadena actualizada.

3. **`insertion_sort(word)`**:
   - Ordena la palabra usando las funciones anteriores para aplicar el algoritmo de inserción.

#### Ejemplo de ejecución:

Si se ejecuta el programa con la palabra `"hola"`:

```commandline
python3 ordenapalabra-insert.py hola
```

El resultado en la consola debería ser:

```
ahlo
```

#### Detalles:

* Usa las funciones provistas y respeta los comentarios que explican su funcionamiento.
* No se permite el uso de métodos integrados de ordenación de Python (`sorted()` o `.sort()`).

**Pistas**:
- El método de inserción es eficiente para listas pequeñas y casi ordenadas.
- Presta atención al manejo de los índices y asegúrate de que los caracteres se desplazan correctamente antes de insertar.
