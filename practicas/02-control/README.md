# Estructuras de control

Actividades:

* Secuencia
* Condicionales: if-then-else
* Bucles: while, for
* Introducción a funciones
* Excepciones
* Aserciones (assertions)

## Aserciones (assertions)

Ejemplos:

```python
num = int(input())
assert num > 10
assert num > 10, "num is not greater than 10"
assert num > 10, f"num ({num} is not greater than 10"
assert num > 10, \
  f"num ({num} is not greater than 10"
```

```commandline
assert val in ['A', 'B', 'C', 'D']
assert isinstance(num, int)
```

Referencias:

* [Python's assert: Debug and Test Your Code Like a Pro](https://realpython.com/python-assert-statement/)
* [El libro de Python: Assert en Python](https://ellibrodepython.com/assert-python)

## Ejercicios

**Ejercicio:** Números primos.
  * Ejercicio realizado en clase
  * [Enunciado](primos/README.md)

**Ejercicio:** Enumeraciones.
  * Ejercicio de entrega en GitLab
  * **Fecha de entrega:** 10 de octubre de 2024
  * [Repositorio plantilla](https://gitlab.eif.urjc.es/cursoprogram/plantillas2024/enumera/)
  * [Enunciado](enumera/README.md)

**Ejercicio:** Suma
  * Ejercicio de entrega en GitLab
  * **Fecha de entrega:** 22 de octubre de 2024
  * [Repositorio plantilla](https://gitlab.eif.urjc.es/cursoprogram/plantillas2024/suma/)
  * [Enunciado](suma/README.md)
