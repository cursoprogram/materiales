#!/usr/bin/env python3

n = int(input("Dame un número entero mayor que 0: "))
if n <= 0:
    print("El número debe ser mayor que 0")
    exit()

for i in range(n, 0, -2):
    for j in range(1, i+1, 2):
        print(j, end=" ")
    print()

for i in range(n, 0, -2):
    for j in range(2, i+1, 2):
        print(j, end=" ")
    print()