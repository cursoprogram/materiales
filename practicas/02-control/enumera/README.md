### Enumeraciones

Escribe un programa Python que al arrancar pida al usuario un número entero. A continuación, escribirá en pantalla:

* En líneas sucesivas, listas de los números impares menores o iguales que ese número, empezando por 1, en orden ascendente; luego los mismos menos el último de los anteriores, y así sucesivamente hasta que solo escribamos un 1.
* A continuación en líneas sucesivas, listas de los números pares menores o iguales que ese número, empezando por 2, en orden ascendente; luego los mismos menos el último de los anteriores, y así sucesivamente hasta que solo escribamos un 2.

Comprueba que el número que te han dado es mayor que cero. Procura que el código sea lo más sencillo y legible posible, manteniendo la funcionalidad definida.

Ejemplos de ejecución podrían ser:

```shell
$ python3 enumeraciones.py
Dame un número entero mayor que 0: 5
1 3 5
1 3
1
2 4
2
```

```shell
$ python3 enumeraciones.py
Dame un número entero mayor que 0: 8
1 3 5 7
1 3 5
1 3
1
2 4 6 8
2 4 6
2 4
2
```

Recuerda que el programa tendrás que entregarlo en un repositorio de acceso público o interno (no privado), creado bifurcando (forking) el repositorio de plantilla de este ejercicio.

**[Ejemplo de solución](enumeraciones.py)**