#! /usr/bin/env python3
import sys


def main():
    argumento = int(sys.argv[1])
    numero = 1
    total = 0
    sumandos = []
    impares = 0
    while numero <= argumento:
        total = total + numero
        sumandos.append(str(numero))
        if numero % 2 != 0:
            impares = impares + 1
        numero = numero + 1

    print(" + ".join(sumandos), "=", total)
    if total % 2 == 0:
        print(f"{total} es par", end="")
    else:
        print(f"{total} es impar", end="")
    print(f", se han sumado {impares} números impares")


if __name__ == "__main__":
    main()
