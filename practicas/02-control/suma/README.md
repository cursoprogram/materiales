### Suma de números

Escribe un programa, `suma.py` que tome como argumento un número entero positivo y luego muestre la suma de todos los números enteros positivos menores o iguales a ese número. Cuando lo haya hecho, el programa mostrará en una nueva línea si la suma es un número par o impar, y cuántos números de los que se han sumado eran impares.

* Ejemplos de ejecución:

```commandline
$ python3 suma.py 5
1 + 2 + 3 + 4 + 5 = 15
15 es impar, se han sumado 3 números impares
$ python3 suma.py 10
1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 = 55
55 es impar, se han sumado 5 números impares
```

* Ayudas:

  * Puedes usar una variable para almacenar la suma y otra variable para almacenar el número actual que se está sumando. En cada iteraciòn del bucle `while`, puedes sumar el número actual a la variable de suma.
  * Puedes usar una lista para almacenar los números que se han sumado.
  * Puedes usar `" + ".join(lista)"` para convertir la lista de números en una cadena de texto.
  * Para saber qué argumento ha escrito el usuario en la línea de comandos, utiliza la lista de argumentos `sys.argv`. Esta lista tiene como primer elemento el nombre del programa, y los siguientes elementos son los argumentos escritos por el usuario. Si dudas al respecto, utiliza el depurador para ver sus valores.
  * En PyCharm puedes poner argumentos de línea de comandos editando la configuración del "ejecutador" del programa (accediendo a ella por ejemplo en el menú junto al triángulo y el bichito, en la parte superior), y poniendo el valor correspondiente en la caja "script commands".
  
* Restricciones:

  * El ejercicio debe resolverse usando uno o varios bucles `while`. No se pueden usar bucles `for` ni "list comprehensions".
  * No puede usarse la función `sum()` ni ninguna otra función que se encargue de la suma.
  * El programa tendrá también una función `main()`, que tendrá el bucle `while` que resuelve el problema.
  * Utiliza `if __name__ == "__main__"` para llamr a `main()` cuando el fichero se ejecute como programa principal.

Recuerda que el programa tendrás que entregarlo en un repositorio de acceso público o interno (no privado), creado bifurcando (forking) el repositorio de plantilla de este ejercicio. Has de utilizar el esquema de programa definido en el programa que encontrarás en ese repositorio de plantilla.

**[Ejemplo de solución](suma.py)**