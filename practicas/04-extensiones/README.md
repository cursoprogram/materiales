# Extensiones al entorno de programación

# El entorno de programación III

## Entornos virtuales (venv)

Actividades:

* Explicación del entorno virtual:
  * Creación: `python3 -m venv <dir>`
  * Activación: `<dir>/bin/activate`
  * Desactivación: `deactivate`
* Entornos virtuales en PyCharm

Referencias:

* [Python: Creation of virtual environments](https://docs.python.org/3/library/venv.html)
* [PyCharm: Configure a virtual environment](https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html)

## Instalación de paquetes (pip)

Actividades:

* Explicación de la instalación de paquetes:
  * https://pypi.org
  * `pip install <pkg>`

Referencias:

* [Python: Installing Packages](https://packaging.python.org/en/latest/tutorials/installing-packages/)
* [PyCharm: Install, uninstall, and upgrade packages](https://www.jetbrains.com/help/pycharm/installing-uninstalling-and-upgrading-packages.html)

## Análisis estático (MyPy)

Actividades:

* Instala el plugin MyPy de PyCharm (menú `Settings`, opción `Plugins`).
* Copia el fichero [segundos/segundos.py](segundos/segundos3.py) en un directorio, y ábrelo como repositorio en PyCharm.
* Ejecuta MyPy para comprobarlo

Referencias:

* [PyCharm MyPy plugin](https://plugins.jetbrains.com/plugin/11086-mypy)
* [MyPy Documentation](https://mypy.readthedocs.io/en/stable/index.html)
* [How to start using Python Type Annotations with Mypy](https://www.stackbuilders.com/blog/using-types-in-python-with-mypy/)


## Pruebas (testing)

Actividades:

* Copia el directorio [segundos](segundos) en un repositorio local.
* Ábrelo como un proyecto con PyCharm
* Ejecuta los tests (ejecutando el directorio `tests`)
* Ejecuta los tests (ejecutando el fichero `tests/test_segundos2.py`
* Fuerza algún error, bien cambiando el código de `segundos2.py`, bien cambiando alguna de las comprobaciones en los tests.

Referencias:

* [Real Python: Getting Started With Testing in Python](https://realpython.com/python-testing/)


## Ejercicios

**Ejercicio:** Programa que usa módulos de Pypi.
  * Ejercicio realizado en clase
  * Enunciado:
    * Crea entorno virtual
    * Instala paquetes: `pip install pyqrcode pypng`
    * Ejecuta [qr.py](../../frikiminutos/qr/qr.py)

**Ejercicio:** Programa que usa módulos de Pypi, en PyCharm.
  * Ejercicio realizado en clase
  * Enunciado:
    * Crea entorno virtual en PyCharm
    * Instala paquetes (pyqrcode, pypng) en PyCharm
    * Ejecuta [qr.py](../../frikiminutos/qr/qr.py)

