## Programa 2024-2025

### Clases presenciales

* Martes 9:00 a 11:00, 210 Laboratorios III
* Jueves 11:00 a 13:00, 203 Laboratorios III

### Sesiones

28 sesiones en total:

| Martes | Tema                      | Jueves | Tema                            |
|--------|---------------------------|--------|---------------------------------|
| Sep 10 | Presentación, Introducción | Sep 12 | Introducción                   |
| Sep 17 | Introducción              | Sep 19 | Introducción                    |
| Sep 24 | Entorno                   | Sep 26 | Entorno                         |
| Oct 1  | Entorno, Estructuras control | Oct 3  | Entorno, Estructuras de control |
| Oct 8  | NO LECTIVO                | Oct 10 | Divide y vencerás               |
| Oct 15 | Extensiones, Estructuras de control | Oct 17 | Divide y vencerás     |
| Oct 22 | Divide y vencerás         | Oct 24 | Divide y vencerás               |
| Oct 29 | Divide y vencerás         | Oct 31 | Estructura de datos             |
| Nov 5  | Estructuras datos         | Nov 7  | Algoritmos                      |
| Nov 12 | Estructuras datos         | Nov 14 | Algoritmos                      |
| Nov 19 | Estructuras datos         | Nov 21 | Algoritmos                      |
| Nov 26 | Estructuras datos         | Nov 28 | Testing y Proyeto final         |
| Dic 3  | Gestión de Ficheros y pila       | Dic 5  | NO LECTIVO               |
| Dic 10 | Orientación objetos       | Dic 12 |Explicacion Proyecto Final Opcional |
| Dic 17 | Recursividad              | Dic 19 | Eficiencia                      |

* [Temario de Frikiminutos](../frikiminutos/README.md)

### Programa

* Presentación de la asignatura
  * Objetivos
  * Métodos
  * Programa
  * Evaluación
* [Introducción a la programación y la informática](00-intro/README.md):
  * Programación con `turtle`
  * Motivación
  * Instrucciones
  * Variables
  * Expresiones
* [El entorno de programación](01-entorno/README.md):
  * El IDE (Integrated Development Environment): PyCharm
  * El entorno Linux del laboratorio
  * Introducción a la shell (intérprete de comandos)
  * Depuración (debugging)
  * Sistemas de control de versiones: Git
  * Servicios de apoyo al desarrollo: GitLab
* [Estructuras de control](02-control/README.md):
  * Secuencia
  * Condiciones (if-elif-else)
  * Bucles (for, while)
  * Excepciones (try, except)
  * Aserciones (assertions)
* [Divide y vencerás](03-divide/README.md):
  * Funciones
  * Ámbitos de las variables
  * Instalación de paquetes Python: pip
* [Extensiones al entorno de programación](04-extensiones/README.md):
  * Entornos virtuales
  * Instalación de paquetes
  * Pruebas (testing)
  * Análisis estático
* [Estructuras de datos](05-estructuradatos/README.md):
  * diccionarios
  * combinación de estructuras
  * punteros y gestión de memoria
* [Algoritmos I](06-algoritmos/README.md):
  * Selección
  * Insercción
  * Manipulación de cadenas
* [Testing](07-testing/README.md):
  * Introducción a tests
  * Tests en ejercicios
* Gestión de ficheros y pila
* Programación orientada a objectos
  * Clases y objetos (instanciación de clases)
  * Herencia
* [Recursividad](): (pequeña broma)
  * [Ejemplos de recursividad](08-recursividad/README.md)
* Eficiencia:
  * Complejidad
  * Tipos de complejidad
  * Complejidad para algunos algoritmos
* [Proyecto final](final)
