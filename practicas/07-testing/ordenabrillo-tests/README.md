### Testea el Brillo de los Píxeles en un Canvas

En esta práctica, se trabajará con el fichero `ordenabrillo.py`, que contiene un programa diseñado para ordenar los píxeles de un canvas generado aleatoriamente en función de su brillo. Tu tarea será crear un conjunto de tests automatizados utilizando el módulo `unittest`, verificando que las funciones principales del programa funcionan correctamente en distintos escenarios. Estos tests se encontraran en el fichero `tests.py`.

#### Objetivo:

El objetivo de esta práctica es:
1. Aprender a escribir tests para verificar el comportamiento de un programa.
2. Familiarizarse con los métodos del módulo `unittest`, como `assertEqual`, `assertRaises`, y `@patch`.
3. Desarrollar confianza en la calidad del código a través de la validación automatizada.

---

### Detalles de las Funciones a Testear

Deberás escribir tests para las siguientes funciones de `ordenabrillo.py`. A continuación, se incluye un esquema inicial para que completes los tests agregando los `assert` necesarios.

#### 1. `lee_orden()`
Esta función valida los argumentos de la línea de comandos y devuelve el orden de ordenación (`"ascendente"` o `"descendente"`). Si no se proporciona un argumento válido, termina el programa.

**Esquema:**
```python
@patch("sys.argv", ["ordenabrillo.py", "ascendente"])
def test_lee_orden_ascendente(self):
    # Agrega el assert necesario

@patch("sys.argv", ["ordenabrillo.py", "descendente"])
def test_lee_orden_descendente(self):
    # Agrega el assert necesario

@patch("sys.argv", ["ordenabrillo.py"])
def test_lee_orden_faltan_argumentos(self):
    # Verifica que el programa termina con un error

@patch("sys.argv", ["ordenabrillo.py", "otro"])
def test_lee_orden_orden_invalido(self):
    # Verifica que el programa termina con un error
```

---

#### 2. `aplanar_canvas(canvas)`
Convierte un canvas bidimensional en una lista unidimensional de píxeles.

**Esquema:**
```python
def test_aplanar_canvas(self):
    # Verifica que un canvas 2D se convierte en una lista plana correctamente
```

---

#### 3. `encontrar_indice_seleccionado(pixels, indice_inicio, orden)`
Encuentra el índice del píxel con el brillo mínimo o máximo desde `indice_inicio` según el orden especificado.

**Esquema:**
```python
def test_encontrar_indice_seleccionado_ascendente(self):
    # Verifica que encuentra el índice correcto para brillo ascendente

def test_encontrar_indice_seleccionado_descendente(self):
    # Verifica que encuentra el índice correcto para brillo descendente
```

---

#### 4. `ordenar_pixeles_por_brillo(pixels, orden)`
Ordena los píxeles por brillo en el orden especificado (ascendente o descendente).

**Esquema:**
```python
def test_ordenar_pixeles_por_brillo_ascendente(self):
    # Verifica que los píxeles se ordenan correctamente de menor a mayor brillo

def test_ordenar_pixeles_por_brillo_descendente(self):
    # Verifica que los píxeles se ordenan correctamente de mayor a menor brillo
```

---

#### 5. `calcula_brillo(color)`
Calcula el brillo de un color en función del diccionario provisto en `pixels.py`.

**Esquema:**
```python
def test_calcula_brillo(self):
    # Verifica que calcula el brillo correcto para diferentes colores
```

---

### Ejemplo de Uso

Una vez que completes los tests, puedes ejecutarlos desde la terminal para verificar que las funciones de `ordenabrillo.py` funcionan correctamente.

1. Ejecuta los tests:
   ```bash
   python tests.py
   ```

2. O utiliza el descubridor de tests de `unittest`:
   ```bash
   python -m unittest discover
   ```

---

### Consideraciones:

- **Completa los `assert` faltantes** en los esquemas de cada función para validar su comportamiento esperado.
- Asegúrate de incluir casos normales (funcionamiento esperado) y casos límite (argumentos inválidos o datos extremos).
- Usa `@patch` para simular los argumentos de línea de comandos en los tests de `lee_orden()`.
- Revisa que los valores de brillo en `calcula_brillo` coincidan con los definidos en `pixels.py`.

