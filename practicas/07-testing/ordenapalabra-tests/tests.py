import unittest
from ordenapalabra import find_minimum, selection_sort

class TestOrdenaPalabra(unittest.TestCase):

    # Tests para find_minimum
    def test_find_minimum_in_middle(self):
        self.assertEqual(find_minimum("hola", 0), 3, "Debería encontrar 'a' como el carácter mínimo en la posición 3")

    def test_find_minimum_entire_string(self):
        self.assertEqual(find_minimum("zxy", 0), 1, "Debería encontrar 'x' como el carácter mínimo en la posición 1")

    def test_find_minimum_from_position(self):
        self.assertEqual(find_minimum("bca", 1), 2, "Desde la posición 1, el carácter mínimo 'a' está en la posición 2")

    def test_find_minimum_single_char(self):
        self.assertEqual(find_minimum("h", 0), 0, "Con un solo carácter, debería devolver 0")

    # Tests para selection_sort
    def test_selection_sort_simple(self):
        self.assertEqual(selection_sort("hola"), "ahlo", "La palabra 'hola' debería ordenarse como 'ahlo'")

    def test_selection_sort_already_sorted(self):
        self.assertEqual(selection_sort("abc"), "abc", "La palabra ya ordenada debería permanecer igual")

    def test_selection_sort_reverse_order(self):
        self.assertEqual(selection_sort("zyx"), "xyz", "La palabra 'zyx' debería ordenarse como 'xyz'")

    def test_selection_sort_with_repeated_chars(self):
        self.assertEqual(selection_sort("banana"), "aaabnn", "La palabra 'banana' debería ordenarse como 'aaabnn'")

    def test_selection_sort_empty_string(self):
        self.assertEqual(selection_sort(""), "", "Una cadena vacía debería devolver otra cadena vacía")

    def test_selection_sort_single_char(self):
        self.assertEqual(selection_sort("a"), "a", "Una cadena con un solo carácter debería permanecer igual")

# Ejecutar los tests
if __name__ == '__main__':
    unittest.main()