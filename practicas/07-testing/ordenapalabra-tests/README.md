# Tests para la ordenación de palabras con selección

## Objetivo

El objetivo de este ejercicio es aprender a utilizar el módulo `unittest` para escribir y ejecutar pruebas automatizadas en Python. Esto incluye:

1. Probar funciones específicas para encontrar el carácter mínimo en una subcadena y ordenar palabras alfabéticamente.
2. Familiarizarse con diferentes tipos de aserciones (`assertEqual`, `assertNotEqual`, `assertTrue`, etc.).
3. Comprobar el comportamiento esperado de las funciones `find_minimum` y `selection_sort` en casos comunes y casos límite.

---

## Descripción

Se proporciona un conjunto de funciones para ordenar palabras mediante el algoritmo de selección, contenidas en el fichero [ordenapalabra.py](ordenapalabra.py). Estas funciones son:

1. **`find_minimum(word, start_pos)`**: Encuentra la posición del carácter mínimo en una subcadena.
2. **`selection_sort(word)`**: Ordena los caracteres de una palabra alfabéticamente.

La tarea es:

1. Escribir pruebas unitarias para estas funciones utilizando `unittest`.
2. Asegurarte de que las pruebas cubran casos como cadenas vacías, caracteres repetidos, cadenas ya ordenadas, y cadenas en orden inverso.
3. Verificar que los resultados sean correctos utilizando diferentes tipos de aserciones.

Los tests están en el fichero `tests.py`.

---

## Tipos de Aserciones Utilizadas

En este ejercicio se han utilizado los siguientes tipos de aserciones:

- **`assertEqual(a, b)`**: Comprueba que `a` sea igual a `b`.
- **`assertNotEqual(a, b)`**: Comprueba que `a` no sea igual a `b`.
- **`assertTrue(x)`**: Verifica que la condición `x` sea verdadera.
- **`assertGreater(a, b)`**: Verifica que `a` sea mayor que `b`.
- **`assertLess(a, b)`**: Verifica que `a` sea menor que `b`.

---

## Cómo ejecutar los tests

Ejecuta el archivo directamente desde la terminal:
   ```bash
   python tests.py
   ```

O bien, utiliza el módulo `unittest` para ejecutar los tests:
   ```bash
   python -m unittest tests.py
   ```

También puedes descubrir y ejecutar automáticamente todos los tests de un directorio con:
   ```bash
   python -m unittest discover
   ```

---
