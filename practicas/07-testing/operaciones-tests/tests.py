import unittest
from operaciones import suma, resta, multiplicar, dividir

class TestOperacionesMatematicas(unittest.TestCase):

    # Tests para la suma
    def test_suma_positivos(self):
        self.assertEqual(suma(3, 2), 5, "La suma de 3 y 2 debería ser 5")

    def test_suma_negativos(self):
        self.assertEqual(suma(-3, -2), -5, "La suma de -3 y -2 debería ser -5")

    def test_suma_mixtos(self):
        self.assertTrue(suma(-3, 3) == 0, "La suma de -3 y 3 debería ser 0")

    # Tests para la resta
    def test_resta_positivos(self):
        self.assertEqual(resta(5, 3), 2, "La resta de 5 y 3 debería ser 2")

    def test_resta_negativos(self):
        self.assertNotEqual(resta(-5, -3), -10, "La resta de -5 y -3 no debería ser -10")

    def test_resta_mixtos(self):
        self.assertGreater(resta(3, -3), 0, "La resta de 3 y -3 debería ser mayor que 0")

    # Tests para la multiplicación
    def test_multiplicacion_positivos(self):
        self.assertEqual(multiplicar(4, 3), 12, "La multiplicación de 4 y 3 debería ser 12")

    def test_multiplicacion_cero(self):
        self.assertEqual(multiplicar(4, 0), 0, "Cualquier número multiplicado por 0 debería ser 0")

    def test_multiplicacion_negativos(self):
        self.assertTrue(multiplicar(-4, -3) > 0, "La multiplicación de dos negativos debería ser positiva")

    # Tests para la división
    def test_division_basica(self):
        self.assertAlmostEqual(dividir(10, 4), 2.5, msg="La división de 10 entre 4 debería ser 2.5")

    def test_division_por_cero(self):
        with self.assertRaises(ValueError, msg="Debería lanzar ValueError al dividir por cero"):
            dividir(10, 0)

    def test_division_negativa(self):
        self.assertLess(dividir(-10, 2), 0, "La división de un negativo entre un positivo debería ser negativa")

    def test_division_cercana(self):
        self.assertAlmostEqual(dividir(1, 3), 0.3333, places=4, msg="La división de 1 entre 3 debería ser aproximadamente 0.3333")

# Ejecución de los tests
if __name__ == '__main__':
    unittest.main()