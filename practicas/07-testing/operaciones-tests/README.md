# Tests para las operaciones matemáticas más simples

## Objetivo

El objetivo de este ejercicio es aprender a utilizar el módulo `unittest` para escribir y ejecutar pruebas automatizadas en Python. Esto incluye:

1. Probar funciones matemáticas básicas como **suma**, **resta**, **multiplicación** y **división**.
2. Familiarizarse con diferentes tipos de aserciones (`assertEqual`, `assertTrue`, `assertGreater`, etc.).
3. Manejar excepciones en los tests, como la división por cero.


## Descripción

Se proporciona un conjunto de funciones matemáticas (`suma`, `resta`, `multiplicar`, `dividir`) que implementan operaciones básicas, dentro del fichero [operaciones.py](operaciones.py), lo que hemos hecho en clase:

1. Escribir pruebas unitarias para estas funciones utilizando `unittest`.
2. Asegurarte de que las pruebas cubran casos comunes (valores positivos, negativos, cero) y casos límite (como dividir por cero).
3. Utilizar diferentes tipos de aserciones para verificar el comportamiento esperado.

Estos tests están en el fichero `tests.py`

## Tipos de Aserciones Utilizadas

En este ejercicio se han utilizado los siguientes tipos de aserciones:

- **`assertEqual(a, b)`**: Comprueba que `a` sea igual a `b`.
- **`assertNotEqual(a, b)`**: Comprueba que `a` no sea igual a `b`.
- **`assertTrue(x)`**: Verifica que la condición `x` sea verdadera.
- **`assertGreater(a, b)`**: Verifica que `a` sea mayor que `b`.
- **`assertLess(a, b)`**: Verifica que `a` sea menor que `b`.
- **`assertAlmostEqual(a, b, places=n)`**: Verifica que `a` y `b` sean iguales hasta `n` decimales.
- **`assertRaises(exception)`**: Comprueba que se lanza una excepción específica (por ejemplo, `ValueError` al dividir por cero).

---

## Cómo ejecutar los tests

Sigue estos pasos para ejecutar los tests:

Ejecuta el archivo directamente desde la terminal:

   ```bash
   python test.py
   ```

O bien, utiliza el módulo `unittest` para ejecutar los tests:
   ```bash
   python -m unittest test_operaciones.py
   ```

También puedes descubrir y ejecutar automáticamente todos los tests de un directorio con:
   ```bash
   python -m unittest discover
   ```