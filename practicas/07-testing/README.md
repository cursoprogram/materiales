# Testing

Actividades:

* Testing en funciones simples
* Testing en ejercicios de entrega

## Ejercicios

**Ejercicio:** Tests en funciones simples
  * Ejercicio hecho en clase
  * [Enunciado](operaciones-tests/README.md)

**Ejercicio:** Tests en ordenapalabra por selección
  * Ejercicio hecho en clase
  * [Enunciado](ordenapalabra-tests/README.md)

**Ejercicio:** Tests en Ordenabrillo por selección
  * Ejercicio introducido en clase
  * **Fecha de entrega:** 05 de diciembre de 2024
  * [Repositorio plantilla](https://gitlab.eif.urjc.es/cursoprogram/plantillas2024/ordenabrillo-tests/)
  * [Enunciado](ordenabrillo-tests/README.md)

