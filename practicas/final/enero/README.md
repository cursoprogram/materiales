### Proyecto Final: Generación y Manipulación de Texturas Visuales

El proyecto final de la asignatura consiste en la creación de varios módulos para generar y manipular texturas visuales en un lienzo bidimensional. Se trabajará con píxeles simulados para crear patrones, aplicar transformaciones y efectos, y generar texturas dinámicas. Este proyecto os permitirá consolidar los conceptos fundamentales de programación y procesamiento de datos visuales.


**Importante**: Este enunciado puede tener algún error y va a sufrir cambios, por favor si encontráis algún fallo, notificarlo a los profesores de la asignatura.


[Repositorio plantilla](https://gitlab.eif.urjc.es/cursoprogram/plantillas2024/final/)


---

#### Estructura del Proyecto

El proyecto está estructurado en:
- **Requisitos mínimos**: Cumplirlos es necesario para que el proyecto sea considerado aprobado, con una calificación base de 2.0 puntos.
- **Requisitos opcionales**: Implementar estas funcionalidades adicionales permitirá obtener hasta 3.0 puntos adicionales.

El repositorio de plantilla incluye:
- **Módulo `texturas.py`**: Contendrá las funciones necesarias para generar, manipular y transformar texturas.
- **Ejemplo `demo_texturas_minimo.py`**: Un ejemplo funcional que aplica efectos y transforma texturas, llamando a las funciones de los requisitos mínimos.
- **Ejemplo `demo_texturas_opcional.py`**: Un ejemplo funcional que aplica efectos y transforma texturas, llamando a las funciones de los requisitos opcionales.
- **Directorio `tests/`**: Incluye pruebas básicas para los requisitos mínimos.
- **Fichero `README.md`**: Información de la práctica y enlace a este enunciado.
---

### Introducción

En este proyecto, se trabajará con un lienzo representado como una cuadrícula bidimensional donde cada celda representa un píxel. Los píxeles estarán definidos por tuplas RGB `(R, G, B)`, y el lienzo se manejará como una lista de listas de píxeles, junto a un ancho y un alto del lienzo, en formato diccionario:

```python
# Un lienzo de 3 columnas y dos filas, con los píxeles de la fila superior rojos y el resto azules
lienzo = {
    "width": 3,
    "height": 2,
    "pixels": [
        [(255, 0, 0), (255, 0, 0), (255, 0, 0)],  # Fila superior: rojo
        [(0, 0, 255), (0, 0, 255), (0, 0, 255)],  # Fila inferior: azul
    ],
}
```

Las funciones implementadas permitirán:
- Generar patrones visuales básicos.
- Aplicar transformaciones al lienzo.
- Añadir efectos visuales a las texturas generadas.

---

### Requisitos Mínimos (2.0 puntos)

En el módulo `texturas.py`, se implementarán funciones que combinan diccionarios, listas y tuplas para la manipulación y transformación de un lienzo. El lienzo se representará como un diccionario con las siguientes claves:

- `width`: Ancho del lienzo en píxeles.
- `height`: Alto del lienzo en píxeles.
- `pixels`: Una lista bidimensional de píxeles (lista de listas, simulando el canvas), donde cada píxel es una tupla `(R, G, B)`.

Un ejemplo de un lienzo de 5 de ancho y 5 de alto tendría la siguiente estructura:

```python
# Un lienzo de 5 x 5
lienzo = {
    "width": 5,
    "height": 5,
    "pixels": [
        [(12, 56, 99), (34, 78, 120), (56, 99, 143), (78, 120, 165), (99, 143, 187)],
        [(143, 187, 210), (187, 210, 233), (210, 233, 255), (233, 255, 255), (255, 255, 255)],
        [(100, 150, 200), (120, 160, 210), (140, 180, 220), (160, 200, 230), (180, 220, 240)],
        [(50, 75, 100), (75, 100, 125), (100, 125, 150), (125, 150, 175), (150, 175, 200)],
        [(10, 20, 30), (20, 30, 40), (30, 40, 50), (40, 50, 60), (50, 60, 70)],
    ],
}
```

#### **1. Patrones Complejos**

Se deberán implementar funciones que generen patrones en el lienzo y almacenen los datos en un diccionario:

- **Patrón Aleatorio**:
  Genera un lienzo con colores aleatorios en cada píxel.

  ```python
  def generar_patron_aleatorio(ancho, alto):
      """
      Genera un lienzo con colores aleatorios.

      Args:
          ancho (int): Número de píxeles de ancho.
          alto (int): Número de píxeles de alto.

      Returns:
          dict: Lienzo representado como un diccionario con claves 'width', 'height' y 'pixels'.
      """
      pass
  ```

- **Patrón a Cuadros**:
  Crea un diseño tipo tablero de ajedrez alternando entre dos colores especificados.

  ```python
  def generar_patron_cuadros(ancho, alto, color1, color2):
      """
      Genera un lienzo con un patrón de cuadros.

      Args:
          ancho (int): Número de píxeles de ancho.
          alto (int): Número de píxeles de alto.
          color1 (tuple): Color de los cuadros claros en formato RGB.
          color2 (tuple): Color de los cuadros oscuros en formato RGB.

      Returns:
          dict: Lienzo representado como un diccionario con claves 'width', 'height' y 'pixels'.
      """
      pass
  ```

- **Patrón de Rectángulos**:
  Genera un lienzo con rectángulos de colores definidos por el usuario. Cada rectángulo estará caracterizado por su posición, dimensiones, color y si debe estar relleno o solo mostrar el borde.

  ```python
  def generar_patron_rectangulos(ancho, alto, rectangulos):
    """
    Genera un lienzo con un patrón de rectángulos.

    Args:
        ancho (int): Número de píxeles de ancho del lienzo.
        alto (int): Número de píxeles de alto del lienzo.
        rectangulos (list): Lista de rectángulos a dibujar. Cada rectángulo es un diccionario con:
            - 'x': Coordenada X de la esquina superior izquierda.
            - 'y': Coordenada Y de la esquina superior izquierda.
            - 'ancho': Ancho del rectángulo.
            - 'alto': Alto del rectángulo.
            - 'color': Color del rectángulo en formato RGB.
            - 'relleno': Booleano, si es True, el rectángulo estará relleno.

    Returns:
        dict: Lienzo representado como un diccionario con los rectángulos dibujados.
    """
    # Inicializar el lienzo con píxeles blancos
    pixels = []
    for i in range(alto):
        fila = []
        for j in range(ancho):
            fila.append((255, 255, 255))
        pixels.append(fila)


    pass
  ```

---

#### **2. Análisis de Brillos**

Implementar una función que analice los brillos de los píxeles del lienzo y devuelva un diccionario con:
- La lista de brillos calculados de cada píxel.
- El brillo promedio del lienzo.
- Los píxeles con brillo máximo y mínimo.

Para calcular el brillo de cada píxel por favor ver los "Detalles Adicionales" de la práctica.

```python
def analizar_brillos(lienzo):
    """
    Analiza los brillos de los píxeles del lienzo.

    Args:
        lienzo (dict): Lienzo representado como un diccionario.

    Returns:
        dict: Diccionario con claves 'brillos', 'promedio', 'max', 'min'.
              'brillos': Lista de brillos de cada píxel.
              'promedio': Brillo promedio del lienzo.
              'max': Píxel con brillo máximo.
              'min': Píxel con brillo mínimo.
    """
    pass
```

---

#### **3. Ordenación por Brillo**

Ordenar los píxeles del lienzo por su brillo utilizando el algoritmo de selección. El orden puede ser ascendente o descendente, y se debe devolver un lienzo con los píxeles ordenados.

```python
def ordenar_por_brillo(lienzo, orden="ascendente"):
    """
    Ordena los píxeles del lienzo por brillo usando el algoritmo de selección.

    Args:
        lienzo (dict): Lienzo representado como un diccionario.
        orden (str): 'ascendente' o 'descendente'.

    Returns:
        dict: Nuevo lienzo con los píxeles ordenados por brillo.
    """
    pass
```

---

#### **4. Transformaciones**

Las siguientes funciones aplicarán transformaciones geométricas y reorganización de píxeles:

- **Rotar Lienzo**:
  Gira el lienzo en un ángulo de 90, 180 o 270 grados.

  ```python
  def rotar_textura(lienzo, angulo):
      """
      Rota el lienzo en el ángulo especificado.

      Args:
          lienzo (dict): Lienzo representado como un diccionario.
          angulo (int): Ángulo de rotación (90, 180, 270).

      Returns:
          dict: Lienzo rotado.
      """
      pass
  ```

- **Reorganizar por Zonas**:
  Divide los píxeles del lienzo en cuatro cuadrantes según el valor de la componente seleccionada (`r`, `g`, `b`):
    - Cuadrante 1: Píxeles con valores bajos (0-63) de la componente seleccionada.
    - Cuadrante 2: Píxeles con valores medios-bajos (64-127).
    - Cuadrante 3: Píxeles con valores medios-altos (128-191).
    - Cuadrante 4: Píxeles con valores altos (192-255).

  ```python
  def reorganizar_por_componente(lienzo, componente):
    """
    Reorganiza el lienzo dividiéndolo en cuadrantes según una componente (R, G o B).

    Args:
        lienzo (dict): Lienzo representado como un diccionario.
        componente (str): Componente del color ('r', 'g' o 'b').

    Returns:
        dict: Lienzo reorganizado por la componente seleccionada.
    """
    pass
  ```

- **Transformar a Escala de Grises**:
  Convierte todos los píxeles del lienzo a tonos de gris. El tono de gris se calcula como el promedio de los valores RGB del píxel original. Por ejemplo un píxel con RGB `(10, 50, 30)` tiene una escala de grises de `(30, 30, 30)`.
  ```python
    def transformar_a_grises(lienzo):
    """
    Convierte el lienzo a escala de grises.

    Args:
        lienzo (dict): Lienzo representado como un diccionario.

    Returns:
        dict: Lienzo transformado a escala de grises.
    """
    pass
  ```

---

### Detalles Adicionales

1. **Brillo de Píxeles**:
   El brillo se calculará como el promedio de los valores RGB:
   ```python
   def calcula_brillo(pixel):
       return (pixel[0] + pixel[1] + pixel[2]) // 3
   ```

2. **Estructura del Lienzo**:
   Todas las funciones deben trabajar con la representación de lienzo como un diccionario con las claves `width`, `height` y `pixels`. El valor de las claves `width` y `height` debe de ser un entero (int) que determine el tamaño del lienzo, el valor de la clave `pixels` debe de ser una lista bidimensional donde se almacene una tupla RGB `(R, G, B)` por cada píxel.

3. **Validación de Entradas**:
   Las funciones deberán manejar casos de entrada inválidos con mensajes de error claros.

4. **Pintar el lienzo**:
   Como en las prácticas semanales, hay un fichero `pixels.py` donde hay una funcion `pinta_lienzo(lienzo)` que se llamará pasando por argumento el diccionario resultante de cada una de las funciones de la práctica final que devuelvan un diccionario de un lienzo para ver el resultado pintado.

5. **Esperar para Visualización**:
   Además, el fichero `pixels.py` incluye una función `espera()` que debe ser llamada al final de la ejecución para mantener la ventana del lienzo abierta. Esto asegurará que el lienzo permanezca visible hasta que el usuario cierre manualmente la ventana.

6. **Generar lienzo según imagen**:
   Dentro del fichero `pixels.py` incluye una función `cargar_lienzo_desde_imagen(ruta_imagen)` que puede ser llamada para generar un lienzo desde una imagen en formato `.gif` o `.png` ser llamada al final de la ejecución para mantener la ventana del lienzo abierta. Esto asegurará que el lienzo permanezca visible hasta que el usuario cierre manualmente la ventana.

---

### Requisitos Opcionales (hasta 3.0 puntos) - Aún por terminar

En esta sección, los estudiantes deberán implementar funciones adicionales para mejorar la funcionalidad del lienzo y explorar conceptos avanzados de procesamiento de imágenes. Estas funciones están diseñadas para trabajar con la representación del lienzo como un diccionario, utilizando listas, tuplas y cálculos matemáticos.

---

#### **1. Efecto de Degradado**

Generar un degradado de colores que se extienda desde una esquina inicial hasta una esquina final del lienzo. El degradado debe calcular los valores RGB de forma lineal entre los colores proporcionados.

```python
def generar_degradado(ancho, alto, color_inicio, color_fin):
    """
    Genera un lienzo con un degradado entre dos colores.

    Args:
        ancho (int): Número de píxeles de ancho.
        alto (int): Número de píxeles de alto.
        color_inicio (tuple): Color inicial del degradado en formato RGB.
        color_fin (tuple): Color final del degradado en formato RGB.

    Returns:
        dict: Lienzo representado como un diccionario con claves 'width', 'height' y 'pixels'.
    """
    pass
```

**Detalles**:
- Los valores RGB de cada píxel se interpolarán linealmente desde `color_inicio` hasta `color_fin`.
- El degradado deberá cubrir todo el lienzo.

---

#### **2. Filtro de Contraste**

Ajustar el contraste del lienzo aumentando la diferencia entre los valores RGB de cada píxel y un valor de referencia (e.g., un promedio global o un valor intermedio). El contraste determina la diferencia entre las áreas claras y oscuras de una imagen, y este filtro permite aumentar o reducir dicha diferencia. El objetivo del filtro de contraste es modificar cada píxel del lienzo ajustando sus valores RGB según un factor de contraste. El ajuste se realiza tomando como referencia un valor medio (por ejemplo, 128). Un factor mayor que 1 incrementa el contraste, mientras que un factor menor que 1 lo reduce.

```python
def ajustar_contraste(lienzo, factor):
    """
    Ajusta el contraste de la textura.

    Args:
        lienzo (dict): Lienzo representado como un diccionario.
        factor (float): Factor de ajuste del contraste (e.g., 1.2 para aumentar, 0.8 para reducir).

    Returns:
        dict: Nuevo lienzo con el contraste ajustado.
    """
    medio = 128
    pass
```

**Ejemplo**:
- Lienzo Original:
    - Un lienzo con colores suaves y tonos similares.
```python
# Lienzo de ejemplo
lienzo = {
    "width": 5,
    "height": 5,
    "pixels": [
        [(100, 120, 140), (110, 130, 150), (120, 140, 160), (130, 150, 170), (140, 160, 180)],
        [(150, 170, 190), (160, 180, 200), (170, 190, 210), (180, 200, 220), (190, 210, 230)],
        [(200, 220, 240), (210, 230, 250), (220, 240, 255), (230, 250, 255), (240, 255, 255)],
        [(10, 20, 30), (20, 30, 40), (30, 40, 50), (40, 50, 60), (50, 60, 70)],
        [(60, 70, 80), (70, 80, 90), (80, 90, 100), (90, 100, 110), (100, 110, 120)],
    ],
}
```
- Contraste Aumentado (`factor = 1.5`):
    - Las áreas claras se vuelven más brillantes, y las oscuras, más oscuras.
- Contraste Reducido (`factor = 0.5`):
    - Los colores convergen hacia tonos medios, creando una imagen más homogénea.

**Detalles**:
- Un factor mayor que 1 aumenta el contraste; un factor menor que 1 lo reduce.
- El cálculo deberá ajustar cada componente RGB de acuerdo al siguiente esquema:
  ```python
  nuevo_valor = max(0, min(255, referencia + factor * (valor - referencia)))
  ```
  Donde `referencia` puede ser el brillo promedio de la imagen.

---

#### **3. Compresión de Color**

Reducir la cantidad de colores en el lienzo agrupando píxeles de colores similares en categorías definidas.

```python
def comprimir_colores(lienzo, niveles):
    """
    Reduce la cantidad de colores del lienzo agrupándolos en niveles definidos.

    Args:
        lienzo (dict): Lienzo representado como un diccionario.
        niveles (int): Número de niveles de color a mantener.

    Returns:
        dict: Nuevo lienzo con los colores comprimidos.
    """
    pass
```

**Detalles**:
- Los valores RGB se agruparán en niveles equidistantes.
- Por ejemplo, para `niveles=4`, los valores posibles para cada componente RGB serían `[0, 85, 170, 255]`.

---

#### **4. Generación de Textura Basada en Diccionarios**

Permitir al usuario definir un patrón específico proporcionando las posiciones y colores de ciertos píxeles en un diccionario. El resto del lienzo deberá rellenarse con un color de fondo.

```python
def generar_patron_custom(ancho, alto, configuracion, color_fondo):
    """
    Genera un lienzo según una configuración específica de posiciones y colores.

    Args:
        ancho (int): Ancho del lienzo.
        alto (int): Alto del lienzo.
        configuracion (dict): Diccionario con claves como tuplas (x, y) y valores RGB.
        color_fondo (tuple): Color de fondo del lienzo en formato RGB.

    Returns:
        dict: Lienzo representado como un diccionario.
    """
    pass
```

**Detalles**:
- `configuracion` será un diccionario donde las claves representen coordenadas `(x, y)` y los valores sean colores `(R, G, B)`.
- Los píxeles no definidos en `configuracion` se rellenarán con `color_fondo`.

---

#### **5. Tests Extras**

Creación de **tests adicionales** para las funcionalidades implementadas, así como para escenarios no cubiertos por los tests iniciales. Este apartado es opcional y permitirá explorar conceptos avanzados de validación y pruebas de software.

Requisitos:
1. **Archivo:** Los tests deben implementarse en un archivo llamado `tests/test_extra.py`, ubicado en el directorio raíz del proyecto.
2. **Estructura:** Se debe utilizar el módulo `unittest` para organizar los tests, siguiendo las mejores prácticas aprendidas en clase.
3. **Cobertura:** Los tests pueden incluir, pero no se limitan a:
   - Casos límite o extremos (e.g., tamaños de lienzo muy grandes o vacíos).
   - Validaciones adicionales para funciones opcionales o personalizadas.
   - Tests creativos que exploren escenarios complejos.
4. **Comentarios:** Cada test debe incluir comentarios que expliquen brevemente su propósito.

Ejemplo de Estructura:
```python
import unittest
from texturas import generar_degradado

class TestExtra(unittest.TestCase):
    def test_degradado_extremo(self):
        # Test para verificar degradado en un lienzo vacío
        resultado = generar_degradado(0, 0, (255, 0, 0), (0, 0, 255))
        self.assertEqual(resultado["width"], 0)
        self.assertEqual(resultado["height"], 0)
        self.assertEqual(resultado["pixels"], [])

if __name__ == "__main__":
    unittest.main()
```


#### **6. Otras funciones**

Además, puedes proponer tus propios requisitos opcionales. Si lo haces, háblalo con los profesores, para que te indiquesn aproximadamente cómo se van a valorar, y así puedas decidir mejor si te interesa o no realizarlos.

### Entrega

El proyecto final ha de ser entregado en un repositorio de acceso público o interno (no privado), creado bifurcando (forking) el repositorio de  plantilla. La entrega de la práctica se hará subiendo commits a ese repositorio git, como hemos hecho en prácticas anteriores.

La entrega deberá incluir un fichero `entrega.md`, que tendrá al menos los siguientes datos, en este orden:

* La primera línea del fichero ha de ser `# ENTREGA CONVOCATORIA ENERO`. La segunda, el nombre completo del estudiante, y su dirección de correo de la Universidad.
* Enlace al video de demostración del proyecto. El vídeo será de una duración máxima de 2:00 minutos si se presenta solo la parte básica, de 4:00 minutos si también se presenta la parte adicional. El video constará de una captura de pantalla que muestre cómo se lanzan las aplicaciones, cómo se ven alguna de las imágenes resultantes, y en general que muestre lo mejor posible la funcionalidad del proyecto. Siempre que sea posible, el alumno comentará en el audio del vídeo lo que vaya ocurriendo en él. El video se subirá a algún servicio de subida de vídeos en Internet (por ejemplo, Vimeo, Twitch, o YouTube).
* Apartado "`## Requisitos mínimos`": listado en formato [Markdown](https://www.markdownguide.org/basic-syntax/#lists-1) de los requisitos mínimos que se han implementado, indicando el nombre que aparece en el enunciado (por ejemplo "`generar_patron_aleatorio`").
* Apartado "`## Requisitos opcionales`": listado en formato [Markdown](https://www.markdownguide.org/basic-syntax/#lists-1) de los requisitos opcionales que se han implementado, indicando el nombre que aparece en el enunciado (por ejemplo "`generar_patron_custom`").
* Apartado "`## Requisitos opcionales propios`": listado en formato [Markdown](https://www.markdownguide.org/basic-syntax/#lists-1) de los requisitos opcionales que se han implementado que no están en el listado del enunciado. En este caso, para cada uno de ellos, incluir una breve descripción, que incluya cómo se pueden ejecutar. 
* Comentarios. Cualquier otro comentario que se quiera realizar sobre la práctica entregada.

Al terminar, comprueba en tu repositorio en EIF GitLab que están correctamente todos los ficheros que quieres entregar en el repositorio de entrega.
