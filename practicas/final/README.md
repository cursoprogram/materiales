# Proyecto final

Actividades:

* Presentacion "Generación y Manipulación de Texturas Visuales"

## Ejercicios

**Ejercicio a entregar:** Proyecto final (convocatoria de enero) - Generación y Manipulación de Texturas Visuales
  * **Fecha de entrega:** El día antes de la fecha del examen, 23:59.
  * [Repositorio plantilla](https://gitlab.eif.urjc.es/cursoprogram/plantillas2024/final/)
  * [Enunciado](enero/README.md).

