### Imagen

Este es un ejercicio para hacer procesamiento básico de imágenes. Para hacerlo, vamos a utilizar la funcionalidad que nos proporciona una nueva versiópn del fichero `pixels.py` del repositorio plantilla de este ejercicio, que es similar al de la práctica "Líneas", pero permite no solo pintar pixels sino también cargar una imagen en el lienzo, y leer el valor de pixels.

Para leer y escribir valores de/en el lienzo vamos a utilizar el formato RGB. Este formato se basa en descomponer un color en sus componentes roja (R, roja), verde (G, verde) y azul (B, azul). Expresaremos estas componentes con una tupla de tres números enteros, cada uno de ellos entre 0 y 255. El 0 representa la ausencia de ese component, y el 255 el máximo valor de ese component. Por ejemplo, el color rojo se expresará como (255, 0, 0), dado que tendrá el valor máximo para el component R, y no tendrá componentes G ni B. El color negro es el (0, 0, 0), y el blanco el (255, 255, 255). Puedes ver más información sobre el formato RGB para expresar colores en la [página RGB de Wikipedia](https://es.wikipedia.org/wiki/RGB).

Para realizar esta práctica, comienza leyendo (y entendiendo) el fichero `invert.py`. Ejecuta este fichero, y `pixels.py`, para ver cómo funcionan. Lee el código de `invert.py`, y las cadenas de documentación de `pixels.py`, para entender cómo funciona el módulo `pixels`. Ten en cuenta que `pixels.py` realiza una inversión sencilla de los colores de la imagen: para cada componente RGB del color de un pixel, resta su valor de 255. Con eso se consigue "invertir" los colores, transformando por ejemplo el negro en blanco y viceversa.

A continuación, escribe un programa `process.py` que tenga la siguiente funcionalidad y estructura:

* Para ejecutarlo, se pondrá el nombre del programa con tres argumentos: un fichero, en formato GIF, que será la imagen a procesar, un número real (float) que será un factor para el cambio de brillo, y un número entero (int, positivo o negativo) que será la variación para el desplazamiento de los colores. Por ejemplo, una ejecución para procesar el fichero `cafe.gif`, con cambio de brillo 1.5 y desplazamiento de colores 50, sería:

```commandline
python3 process.py cafe.gif 1.5 50
```

* El cambio de brillo se realizará con una función que acepte como parámetro el factor de cambio de brillo, y lo aplique a cada pixel de la imagen, multiplicando cada uno de sus componentes RGB por el factor. En los casos en que el producto de un número mayor que 255, se pondrá el valor 255. La signatura de esta función es la siguiente:

```python
def cambia_brillo(brillo: float):
```

* El desplazamiento se realizará con una función que acepte como parámetro la variación de desplazamiento de los colores, y lo aplique a cada pixel de la imagen, sumando a cada uno de sus componentes RGB el desplazamiento. La signatura de esta función es la siguiente:

```python
def cambia_desplazamiento(desplazamiento: int):
```

* También se realizará una función para leer los argumentos de la línea de comandos. Esta función devolverá el nombre del fichero con la imagen, el factor de cambio de brillo y el desplazamiento. La signatura de esta función es la siguiente:

```python
def lee_argumentos() -> tuple[str, float, int]:
```

* Y habrá una función `main()`, que ejecutará el programa principal. De hecho el programa ha de añadir, literalmente, el siguiente código:

```python
def main():
    imagen, brillo, desplazamiento = lee_argumentos()
    pixels.prepara(imagen)
    cambia_brillo(brillo)
    cambia_desplazamiento(desplazamiento)
    pixels.espera()


if __name__ == "__main__":
    main()
```

Prueba el programa, y comprueba que funciona bien para distintos valores de brillo y desplazamiento. Basta con que lo pruebes con los ficheros `cafe.gif` y `cafe_small.gif` que encontrarás en el repositorio de plantilla, pero puedes probarlos con otros ficheros en formato GIF.

Asegúrate de que el programa cumple con lo especificado en [PEP8](https://www.python.org/dev/peps/pep-0008/). Recuerda que puedes comprobarlo usando el programa `pycodestyle`, y que en PyCharm puedes usar la opción "Code | Reformat Code" para dar formato al código, de forma muy similar a como pide PEP8.

Mientras estés haciendo la práctica, no olvides hacer varios commits mientras la vayas realizando, y uno al final que incluya todo lo que quieras entregar. No olvides hacer un `push` al repositorio. Se valorará que haya más de cuatro commits realizados por ti.

**[Ejemplo de solución](process.py)**