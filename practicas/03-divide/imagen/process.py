#!/usr/bin/env python3

"""
Programa para procesar una imagen, usando el módulo pixels
"""

import sys

import pixels

def lee_argumentos() -> tuple[str, float, int]:
    if len(sys.argv) != 4:
        print("Uso: process.py <imagen> <brillo> <desplazamiento>")
        exit()

    imagen = sys.argv[1]
    brillo = float(sys.argv[2])
    desplazamiento = int(sys.argv[3])

    return imagen, brillo, desplazamiento

def cambia_brillo(brillo: float):
    for x in range(pixels.ancho):
        for y in range(pixels.alto):
            (r, g, b) = pixels.lee(x, y)
            r = min(255, int(r * brillo))
            g = min(255, int(g * brillo))
            b = min(255, int(b * brillo))
            pixels.pinta(x, y, (r, g, b))

def cambia_desplazamiento(desplazamiento: int):
    for x in range(pixels.ancho):
        for y in range(pixels.alto):
            (r, g, b) = pixels.lee(x, y)
            r = min(255, int(r + desplazamiento))
            r = max(0, r)
            g = min(255, int(g + desplazamiento))
            g = max(0, g)
            b = min(255, int(b + desplazamiento))
            b = max(0, b)
            pixels.pinta(x, y, (r, g, b))


def main():
    imagen, brillo, desplazamiento = lee_argumentos()
    pixels.prepara(imagen)
    cambia_brillo(brillo)
    cambia_desplazamiento(desplazamiento)
    pixels.espera()


if __name__ == "__main__":
    main()
