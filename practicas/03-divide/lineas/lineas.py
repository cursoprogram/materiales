#!/usr/bin/env python3

"""
Programa para pintar lineas en un lienzo, usando pixels
"""

import sys

from pixels import tam_lienzo, pinta, espera


def lee_lineas():
    argumentos = sys.argv[1:]
    if len(argumentos) == 0:
        print("No hay lineas a pintar")
        exit()
    elif len(argumentos) % 4 != 0:
        print("Para cada linea hacen falta cuatro parámetros: 'h'|'v' x y l")
        exit()
    else:
        lineas = []
        for i in range(0, len(argumentos), 4):
            dir = argumentos[i]
            if dir == "h":
                dir = "horizontal"
            elif dir == "v":
                dir = "vertical"
            else:
                print("La dirección debe ser 'h' o 'v'")
                exit()
            (x, y) = (int(argumentos[i + 1]), int(argumentos[i + 2]))
            l = int(argumentos[i + 3])
            lineas.append((dir, x, y, l))
        return lineas


def pinta_horizontal(coordenadas, longitud,
                     ancho, color="black"):
    (coord_x, coord_y) = coordenadas
    (x, y) = (coord_x, coord_y)
    while x < ancho and x < coord_x + longitud:
        pinta(x, y, color)
        x = x + 1
    if x == coord_x + longitud:
        return (True)
    else:
        return (False)


def pinta_vertical(coordenadas, longitud,
                   alto, color="black"):
    (coord_x, coord_y) = coordenadas
    (x, y) = (coord_x, coord_y)
    while y < alto and y < coord_y + longitud:
        pinta(x, y, color)
        y = y + 1
    if y == coord_y + longitud:
        return (True)
    else:
        return (False)


def pinta_linea(coordenadas, longitud, dirección, ancho,
                alto, color="black"):
    (coord_x, coord_y) = coordenadas
    if dirección == "horizontal":
        if coord_y < 0 or coord_y >= alto or coord_x < 0:
            return (False)
        else:
            result = pinta_horizontal(coordenadas, longitud,
                                      ancho, color)
            return (result)
    elif dirección == "vertical":
        if coord_x < 0 or coord_x >= ancho and coord_y < 0:
            return (False)
        else:
            result = pinta_vertical(coordenadas, longitud,
                                    alto, color)
            return (result)


def main():
    """Programa principal"""

    lineas = lee_lineas()
    (ancho, alto) = tam_lienzo()
    for linea in lineas:
        (dir, x, y, l) = linea
        result = pinta_linea((x, y), l, dir, ancho, alto)
        print(result)
    espera()


if __name__ == "__main__":
    main()
