### Pinta líneas

El fichero `pixels.py` del repositorio plantilla es el mismo de la práctica "Tablero de ajedrez":

Hay que completar el fichero `lineas.py` del repositorio plantilla, usando `pixels.py` como módulo para pintar líneas. Solo se utilizará `pixels.py`, sin hacer ninguna llamada directa a `turtle` ni a otros módulos.

En este fichero `lineas.py` tendrás que escribir el código que falta en las funciones lee_lineas(), pinta_horizontal(), pinta_vertical() y pinta_linea(), de forma que acepte como argumentos grupos de cuatro parámetros. Cada grupo corresponderá con una línea recta a pintar. Los parámetros de cada línea serán:

* Una letra `h` o `v`, para indicar si la línea es horizontal o vertical.
* Dos números enteros, que corresponderán con las coordenadas X e Y donde habrá que comenzar a pintar la línea.
* Un número entero, que corresponderá con la longitud de la línea. Si la línea es horizontal, se pintará a partir de las coordenadas X e Y hacia la derecha. Si la línea es vertical, se pintará a partir de las coordenadas X e Y hacia abajo. Recuerda que las coordenadas (0, 0) son las de la esquina superior izquierda, y las coordenadas X crecen hacia la derecha, y las coordenadas Y crecen hacia abajo.

Por ejemplo:

```commandline
python3 lineas.py h 0 0 10
```

Dibujará una línea horizontal, comenzando en (0, 0) y acabando en (10, 0).

```commandline
python3 lineas.py v 0 0 10 h 5 3 10
```

Dibujará dos líneas, una vertical comenzando en (0, 0) y acabando en (0, 10), y una horizontal comenzando en (5, 3) y acabando en (5, 13).

Además, cada una de las siguientes funciones ha de cumplir algunos detalles:

* `lee_lineas()`: Lee los argumentos que el usuario haya escrito en la línea de comandos, y devuelve una lista de grupos de cuatro parámetros. Cada grupo de parámetros corresponderá con una línea recta a pintar, y será una tupla. En cada tupla, el primer elemento será la cadena `horizontal` o `vertical` (según el argumento original sea `h` o `v`), el segundo elemento es un número entero, que corresponderá con las coordenadas X de la línea, el tercero es un número entero, que corresponderá con las coordenadas Y de la línea, y el cuarto correspondorá con la longitud de la línea.

* `pinta_linea()`: Devolverá `True` si la línea se pudo pintar bien, pero `False` si la línea no se pudo pintar bien. Por ejemplo, si el usuario introduce un origen que está fuera del lienzo, o si pide pintar una línea que se "sale" del lienzo. Si el origen está fuera del lienzo, además no pintará la línea.

* `pinta_horizontal()`: Pintará una línea horizonta. Devolverá `True` si la línea horizontal se pudo pintar bien, pero `False` si la línea no se pudo pintar bien. Pintará la parte de la línea que "quepa" en el lienzo. Recibirá como parámetros las coordenadas de comienzo y la longitud, el ancho (grosor)  y el color de la línea.

* `pinta_vertical()`: Pintará una línea vertical. Devolverá `True` si la línea vertical se pudo pintar bien, pero `False` si la línea no se pudo pintar bien. Pintará la parte de la línea que "quepa" en el lienzo. Recibirá como parámetros las coordenadas de comienzo y la longitud, el ancho (grosor)  y el color de la línea.

**[Ejemplo de solución](lineas.py)**

