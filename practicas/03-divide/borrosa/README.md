### Imagen borrosa

En esta práctica, vamos a crear un programa de difusión de imágenes utilizando el algoritmo de promedio de colores de píxeles vecinos. En otras palabras, vamos a hacer que la imagen sea más borrosa (difuminada) que el original.

El programa ha de cumplir los siguientes requisitos:

* El programa debe recibir como entrada el nombre de un archivo de imagen (que será un fichero en formato GIF) y el número de etapas de difusión. Deberá detectar que no se ha puesto un número de argumentos correcto, que el argumento correspondiente al nombre del archivo de imagen no corresponde a un archivo que exista, y que el argumento que corresponde al número de etapas es un entero, mostrando en cada caso un mensaje de ayuda apropiado, y terminando el programa. La inexistencia del archivo se detectará capturando la excepción `FileNotFoundError` (que será levantada por `pixels.preparar()`), y el que el número no sea entero capturando la excepción `ValueError` (que será levantada al convertir el argumento a entero).
* El programa usará las funciones del módulo `pixels.py` para leer, manipular y mostrar imágenes. Este módulo se proporciona en el repositorio de plantilla de la práctica. 
* El programa debe aplicar el algoritmo de difusión de manera iterativa (el número de veces que se haya indicado como número de etapas), promediando los colores de los píxeles vecinos en cada etapa. Se consideran pixels vecinos los cuatro pixels que se encuentran inmediatamente encima, debajo, a la izquierda y a la derecha del pixel dado. En caso de que no haya alguno de estos pixels (porque el pixel dado está en un borde de la imagen), se detectará usando la excepción `IndexError` (que será levantada por `pixels.lee()`).
* Cada "etapa de difusión" consistirá en aplicar el algoritmo anterior a todos los pixels de la imagen, una sola vez. Cada vez que el programa inicie una etapa, mostrará el mensaje "Etapa N". El programa debe, además, mostrar la imagen difuminada (actualizando la imagen de la etapa anterior) después de cada etapa, usando `pixels.actualiza()` o `pixels.espera()`, al final del todo.

Ejemplos de uso:

```
$ python difumina.py cafe.gif hola
Número de etapas debe ser un entero: hola
Uso: difumina.py <imagen> <etapas>
$ python difumina.py no_existe.gif 5
Fichero de imagen no encontrado: no_existe.gif
Uso: difumina.py <imagen> <etapas>
$ python difumina.py cafe.gif 5
Etapa 1
Etapa 2
Etapa 3
Etapa 4
Etapa 5
```

Estructura del programa:

* El programa principal se escribirá en una función `main()`, que será a su vez llamada desde un `if` que solo se ejecutará cuando el progrma se lance como programa principal.
* La función `main()` tendrá la siguiente estructura:

```python
    imagen, etapas = lee_argumentos()
    try:
        pixels.prepara(imagen)
    ...
    for etapa in range(etapas):
        pixels.actualiza()
        print(f"Etapa {etapa + 1}")
        emborrona()
    pixels.espera()

```

* La función `lee_argumentos()` se encargará de leer los argumentos de la línea de comandos.
* La función `emborrona()` se encargará de difuminar (hacer borrosa) la imagen, usando las funciones del módulo `pixels.py`para calcular el nuevo valor de los colores de cada pixel, en función de sus pixels vecinos.

Entrega:

* El programa debe llamarse `difumina.py`.
* El programa debe estar en un repositorio en el GitLab de la EIF, creado como un fork del repositorio plantilla de esta práctica.
* Prueba el programa, y comprueba que funciona bien para distintos valores de número de etapas. Basta con que lo pruebes con los ficheros `cafe.gif` y `cafe_small.gif` que encontrarás en el repositorio de plantilla, pero puedes probarlos con otros ficheros en formato GIF.
* Asegúrate de que el programa cumple con lo especificado en [PEP8](https://www.python.org/dev/peps/pep-0008/). Recuerda que puedes comprobarlo usando el programa `pycodestyle`, y que en PyCharm puedes usar la opción "Code | Reformat Code" para dar formato al código, de forma muy similar a como pide PEP8.
* Mientras estés haciendo la práctica, no olvides hacer varios commits mientras la vayas realizando, y uno al final que incluya todo lo que quieras entregar. No olvides hacer un `push` al repositorio. Se valorará que haya más de cuatro commits realizados por ti.

**[Ejemplo de solución](difumina.py)**