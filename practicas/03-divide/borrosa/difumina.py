#! /usr/bin/env python3

import sys

import pixels

uso = "Uso: process.py <imagen> <etapas>"


def lee_argumentos() -> tuple[str, int]:
    if len(sys.argv) != 3:
        print(uso)
        exit()

    imagen = sys.argv[1]
    try:
        etapas = int(sys.argv[2])
    except ValueError:
        print(f"Número de etapas debe ser un entero: {sys.argv[2]}")
        print(uso)
        exit()
    return imagen, etapas


def emborrona():
    for x in range(pixels.ancho):
        for y in range(pixels.alto):
            colores = []
            vecinos = [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]
            for vecino in vecinos:
                try:
                    colores.append(pixels.lee(vecino[0], vecino[1]))
                except IndexError:
                    continue
            suma_colores = (0, 0, 0)
            for color in colores:
                suma_colores = (suma_colores[0] + color[0],
                                suma_colores[1] + color[1],
                                suma_colores[2] + color[2])
            media_colores = (suma_colores[0] // len(colores),
                             suma_colores[1] // len(colores),
                             suma_colores[2] // len(colores))
            pixels.pinta(x, y, media_colores)


def main():
    imagen, etapas = lee_argumentos()
    try:
        pixels.prepara(imagen)
    except FileNotFoundError:
        print(f"Fichero de imagen no encontrado: {imagen}")
        print(uso)
        exit()
    for etapa in range(etapas):
        pixels.actualiza()
        print(f"Etapa {etapa + 1}")
        emborrona()
    pixels.espera()


if __name__ == "__main__":
    main()
