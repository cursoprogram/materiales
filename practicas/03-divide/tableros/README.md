### Tablero sencillo

Explora el fichero `pixels.py` del repositorio de plantilla de la práctica "Tablero de ajedrez":

* Pruébalo, ejecutando un programa que lo usa como módulo:

```shell
python3 tablero_simple.py
```

* Fíjate en su estructura:
  * Tiene un `if` al final que controla qué se ejecuta cuando se le lanza como programa, y qué se ejecuta cuando le importa otro módulo
  * Tiene una función `main()` que es el programa principal. Date cuenta de que ese nombre no es especial, se podría llamar de cualquier manera, lo importante es que se le llame solo cuando el fichero se lanza como programa.
  * Tiene una función `prepara_tortuga()`, que configura la tortuga para que funcione la función `pinta()`. El propio programa llama a esta función antes de que se pueda llamar a `pinta()`, así que en realidad no hace falta llamarla desde `main()` o cuando se use como módulo.
  * Tiene una función `pinta()` que pinta un "pixel" de un color. Un pixel es la unidad menor que podemos pintar de un color. El programa tiene una variable que controla qué dimensiones tiene el pixel (en términos de "unidades de tortuga").
  * Tiene una función `espera()` que hemos de llamar al terminar de pintar, para poder ver lo que hemos pintado.

* Modifícalo (cambiando solo la función `main()`) para que en lugar de lo que pinta pinte, un pixel negro (`black`), uno rojo debajo de él (`red`), uno verde debajo de él (`green`), uno blanco debajo de él (`white`), y uno azul debajo de él (`blue`).

Explora ahora el fichero `tablero_simple.py`:

* Ejecútalo
* Fíjate en su estructura:
  * De nuevo tiene un `if` para controlar qué se ejecuta cuando se le llama como programa. Verás que ejecuta `main()` cuando se le llama como programa, y que no ejecuta nada cuando se importa como módulo.
  * Fíjate cómo utiliza las funciones definidas en `pixels,py`.
  * Fíjate cómo hace para pintar dos cuadrados.
* Cambia su función main() para que pinte un rectángulo horizontal rojo, de 10 pixels de ancho y 3 de alto, y otro negre, de las mismas dimensiones, justo encima de él.

### Tablero ajedrez

Importando el programa `pixels.py` como módulo, y sus funciones `pintar()' y `espera()`, escribe un programa que pinte un tablero de ajedrez con casillas negras y rojas, como el siguiente:

![Tablero de ajedrez](tablero_ajedrez.png)

Solo se utilizará `pixels.py`, sin hacer ninguna llamada directa a `turtle` ni a otros módulos.

El programa tendrá una función `casilla()`, que tendrá la siguiente signatura:

```python
def casilla(pos_x: int, pos_y: int, color: str):
    """Pinta una casilla del tablero

    La pinta en la posición (`pos_x`, `pos_y`), en coordinadas del
    tablero, y del color indicado.
    """
```

Esta función utlizará `pinta()`, de `pixels.py` para pintar los pixels de una casilla.

El programa tendrá también una función `main()`, que llamará a la anterior, para pintar el tablero de ajedrez (que es cuadrado, con ocho casillas de lado), y a `espera()` al terminar.

Utiliza `if __name__ == "__main__"` para llamr a `main()` cuando el fichero se ejecute como programa principal, y una variable `tam_casilla` (que valdrá `3`) para definir el tamaño de la casilla, en pixels. 

**[Ejemplo de solución](tablero_ajedrez.py)**
