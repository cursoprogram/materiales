#!/usr/bin/env python3

"""Pinta un tablero de ajedrez de dos colores"""

from pixels import *

tam_casilla = 3


def casilla(pos_x: int, pos_y: int, color: str):
    """Pinta una casilla del tablero

    La pinta en la posición (`pos_x`, `pos_y`), en coordinadas del
    tablero, y del color indicado.
    """

    for x in range(0, tam_casilla):
        for y in range(0, tam_casilla):
            pinta(pos_x * tam_casilla + x,
                  pos_y * tam_casilla + y,
                  color)


def main():
    """Programa principal"""
    for pos_x in range(0, 8):
        for pos_y in range(0, 8):
            x_espar = ((pos_x % 2) == 0)
            y_espar = ((pos_y % 2) == 0)
            if x_espar == y_espar:
                color = 'red'
            else:
                color = 'black'
            casilla(pos_x, pos_y, color)
    espera()


if __name__ == "__main__":
    main()
