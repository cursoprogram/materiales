import sys

from pixels import pinta, espera, tam_lienzo

def lee_casillas():
    argumentos = sys.argv
    if len(argumentos) == 1:
        sys.exit("No se han proporcionado pixels para pintar")
    coordenadas = argumentos[1:]
    if len(coordenadas) % 2 != 0:
        sys.exit("Se han proporcionado mal las coordenadas")

    casillas = []
    for i in range(0, len(coordenadas), 2):
        x = int(coordenadas[i])
        y = int(coordenadas[i + 1])
        casillas.append((x, y))
    return casillas

def prepara_mundo(ancho, alto, casillas):
    mundo = []
    for x in range(0, ancho):
        mundo.append([False] * alto)
    for (x, y) in casillas:
        mundo[x][y] = True
    return mundo

def pinta_mundo(mundo):
    ancho = len(mundo)
    alto = len(mundo[0])
    for x in range(0, ancho):
        for y in range(0, alto):
            if mundo[x][y]:
                pinta(x, y, "green")

def main():
    """Programa principal"""

    casillas = lee_casillas()
    (ancho, alto) = tam_lienzo()
    mundo = prepara_mundo(ancho, alto, casillas)
    pinta_mundo(mundo)
    espera()

if __name__ == "__main__":
    main()