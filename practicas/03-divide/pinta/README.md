### Pinta pixels

Explora el fichero `pixels.py` del repositorio de plantilla de esta práctica.

* Pruébalo, ejecutándolo:

```shell
python3 pixels.py
```

* Fíjate en su estructura:
  * Tiene un `if` al final que controla qué se ejecuta cuando se le lanza como programa, y qué se ejecuta cuando le importa otro módulo
  * Tiene una función `main()` que es el programa principal. Date cuenta de que ese nombre no es especial, se podría llamar de cualquier manera, lo importante es que se le llame solo cuando el fichero se lanza como programa.
  * Tiene una función `prepara()`, que configura un lienzo (canvas) para que funcione la función `pinta()`. El propio programa llama a esta función antes de que se pueda llamar a `pinta()`, así que en realidad no hace falta llamarla desde `main()` o cuando se use como módulo.
  * Tiene una función `pinta()` que pinta un "pixel" de un color. Un pixel es la unidad menor que podemos pintar de un color. El programa tiene una variable que controla qué dimensiones tiene el pixel (en términos de "unidades de lienzo").
  * Tiene una función `espera()` que hemos de llamar al terminar de pintar, para poder ver lo que hemos pintado.

* Modifícalo (cambiando solo la función `main()`) para que, en lugar de lo que pinta, pinte un pixel verde (`green`) en la posición (3, 3) y uno rojo (`red`) en la posición (3, 4).
* Date cuenta de que la esquina superior tiene las coordenadas (0, 0), y las coordenadas X crecen hacia la derecha, y las coordenadas Y crecen hacia abajo.

Explora ahora el fichero `pinta.py`:

* Es la estructura del programa que tendrás que construir.

* Tiene una función `lee_casillas()` que leerá las coordenadas de las casillas que queramos pintar. Estas coordenadas vendrán en la variable `sys.argv` (los argumentos de la línea de comandos). Las coordenadas vendrán en parejas, primero la coordenada X y luego la Y, separadas por espacios, y separadas por espacios de otras coordenadas. Esta función devolverá una lista de pares de coordenadas. La función comprobará también que haya al menos un argumento, y que el número de argumentos sea par (para poder agruparlo en coordenadas X e Y).

Por lo tanto, el programa se podrá ejecutar de las siguientes formas:

```commandline
python3 pinta.py 3 3 3 4
python3 pinta.py 3 3 3 4 5 6
python3 pinta.py 3 3
```

Cuando se ejecute, por ejemplo, de la primera de esas formas, la función `lee_casillas` devolverá la siguiente lista de pares de coordenadas:

```
[(3, 3), (3, 4)]
```

Si el programa se ejecuta de las siguientes formas, dará error:

```commandline
python3 pinta.py
python3 pinta.py 3 3 3
```

* Tiene una función `prepara_mundo()`, que recibe como parámetros el ancho y el alto del mundo (en pixels), y las casillas que queremos pintar. Pero esta función no pintará las casillas, sino que praprará una lista de listas, `mundo`, que devolverá a quien la llame. Cada elemento de esta lista de listas representará una posición del mundo, que valdrá `True` si corresponde con una casilla que queremos pintar, y `False` si corresponde con una casilla que no queremos pintar. Cada una de las listas que contiene la lista `mundo` corresponderá con una fila vertical de casillas, y por tanto será una lista de booleanos. El índice de la lista cooresponderá con la coordenada X de las casillas que representa la lista. El índice dentro de cada una estas listas corresponderá con la coordenada Y de las casillas que representa la columna. Por lo tanto, la casilla (X, Y) será representada por `mundo[x][y]`. Por ejemplo, si el mundo fuera de 4x3 (4 pixels de ancho y 3 de alto), la casilla (3, 2), la de la esquina inferior derecha, se representaría por `mundo[3][2]`. Y si quisiéramos una versión de ese mundo en el que solo se pintase esa casilla, `mundo` sería:

```
[[False, False, False], [False, False, False], [False, False, False], [False, False, True]]
```

* Tiene una función `pinta_mundo()`, que recibe como parámetro una variable `mundo` como la que devuelve la función anterior, y pinta este mundo en el lienzo: pinta, en color verde (`green`) las casillas que estén como `True`.

* Tiene una función `main()` que llama a las anteriores para pintar las casillas que el usuario haya indicado como argumentos en la línea de comandos.

Tu trabajo en esta práctica es implementar todo lo que falta para que funcionen las funciones anteriores (y todoel programa) tal y como se ha explicado.

**[Ejemplo de solución](pinta.py)**
