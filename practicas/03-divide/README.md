# Divide y vencerás

Actividades:

* Funciones como forma de dividir un problema en partes más sencillas
* Signatura de una función
* Llamada a una función
* Módulos
* Ejecución como programa principal y como módulo
* Valor de vuelta de una función
* Ámbitos de variables

## Ejercicios

**Ejercicio:** Tablero sencillo.
  * Ejercicio realizado en clase
  * [Enunciado](tableros/README.md)

**Ejercicio:** Tablero de ajedrez
  * Ejercicio de entrega en GitLab
  * **Fecha de entrega:** 17 de octubre de 2024
  * [Repositorio plantilla](https://gitlab.eif.urjc.es/cursoprogram/plantillas2024/tableros/)
  * [Enunciado](tableros/README.md)

**Ejercicio:** Pinta pixels
  * Ejercicio de entrega en GitLab
  * **Fecha de entrega:** 24 de octubre de 2024
  * [Repositorio plantilla](https://gitlab.eif.urjc.es/cursoprogram/plantillas2024/pinta/)
  * [Enunciado](pinta/README.md)

**Ejercicio:** Pinta Líneas
  * Ejercicio de entrega en GitLab
  * **Fecha de entrega:** 29 de octubre de 2024
  * [Repositorio plantilla](https://gitlab.eif.urjc.es/cursoprogram/plantillas2024/lineas/)
  * [Enunciado](lineas/README.md)

**Ejercicio:** Imagen
  * Ejercicio de entrega en GitLab
  * **Fecha de entrega:** 31 de octubre de 2024
  * [Repositorio plantilla](https://gitlab.eif.urjc.es/cursoprogram/plantillas2024/imagen/)
  * [Enunciado](imagen/README.md)

**Ejercicio:** Imagen borrosa
  * Ejercicio de entrega en GitLab
  * **Fecha de entrega:** 5 de noviembre de 2024
  * [Repositorio plantilla](https://gitlab.eif.urjc.es/cursoprogram/plantillas2024/borrosa/)
  * [Enunciado](borrosa/README.md)
