### Operaciones

Este ejercicio es simplemente uno de los muchos posibles para empezar a familiarizarse con PyCharm y el depurador.

* Crea en PyCharm un fichero, `operaciones.py`
* Escribe código para escribir, primero, el resultado de sumar 2 y 2, luego, el resultado de multiplicar 3 por 4 (usando la función `print()`.
* El programa deberá mostrar en una línea el resultado de la suma, y en la línea siguiente, el resultado de la multiplicación.
* Ejecútalo, y usa el depurador con él
* Ejemplo de solución: [operations.py](operations.py)
