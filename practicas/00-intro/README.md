# Introducción

Introducción a Python y al entorno de trabajo en el laboratorio

## Actividades

* Presentación del módulo "turtle" de Python.
  * Ejercicios: Tortuga.
* Presentación: Motivación
  * Transparencias, tema "Motivación y fundamentos"
* Distintas formas de ejecutar programas Python
  * Ejercios: Ejecuciones.
* El IDE (Integrated Development Environment): PyCharm
* Presentación: Conceptos básicos de informática
  * Transparencias, tema "Introducción"
* Depuración en PyCharm
* Introducción a condicionales y bucles
  * Ejercicios: Condicionales y bucles
* Presentación: Conceptos básicos de Python
  * Transparencias, tema "Introducción"
* Introducción a enteros, caracteres y listas
  * Ejercicios: Listas y otros tipos

## El entorno de trabajo Linux

* Presentación: los laboratorios Linux de la Escuela.
* Creación de cuentas para los laboratorios Linux (uso de móvil o cuenta alumno / eif).
* Prueba de cuenta en uno de los ordenadores.

## El IDE (Integrated Development Environment): PyCharm

Actividades:

* Lanzamiento de PyCharm
* Apertura de un proyecto
* El terminal de PyCharm
* El intérprete de Python de PyCharm. Python como calculadora (2)
* Primer script de Python
* Ejecución

Referencias:

* [PyCharm: Get Started](https://www.jetbrains.com/help/pycharm/quick-start-guide.html#code-assistance)
* [PyCharm: First Steps](https://www.jetbrains.com/help/pycharm/creating-and-running-your-first-python-project.html)

## Depuración (debugging)

Actividades:

* Explicación del depurador:
  * Puntos de parada, ejecución paso a paso
  * Variables y ámbitos
* Ejercicio con el depurador (usando por ejemplo [operations.py](operations.py))

Referencias:

* [PyCharm: Debugging Python Code](https://www.jetbrains.com/help/pycharm/part-1-debugging-python-code.html)

## Ejercicios

**Ejercicio:** Tortuga
  * Ejercicio de entrega en el foro
  * **Fecha de entrega:** 17 de septiembre
  * [Enunciado](tortuga/README.md)

**Ejercicio:** Ejecuciones
  * Ejercicio de entrega en el foro
  * **Fecha de entrega:** 19 de septiembre
  * [Enunciado](ejecuciones/README.md)

**Ejercicio:** Condicionales y bucles
  * Ejercicio de entrega en el foro
  * **Fecha de entrega:** 24 de septiembre
  * [Enunciado](cond-bucles/README.md)

**Ejercicio:** Listas y otros tipos
  * Ejercicio de entrega en el foro
  * **Fecha de entrega:** 26 de septiembre
  * [Enunciado](listas/README.md)

**Ejercicio:** Días, horas, minutos y segundos.
  * Ejercicio recomendado
  * [Enunciado](segundos/README.md)

**Ejercicio:** Operaciones matemáticas simples.
  * Ejercicio recomendado
  * [Enunciado](operaciones/README.md)

## Materiales complementarios

* [Soluciones a ejercicios en transparencias](soluciones)
* [Ejemplos de programas](ejemplos)
