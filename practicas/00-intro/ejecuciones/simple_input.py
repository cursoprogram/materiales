#!/usr/bin/env python3

"""
Pregunta al usuario, y haz una línea según te indique
"""
from turtle import *

opcion = input("Dirección (A: arriba, D: derecha): ")
if opcion == 'A':
    left(90)
    forward(100)
elif opcion == 'B':
    forward(100)
done()
