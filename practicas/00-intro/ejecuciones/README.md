### Distintas formas de ejecutar un programa

Vamos a explorar distintas formas de ejecutar un programa Python

Comenzaremos con un programa que use el módulo 'turtle':

* Ejecución en sitio web

* Ejecución desde terminal, invocando con python:
  * Crea un fichero con cualquier editor de texto plano
  * Cámbiate al directorio donde está, y ejecútalo invocándolo con python:
```commandline
cd <directorio_donde_está_el_fichero>
python3 simple.py
```

* Ejecución desde terminal, directamente:
  * Añade la primera línea para ejecución:
```python
#!/usr/bin/python3
```

```python
#!/usr/bin/env python3
```
  * Da permisos de ejecución al programa (en línea de comandos, o mediante gestor gráfico de ficheros):
```commandline
cd <directorio_donde_está_el_fichero>
chmod u+x simple.py
```

  * Ejecuta directamente:
```commandline
./simple.py
```

Ahora, vamos a incluir un poco de interacción con el usuario:

* Ejecuta desde el terminal, preguntando al usuario:
  * Haz todo lo necesario y...
```commandline
python3 simple_input.py
```

* Ejecuta desde PyCharm
  * Se lanza PyCharm
  * Se abre un nuevo proyecto (opción "Open")
  * Se crea un fichero simple_input.py en él
  * Se ejecuta el fichero

* Ejecuta desde el terminal de PyCharm

* Ejecuta desde el intérprete de Python de Pycharm

* Ejecuta en el depurador de PyCharm

Ejercicio de entrega:

Crea un programa Python, usando el módulo turtle, que al arrancar ofrezca la opción de que el usuario pulse una letra (y a continuación la tecla ENTER). Una vez el usuario la haya pulsado, si es una de las letras consideradas, la pinta usando turtle. Si no, pinta un cuadrado. Un ejemplo de ejecución sería:
```commandline
python3 letras.py
¿Qué letra quieres pintar? (solo sé pintar la E, J y la T): E
[...el programa pinta una E usando turtle...]
```

Documentación adicional:

* [How to Run Your Python Scripts and Code](https://realpython.com/run-python-scripts/)
* [PyCharm: Run and rerun applications](https://www.jetbrains.com/help/pycharm/running-applications.html)
* [Python if elif else](https://www.datacamp.com/tutorial/python-if-elif-else)
