### Días, horas, minutos y segundos

Escribe un programa en Python3 que lea (de teclado) una cantidad de segundos y los convierta en días, horas, minutos y segundos. El programa, al arrancar, mostrará un mensaje en en pantalla:

```
Número de segundos:
```

y esperará que el usuario escriba un número entero, y la tecla ·"Enter". A continuación escribirá:

```
xxx segundos son ddd día(s), hhh hora(s), mmm minuto(s) y sss segundo(s)
```

Por ejemplo, si se introduce el número 90061, el programa escribirá:

```
90061 segundos son 1 día(s), 1 hora(s), 1 minuto(s) y 1 segundo(s)
```

En la cabecera del programa, pon un comentario que indique qué hace el programa, y el nombre de quien lo ha escrito.

* [Solución](segundos.py)
* [Solución con funciones y tests](segundos2.py) (para probar tests)
* [Solución con funciones y tipos](segundos3.py) (para probar MyPy)
