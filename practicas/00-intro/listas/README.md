### Listas y otros tipos de variables

Enteros (int):

```python
var = 6
var2 = var + 6
var3 = var2 * var
print(var, var2, var3)
```

Cadenas de caracteres (strings, str):

```python
texto = "¡Hola, qué tal!"
texto2 = '¡Hola, qué tal!'
texto3 = texto + texto2
letra = "a"
texto4 = texto3 + " " + letra
print(texto)
print(texto2)
print(texto3)
print(texto4)
numero = int("10")
res1 = "10" + 3
res2 = numero + 3
print(res1, res2)
```

Listas:

```python
lista1 = [1, 2, 3, 4]
lista2 = ["rojo", "azul", "verde"]
print(lista1)
print(lista2)
print(lista1[1])
lista1[0] = "uno"
print(lista1)
```

Módulo `webbrowser`:

```python
from webbrowser import *

open("https://gsyc.urjc.es")
```
Ejercicio de entrega:

Crea un programa Python, usando el módulo webbrowser, que al arrancar muestre como opciones, en pantalla, una cuantas URLs, cada una con un número para poder elegirla. A continuación, pedirá un número al usuario, y cargará en el navegador la URL que se haya elegido. Por ejemplo:

```commandline
python3 cargaurl.py
1: https://www.urjc.es
2: https://gsyc.urjc.es
3: https://labs.eif.urjc.es
¿Qué página quieres cargar? (1-3): 2
[carga la página https://gsyc.urjc.es en el navegador]
```


Documentación adicional:

* [Dive into Python: Native Datatypes](https://diveintopython3.net/native-datatypes.html)
* [Python built-in types](https://docs.python.org/3/library/stdtypes.html)
* [Python numberic types](https://docs.python.org/3/library/stdtypes.html#numeric-types-int-float-complex)
* [Python text sequence type (str)](https://docs.python.org/3/library/stdtypes.html#text-sequence-type-str)
* [Diving into Python: Strings](https://diveintopython3.net/strings.html)
* [Python lists](https://docs.python.org/3/library/stdtypes.html#lists)