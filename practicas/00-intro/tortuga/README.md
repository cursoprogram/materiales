### Juega con la tortuga

Vamos a utlizar un sitio web con tortugas animadas por Python: [Pythonandturtle.com](https://pythonandturtle.com/turtle/). También puedes usar [Trinket.io](https://trinket.io/python)

* Haz un programa que muestre una línea horizontal.
  * [Solución](simple.py)
* Haz un programa que muestre un cuadrado.
* Haz un programa que muestre un triángulo y ponga el mensaje "Hola, soy un triángulo"
* Haz un programa que dibuje una casita (si es posible con una puerta, una ventana, y una chimenea)

Para todo lo anterior, puedes centrarte en usar:

* forward(), left(), right()
* color(), width()
* fillcolor()
* showturtle(), hideturtle()
* home()
* clearscreen()
* write()
* done()

Luego, haremos lo mismo en Pycharm, en un fichero, en el intérprete de Python...

Documentación a utilizar:

* Python manual: [Turtle graphics](https://docs.python.org/3/library/turtle.html), sobre todo el principio ("Tutorial") y la ["Turtle Graphics Reference"](https://docs.python.org/3/library/turtle.html#turtle-graphics-reference)
* Manual de Python en español: [Gráficos con turtle](https://docs.python.org/es/3.10/library/turtle.html)
* [Real Python: Beginners Guide to Python Turtle](https://realpython.com/beginners-guide-python-turtle/): Tutorial paso a paso, pero con un enfoque diferente, más enfocado a la interfaz orientada a objetos que proporciona turtle (y que por ahora no veremos).