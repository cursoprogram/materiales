#!/usr/bin/env python3

from turtle import *

for linea in range(5):
    right(30)
    forward(100)
    right(90)
    forward(10)
    right(90)
    forward(100)

done()
