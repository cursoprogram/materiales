### Condicionales y bucles

Booleanos: pueden tener valor cierto (True) o falso (False).

Por ejemplo, el resultado de evaluar todas estas expresiones es un booleano:

```python
True
False
1 == 2
1 != 2
a == "A"
a == 2
a >= 2
```

Estructura de un condicional if-elif-else:

```python
if <condicion1>:
    <instrucciones1>
elif <condicion2>:
    <instrucciones2>
...
else:
    <instruccciones>
```

Las condiciones pueden ser cualquier cosa que sea evaluada a booleano.
Puede no haber `elif`, y no haber `else`.

Estructura de un bucle for:

```python
for valor in <enumerado>:
    <instrucciones
```

Cada "vuelta" del bucle, `valor` vale un valor del enumerado.

Ejemplo de enumerado: `range(10)`, da valores entre 0 y 9.

```python
for valor in range(10):
    print(valor)
```

Ejercicio de entrega:

Crea un programa Python, usando el módulo turtle, que dibuje una figura geométrica usando al menos un bucle, y si es posible, también un condicional.

Documentación adicional:

* [Booleans en "Dive into Python"](https://diveintopython3.net/native-datatypes.html#booleans)
* [If en el manual de referncia de Python](https://docs.python.org/3/reference/compound_stmts.html#if)
* [For en el manual de referencia de Python](https://docs.python.org/3/reference/compound_stmts.html#for)
