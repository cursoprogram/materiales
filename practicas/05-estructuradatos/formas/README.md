### Dibuja Formas Geométricas con diccionarios

El fichero `pixels.py` del repositorio plantilla es el mismo de la práctica "Tablero de ajedrez":

Hay que completar el fichero `formas.py` del repositorio plantilla, usando `pixels.py` como módulo para dibujar figuras geométricas. Solo se utilizará `pixels.py`, sin hacer ninguna llamada directa a `turtle` ni a otros módulos.

En este fichero `formas.py` tendrás que escribir el código que falta en las funciones `lee_formas()`, `dibuja_cuadrado()`, `dibuja_rectangulo()` y `main()`, de forma que acepte como argumentos grupos de parámetros para cada figura a dibujar. Los parámetros de cada figura serán:

* Para un cuadrado (`cuad`):
  * Coordenadas X e Y, donde habrá que comenzar a dibujar el cuadrado.
  * Un número entero que representa el tamaño del lado del cuadrado.
  * Un color opcional para el cuadrado (si no se especifica, será negro por defecto).

* Para un rectángulo (`rect`):
  * Coordenadas X e Y, donde habrá que comenzar a dibujar el rectángulo.
  * Dos números enteros que representan el ancho y el alto del rectángulo.
  * Un color opcional para el rectángulo (si no se especifica, será negro por defecto).

Por ejemplo:

```commandline
python3 formas.py cuad 2 3 5 blue
```

Dibujará un cuadrado de color azul en las coordenadas (2, 3) con un lado de longitud 5.

```commandline
python3 formas.py rect 10 8 6 3 green cuad 5 5 4
```

Dibujará dos figuras: un rectángulo verde en las coordenadas (10, 8) con ancho 6 y alto 3, y un cuadrado negro en las coordenadas (5, 5) con lado 4.

Además, cada una de las siguientes funciones ha de cumplir algunos detalles:

* `lee_formas()`: Lee los argumentos que el usuario haya escrito en la línea de comandos y devuelve una lista de diccionarios. Cada diccionario representa una figura a dibujar y contiene los parámetros necesarios para cada tipo (cuadrado o rectángulo).

* `dibuja_cuadrado()`: Dibuja un cuadrado en el lienzo. Recibe las coordenadas de comienzo, el tamaño del lado y el color. Devolverá `True` si el cuadrado se pudo pintar bien y `False` si se sale del lienzo.

* `dibuja_rectangulo()`: Dibuja un rectángulo en el lienzo. Recibe las coordenadas de comienzo, el ancho, el alto y el color. Devolverá `True` si el rectángulo se pudo pintar bien y `False` si se sale del lienzo.

**[Ejemplo de solución](formas.py)**