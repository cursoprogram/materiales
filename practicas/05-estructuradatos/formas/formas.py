import sys
from pixels import tam_lienzo, pinta, espera


def lee_formas():
    """Lee los argumentos de línea de comandos y construye una lista de diccionarios que representan las formas."""
    argumentos = sys.argv[1:]

    if len(argumentos) == 0:
        print("No hay formas a pintar.")
        exit()
    elif len(argumentos) < 4:
        print(
            "Formato incorrecto. Para cada figura se necesitan al menos cuatro parámetros: tipo x y lado|ancho alto [color]")
        exit()
    else:
        formas = []
        i = 0
        while i < len(argumentos):
            tipo = argumentos[i]

            if tipo == "cuad":
                if i + 3 >= len(argumentos):
                    print("Formato incorrecto para cuadrado. Uso: cuad x y lado [color]")
                    exit()
                x, y = int(argumentos[i + 1]), int(argumentos[i + 2])
                lado = int(argumentos[i + 3])
                color = argumentos[i + 4] if i + 4 < len(argumentos) else "black"
                formas.append({"tipo": "cuadrado", "x": x, "y": y, "lado": lado, "color": color})
                i += 5 if i + 4 < len(argumentos) else 4

            elif tipo == "rect":
                if i + 4 >= len(argumentos):
                    print("Formato incorrecto para rectángulo. Uso: rect x y ancho alto [color]")
                    exit()
                x, y = int(argumentos[i + 1]), int(argumentos[i + 2])
                ancho = int(argumentos[i + 3])
                alto = int(argumentos[i + 4])
                color = argumentos[i + 5] if i + 5 < len(argumentos) else "black"
                formas.append({"tipo": "rectángulo", "x": x, "y": y, "ancho": ancho, "alto": alto, "color": color})
                i += 6 if i + 5 < len(argumentos) else 5

            else:
                print("Tipo de figura no reconocido. Usa 'cuad' para cuadrado o 'rect' para rectángulo.")
                exit()

        return formas


def dibuja_cuadrado(x, y, lado, color="black"):
    """Dibuja un cuadrado en el lienzo."""
    (ancho, alto) = tam_lienzo()
    for i in range(lado):
        pinta(x + i, y, color)
        pinta(x + i, y + lado - 1, color)
        pinta(x, y + i, color)
        pinta(x + lado - 1, y + i, color)
    if x + lado > ancho or y + lado > alto or x < 0 or y < 0:
        return False
    else:
        return True


def dibuja_rectangulo(x, y, ancho, alto, color="black"):
    """Dibuja un rectángulo en el lienzo."""
    (lienzo_ancho, lienzo_alto) = tam_lienzo()
    for i in range(ancho):
        pinta(x + i, y, color)
        pinta(x + i, y + alto - 1, color)
    for j in range(alto):
        pinta(x, y + j, color)
        pinta(x + ancho - 1, y + j, color)
    if x + ancho > lienzo_ancho or y + alto > lienzo_alto or x < 0 or y < 0:
        return False
    else:
        return True


def main():
    """Programa principal"""
    formas = lee_formas()

    for forma in formas:
        tipo = forma["tipo"]
        x = forma["x"]
        y = forma["y"]
        color = forma.get("color", "black")

        if tipo == "cuadrado":
            lado = forma["lado"]
            dibuja_cuadrado(x, y, lado, color)
        elif tipo == "rectángulo":
            ancho_forma = forma["ancho"]
            alto_forma = forma["alto"]
            dibuja_rectangulo(x, y, ancho_forma, alto_forma, color)

    espera()


if __name__ == "__main__":
    main()
