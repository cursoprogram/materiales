import random

opciones = ["piedra", "papel", "tijera"]

# Historial es una lista con todas las partidas
historial = []


def ganaste_o_perdiste(jugador, computadora):
    """
    Dada la elección del jugador y la computadora, devuelve "Ganaste", "Perdiste" o "Empate"
    """
    pass # Borra esto y añade el código correspondiente


def jugar_piedra_papel_tijera():
    """
    Simula una partida de piedra, papel o tijera y devuelve el resultado en un diccionario
    (con los valores de jugador, computadora y resultado)

    Pregunta al usuario por su elección (comprobando si es válida)
    Elige aleatoriamente la opción del ordenador (y la imprime)
    Llama a la función ganaste_o_perdiste para calcular el resultado
    """
    pass # Borra esto y añade el código correspondiente
        

def mostrar_clasificacion(historial):
    """
    Dado el historial (lista de partidas), devuelve la clasificación
    """
    pass # Borra esto y añade el código correspondiente


def mostrar_historial(historial):
    """
    Dado el historial (lista de partidas), imprime el historial de partidas
    """
    pass # Borra esto y añade el código correspondiente
    

# Punto partida: Jugar partida de piedra, papel o tijera
# Añade partida la historial
# Muestra la clasificación
# Pregunta si el jugador quiere seguir jugando.
# Si sí, va a punto partida:
# Si no, imprime el historial, la clasificación y sale
while True:
    # Aquí viene código
    continuar = input("¿Quieres seguir jugando? (s/n): ")
    if continuar.lower() == 'n':
        # Aquí viene código
        break