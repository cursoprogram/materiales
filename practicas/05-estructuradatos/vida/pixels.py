#!/usr/bin/env python3

"""
Módulo para procesar imágenes en un lienzo, usando tkinter

Un pixel es la menor unidad que se puede leer o pintar de/en el lienzo.

El módulo proporciona las siguientes funciones:

* `prepara`
* 'tam_lienzo'
* 'pinta'
* 'lee'
* 'espera'

Para que funcione, es preciso realizar una llamada a `prepara()` antes
de llamar cualquier otra función.

Además, tiene una función que actúa como programa principal, `main()`,
que no se ejecuta cuando se importa este fichero como módulo,
y que sirve para probarlo.
"""

from tkinter import Tk, Canvas, PhotoImage, mainloop
from _tkinter import TclError

img = None
ventana = None
lienzo_ancho = None
lienzo_alto = None


def prepara(ancho: int, alto:int):
    """Prepara el lienzo"""

    global img, ventana, lienzo_ancho, lienzo_alto
    lienzo_ancho = ancho
    lienzo_alto = alto
    ventana = Tk()
    canvas = Canvas(ventana, width=ancho, height=alto, bg="#ffffff")
    canvas.pack()
    img = PhotoImage(width=ancho, height=alto)
    canvas.create_image((ancho // 2, alto // 2),
                        image=img, state="normal")

def prepara_foto(foto: str = None):
    """Prepara el lienzo con la foto indicada"""

    global img, ventana, ancho, alto
    ventana = Tk()
    try:
        img = PhotoImage(file=foto)
    except TclError:
        raise FileNotFoundError
    ancho = img.width()
    alto = img.height()
    canvas = Canvas(ventana, width=ancho, height=alto)
    canvas.pack()
    canvas.create_image((ancho // 2, alto // 2), image=img, state="normal")


def tam_lienzo():
    """Devuelve el ancho y el alto del lienzo"""
    return lienzo_ancho, lienzo_alto


def pinta(x: int, y: int, color: tuple[int] = (0, 0, 0)) -> None:
    """Pinta un pixel de color `color` en las coordenadas `x` e `y`"""

    img.put(f"#{color[0]:02x}{color[1]:02x}{color[2]:02x}", (x, y))


def lee(x: int, y: int) -> tuple[int]:
    """Lee el valor (color) de un pixel en las coordenadas `x` e `y`"""

    try:
        return img.get(x, y)
    except TclError:
        raise IndexError


def actualiza():
    """Actualiza el lienzo"""

    ventana.update()

def para(tiempo: int = 0):
    ventana.after(tiempo)

def espera():
    """Espera. Normalmente hay que llamarlo al terminar de pintar"""

    mainloop()


def main():
    """Programa principal (para probar el módulo)"""

    prepara('cafe_small.gif')
    pinta(1, 2, "red")
    pinta(6, 2, "blue")
    print(lee(1, 2))
    print(lee(1, 3))

    espera()


if __name__ == "__main__":
    main()
