### El juego de la vida

Usando el programa [pixels.py](pixels.py), para pintar pixels en un lienzo, escribir un programa que simule el juego de la vida.

[Una posible solución](vida.py)