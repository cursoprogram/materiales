#!/usr/bin/env python3

"""
Programa para simular el juego de la vida
https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life
"""

import pixels

"""
Constantes: tamaño de la casilla, ancho y alto del mundo,
y primeras células vivas en el mundo, cada tupla es un par
de coordenadas (x, y).
"""
tam_casilla = 20
ancho_mundo = 80
alto_mundo = 60
primera_poblacion = [(12, 13), (12, 14), (12, 15), (13, 12),
                     (13, 14), (13, 15), (14, 12), (14, 13),
                     (14, 15), (15, 12), (15, 13), (15, 14),
                     (17, 17), (17, 18), (18, 17), (18, 18),
                     (19, 19), (19, 20), (20, 19), (20, 20),
                     (20, 20), (20, 21), (21, 20), (21, 21),
                     (22, 22), (22, 23), (23, 22), (23, 23),
                     (28, 28), (28, 29), (29, 28),
                     (40, 40), (40, 41), (40, 42), ]


def crea_lienzo(ancho: int, alto: int) -> None:
    """Crea el lienzo para mostrar la simulación"""

    pixels.prepara(ancho * tam_casilla, alto * tam_casilla)


def pinta_casilla(x: int, y: int, color: tuple[int, int, int]) -> None:
    """Pinta na casilla del número de pixels indicado en la constante tam_casilla"""

    for lienzo_x in range(x * tam_casilla, (x + 1) * tam_casilla):
        for lienzo_y in range(y * tam_casilla, (y + 1) * tam_casilla):
            pixels.pinta(lienzo_x, lienzo_y, color)


def crea_mundo(ancho: int, alto: int, poblacion):
    """Crea la primera versión del mundo. El mundo es una lista de listas,
    donde cada elemento de la primera lista corresponde con una fila vertical,
    y cada elemento cada una de esas listas corresponde con una casilla horizontal.
    """

    mundo = []
    for x in range(ancho):
        columna = []
        for y in range(alto):
            if (x, y) in poblacion:
                columna.append(True)
            else:
                columna.append(False)
        mundo.append(columna)
    return mundo


def pinta_mundo(mundo: list[list[bool]]) -> None:
    """Muestra en el lienzo el mundo que se pasa como parámetro"""

    for x in range(len(mundo)):
        for y in range(len(mundo[0])):
            if mundo[x][y]:
                pinta_casilla(x, y, (0, 0, 0))
            else:
                pinta_casilla(x, y, (255, 255, 255))


def calcula_vecinos(x: int, y: int, mundo: list[list[bool]]) -> int:
    """Calcula el número de vecinos que tiene una célula
    (número de casillas vivas en su alrededor, esto es,
    número de posiciones a True en el parámetro mundo)
    """

    vecinos = 0
    for vecino_x in range(x - 1, x + 2):
        if vecino_x == x:
            rango_y = (y - 1, y + 1)
        else:
            rango_y = range(y - 1, y + 2)
        for vecino_y in rango_y:
            if vecino_x < 0 or vecino_x >= len(mundo) or vecino_y < 0 or vecino_y >= len(mundo[0]):
                continue
            if mundo[vecino_x][vecino_y]:
                vecinos += 1
    return vecinos


def nueva_ronda(mundo: list[list[bool]]) -> list[list[bool]]:
    """Calcula la nueva ronda del juego de la vida
    Produce una nueva versión del mundo."""
    nuevo_mundo = []
    for x in range(len(mundo)):
        nuevo_mundo.append([])
        for y in range(len(mundo[0])):
            vecinos = calcula_vecinos(x, y, mundo)
            print(mundo[x][y], vecinos, end=" ")
            if mundo[x][y]:
                if vecinos < 2 or vecinos > 3:
                    nuevo_mundo[x].append(False)
                else:
                    nuevo_mundo[x].append(True)
            else:
                if vecinos == 3:
                    nuevo_mundo[x].append(True)
                else:
                    nuevo_mundo[x].append(False)
        print()
    return nuevo_mundo


def main():
    """Programa principal"""

    print("Comenzando programa principal")
    ronda = 1
    crea_lienzo(ancho_mundo, alto_mundo)
    mundo = crea_mundo(ancho_mundo, alto_mundo, primera_poblacion)
    while True:
        print(f"Ronda {ronda}")
        pinta_mundo(mundo)
        pixels.actualiza()
        mundo = nueva_ronda(mundo)
        ronda += 1
        # pixels.para(200)
    pixels.espera()


if __name__ == "__main__":
    main()
