# Estructura de datos

Actividades:

* Presentación de transparencias del tema "Estructuras de datos II"
* Ejercicios de diccionarios simples

## Ejercicios

**Ejercicio:** Pinta Formas Geométricas
  * Ejercicio realizado en clase
  * **Fecha de entrega:** 7 de noviembre de 2024
  * [Repositorio plantilla](https://gitlab.eif.urjc.es/cursoprogram/plantillas2024/formas/)
  * [Enunciado](formas/README.md)

**Ejercicio:** Juego de la vida
  * Ejercicio realizado en clase
  * [Enunciado](vida/README.md)

  **Ejercicio:** Agenda
  * Ejercicio realizado en clase
  * [Enunciado](agenda/README.md)

  **Ejercicio:** Piedra, papel o tijera
  * Ejercicio realizado en clase
  * [Enunciado](ppt/README.md)

  **Ejercicio:** Administrador de una biblioteca
  * Cree un programa Python para administrar una biblioteca. Consideraremos una biblioteca como una colección de libros y revistas. Cada libro/revista debe tener la siguiente información: título, autor, ISBN y estado de disponibilidad. El programa debe permitir a los usuarios:
    * Agregar un libro/revista: solicitar al usuario los detalles del libro y agregarlo a la biblioteca.
    * Buscar un libro/revista: permitir al usuario buscar libros/revistas por título, autor o ISBN.
    * Pedir prestado un libro/revista: marcar un libro/revista como no disponible si está prestado.
    * Devolver un libro/revista: marcar un libro/revista como disponible.
    * Mostrar todos los libros/revistas: imprimir una lista de todos los libros/revistas de la biblioteca, incluido su estado de disponibilidad.