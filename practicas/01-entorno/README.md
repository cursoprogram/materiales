# El entorno de programación

Actividades:

* El entorno Linux del laboratorio, en detalle
* Introducción a la shell (intérprete de comandos)
* Repaso de conceptos Python
* PyCharm
* Asistentes de IA
* GitLab y GitHub

## El entorno Linux del laboratorio, en detalle

* Presentación: Linux, Ubuntu, GNOME.
* Presentación del escritorio Ubuntu:
  * Entorno de ventanas, lanzamiento de aplicaciones, entrada y salida.
  * Seguimiento de la pantalla compartida del profesor.
  * Entornos de trabajos (workspaces).
  * El gestor gráfico de ficheros
  * El sistema de ficheros:
    * Parte compartida (ejemplo: `/home`)
    * Parte de la máquina (ejemplos: `/tmp`, `/usr`)

Referencias:

* [Sitio web de los laboratorios docentes de la Escuela](https://labs.etsit.urjc.es/)
* [Ubuntu Desktop Guide: Your desktop](https://help.ubuntu.com/stable/ubuntu-help/shell-overview.html.en)
* [Ubuntu Desktop Guide](https://help.ubuntu.com/stable/ubuntu-help/)

## Introducción a la shell (intérprete de comandos)

* Lanzamiento de una consola con intérprete de comandos
* Presentación: el intérprete de comandos de Linux (shell):
  * el prompt
  * `pwd`
  * ejecución de comandos (`fortune`, `date` como ejemplo)
  * flechas (adelante, atrás)
  * `ls`, `ls -l`, `ls -a`, `ls -al`
  * tabulador (completar)
  * `cd` (directorios, `..`, `.`, `~`)
  * `mkdir`, `rmdir`
  * `rm`
  * `du -s ~`
  * `man ls`
* Pestañas de la consola
* Interrumpir un comando: `<CTRL> c`. Ejemplo:
```
ls -R /
<CTRL> c
```
* Copiar y pegar en la consola: `<CTRL> <SHIFT> c`, `<CTRL> <SHIFT>v`
* Lanzamiento del intérprete de Python, y salida
* Python como calculadora

## Asistentes de IA

* [Codeium](https://codeium.com/)
  * [Plugin para PyCharm](https://plugins.jetbrains.com/plugin/20540-codeium-ai-autocomplete-and-chat-for-python-js-java-go--)
* [Cody](https://sourcegraph.com/cody)
  * [Plugin para PyCharm](https://plugins.jetbrains.com/plugin/9682-cody-ai-coding-assistant-with-autocomplete--chat)
* [Continue](https://docs.continue.dev/)
  * [Plugin para PyCharm](https://plugins.jetbrains.com/plugin/22707-continue)
* [GitHub Copilot](https://github.com/features/copilot)
  * [Plugin para PyCharm](https://plugins.jetbrains.com/plugin/17718-github-copilot)
* [CodeBuddy](https://codebuddy.ca/)
  * [Plugin para PyCharm](https://plugins.jetbrains.com/plugin/22666-codebuddy)
* [TabNine](https://www.tabnine.com)
  * [Plugin para PyCharm](https://plugins.jetbrains.com/plugin/12798-tabnine)

## GitLab

* [GitLab.com](https://gitlab.com) y el [GitLab de la EIF](https://gitlab.eif.urjc.es)

* Autenticación en el GitLab de la EIF: puedes autenticarte (usuario y contraseña) con tu cuenta de los laboratorios de la EIF.
* Proyecto (repositorio) GitLab.
  * [Repo de la asignatura en GitLab.com](https://gitlab.com/cursoprogram/cursoprogram.gitlab.io)
  * [Repo de recursos de la asignatura en ETSIT GitLab](https://gitlab.eif.urjc.es/cursoprogram/materiales/)
* Proyectos de un grupo o usuario.
  * [Grupo de proyectos de la asignatura en ETSIT GitLab](https://gitlab.eif.urjc.es/cursoprogram)
  * [Usuario jesus.gonzalez.barahona en ETSIT GitLAb](https://gitlab.eif.urjc.es/jesus.gonzalez.barahona)
  * [Usuario jgbarah en GitLab.com](https://gitlab.com/jgbarah)
* Edición de ficheros
  * El botón de editar un fichero
  * El IDE de GitLab

## Git

* Clonado de repositories

```commandline
git clone <url>
```

* Consulta de remotos

```commandline
git remote -v
```

* Consulta del histórico

```commandline
git log
```

* Consulta de estado

```commandline
git status
```

* Añadir ficheros nuevos

```commandline
git add <fichero>
```

* Realizar un commit (atención al punto al final de la línea)

```commandline
git commit -m "Mensaje del commit" .
```

* Subir un commit a un remoto

```commandline
git push
```

* Recibir commits de un remoto

```commandline
git fetch
git rebase origin/master
```

o

```commandline
git fetch
git merge
```

Nosotros recomendamos (para las prácticas de esta asignatura) usar la primera opción, pero si quieres conocer más detalles sobre las diferencias, lee [Git Rebase vs. Git Merge: Which Is Better?](https://www.perforce.com/blog/vcs/git-rebase-vs-git-merge-which-better)

* Flujo normal de prácticas: Normalmente tendremos dos repositorios git para una práctica:
  * Uno remoto (en EIF GitLab), que normalmente habrá empezado como una bifurcación (fork) del repositorio plantilla (uno para cada práctica, mencionado en el enunciado).
  * Otro será el repositorio "local", en el ordenador de trabajo, que se obtendrá clonando el remoto (`git clone`).
  * Cada vez que se hagan cambios en el repositorio local que compongan una nueva versión, se hará `git commit` (quizás previamente haga falta `git add`).
  * Cuando se quiera (se aconseja cada vez que se haga un commit) se enviarán los commits al repositorio remoto, con `git push`.
  * En el remoto, se puede comprobar que los commits se han enviado bien viendo la historia vía la interfaz de GitLab. 
  * Si se hubiera hecho algún cambio en el repositorio remoto (por ejemplo, usando el IDE de GitLab), habrá que recibir los commits correspondientes en el repositorio local, por ejemplo con `git fetch` y `git rebase`.
  * En el repositorio local, se puede comprobar qu elos commits se han recibido bien  viendo la historia vía `git log`.
  * Se puede ver qué ficheros están "bajo control de git" y por tanto en los commits viendo el estado del repositorio (`git status`).

* Git desde PyCharm. Todas las opciones relevantes las tenemos en el menú "Git".

* Flujo de prácticas con tres repos: En algunos casos, se querrá tener además del repositorio remoto en EIF GitLab y el repositorio local en el laboratorio, otro repositorio local en otro ordenador (el ordenador de casa, por ejemplo).
  * En este caso es importante asegurarse de que todos los cambios se suben como commits al repo de GitLab antes de empezar a trabajar en el otro repo "local".
  * El flujo de trabajo en un repo local comenzará por tanto siempre recibiendo los cambios (commits) que pueda haber en el repo en GitLab, haciendo localmente los cambios que se quiera, y al terminar enviándolos al repo remoto en GitLab como uno o varios commits. De esta forma, el repositorio en GitLab siempre servirá para sincronizar cualquiera de los repositorios locales.

  
## Ejercicios

**Ejercicio:** Ficheros y directorios con el intérprete de comandos.
  * Ejercicio realizado en clase
  * [Enunciado](directorios/README.md)

**Ejercicio a entregar:** Comandos de shell
  * Ejercicio de entrega en el foro
  * **Fecha de entrega:** 1 de octubre de 2024
  * [Enunciado](comandos/README.md).

**Ejercicio a entregar:** Permutaciones de una lista
  * Ejercicio de entrega en el foro
  * **Fecha de entrega:** 3 de octubre de 2024
  * [Enunciado](permutaciones/README.md).

**Ejercicio:** Creación de un repositorio
  * Ejercicio realizado en clase
  * [Enunciado](gitlab/README.md)

**Ejercicio:** Bifurcación de un repositorio.
  * Ejercicio realizado en clase
  * [Enunciado](gitlab/README.md)

**Ejercicio:** Bifurcación de un repositorio plantilla.
  * Ejercicio de entrega en GitLab
  * **Fecha de entrega:** 8 de octubre de 2024
  * [Repositorio plantilla](https://gitlab.eif.urjc.es/cursoprogram/plantillas2024/bifurca/)
  * [Enunciado](gitlab/README.md)

Referencias:

* [PyCharm: Get Started](https://www.jetbrains.com/help/pycharm/quick-start-guide.html#code-assistance)
* [PyCharm: First Steps](https://www.jetbrains.com/help/pycharm/creating-and-running-your-first-python-project.html)
* [PyCharm: Debugging Python Code](https://www.jetbrains.com/help/pycharm/part-1-debugging-python-code.htm)
* [The Linux command line for beginners](https://ubuntu.com/tutorials/command-line-for-beginners)
* [Linux Command Line Full course](https://www.youtube.com/watch?v=2PGnYjbYuUo) (video)
* [Pro Git](https://git-scm.com/book/en/v2). Libro sobre git, que cubre desde el uso básico hasta varios detalles avanzados, incluyendo el uso con GitHub (y GitLab).
* [Oh My Git!](https://blinry.itch.io/oh-my-git). Juego de cartas para aprender detalles de git, incluyendo cómo entender el grafo de versiones.



