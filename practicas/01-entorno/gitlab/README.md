### Creación de un repositorio

* Crea un repositorio en GitLab de tu usuario.
* El repositorio creado debe quedar como un repositorio de tu usuario.
* Una vez que tengas ese nuevo repositorio, utiliza la interfaz web de GitLab para conseguir que tenga un fichero `README.md` en el que ponga "Este es el repositorio de <nombre>", donde <nombre> es tu nombre.
* Asegúrate de que el repositorio es público, o interno.

### Bifurcación de un repositorio

* Bifurca (haz un fork) del repositorio [Bifurca Repositorio](https://gitlab.eif.urjc.es/cursoprogram/bifurca-repositorio/).
* El repositorio resultante debe quedar como un repositorio de tu usuario.
* Una vez que tengas ese nuevo repositorio, utilizando la interfaz web de GitLab, modifica el fichero README.md que hay en él para añadir, después del enunciado, el texto "Este es el repositorio de <nombre>", donde <nombre> es tu nombre.
* Asegúrate de que el repositorio es público (visible para todos los visitantes) o interno (visible para los usuarios que se hayan autenticado).

### Bifurcación de un repositorio plantilla

* Bifurca (haz un fork) del repositorio plantilla [Bifurca](https://gitlab.eif.urjc.es/cursoprogram/plantillas2024/bifurca/).
* El repositorio resultante debe quedar como un repositorio de tu usuario.
* Una vez que tengas ese nuevo repositorio, utilizando la interfaz web de GitLab, modifica el fichero README.md que hay en él para añadir, después del enunciado, el texto siguiente: "Este es el repositorion construido por <nombre>, haciendo un fork del repositorio plantilla de la primera práctica de entrega en GitLab", donde <nombre> es tu nombre.
* Asegúrate de que el repositorio es público (visible para todos los visitantes) o interno (visible para los usuarios que se hayan autenticado).
