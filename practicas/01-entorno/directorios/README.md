### Ficheros y directorios con el intérprete de comandos

* Cámbiate a tu directorio hogar (principal), crea un directorio con `mkdir`, cámbiate a él.
* Crea un fichero con el editor básico de Ubuntu, en ese directorio que acabas de crear, con el contenido "Hola".
* Ejecuta los comandos `pwd`, `ls`, `ls -l` y `ls -al`.
* Contesta al ejercicio en el foro de ejercicios con el resultado de ejecutar los comandos anteriores. 

Si se te pide contestar en el foro de ejercicios, copia y pega desde el intérprete de comandos el resultado de ejecutar esos comandos. Recuerda que en el intérprete de comandos puedes copiar un contenido seleccionándolo y pulsando "Mayúsclas"-"Control"-C (y no "Control"-C como se hace en otras aplicaciones). Luego podrás pegar lo copiado en la respuesta, en este foro, con "Control"-V. También puedes usar tanto en el intérprete de comandos como aquí en el foro la opción de, una vez seleccionado, pulsar el botón derecho y usar la opción de menú "Copiar" y luego "Pegar".
