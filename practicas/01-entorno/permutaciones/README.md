### Permutaciones de una lista

Dada una lista de strings (tiras de caracteres) como `['Hola', 'Qué tal', 'Adiós']' haz un programa que escriba todas sus permutaciones, cada una en una lista, con los strings separados por espacios. Por ejemplo:

```commandline
% python3 permutaciones.py
Lista: ['Hola', 'Qué tal', 'Adiós']
Permutaciones:
Hola Qué tal Adiós
Hola Adiós Qué tal
Qué tal Hola Adiós
Qué tal Adiós Hola
Adiós Hola Qué tal
Adiós Qué tal Hola
```

El programa empezará con estas líneas:

```python
lista = ['Hola', 'Qué tal', 'Adiós']
print('Lista:', lista)
print('Permutaciones:')
```

Opcional: ¿puedes hacer lo mismo, para listas con cuatro strings? ¿Y con un número arbitrario de strings?

Solución: [permutaciones.py](permutaciones.py)
