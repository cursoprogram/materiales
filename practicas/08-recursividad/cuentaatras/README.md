# Programa con un cuenta atrás

## Objetivo

El objetivo de este ejercicio es comprender la recursividad y utilizarla en un sencillo ejemplo, un programa de cuenta atrás.

1. Comprender el concepto de recursividad: Introducir la idea de que una función se llama a sí misma y cómo esto puede resolver problemas de manera elegante.
2. Identificar el caso base y el caso recursivo: Distinguir la condición que detiene la recursión y la parte de la función que se llama a sí misma.
3. Implementar funciones recursivas simples: Resolver problemas como el cálculo del factorial, la secuencia de Fibonacci o la búsqueda binaria utilizando recursividad.
4. Visualizar el proceso recursivo: Utilizar herramientas de depuración o diagramas para entender cómo se ejecuta una función recursiva paso a paso.

---

#### Descripción del Programa

El programa se compone de la siguientes función:

* `countdown(n)`: Recibe un entero n que será el número de partida, que se decrementará una unidad hasta que alcance cero.

#### Ejemplo de Uso

```commandline
python3 cuentaatras.py
```