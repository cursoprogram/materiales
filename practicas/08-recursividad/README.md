# Recursividad:

Actividades:

* Ejemplos de funciones con recursividad

## Ejercicios

**Ejercicio:** Cuenta atrás
  * Ejercicio hecho en clase
  * [Enunciado](cuentaatras/README.md)

**Ejercicio:** Factorial
  * Ejercicio hecho en clase
  * [Enunciado](factorial/README.md)

**Ejercicio:** Buscaminas
  * Ejercicio hecho en clase
  * [Enunciado](buscaminas/README.md)