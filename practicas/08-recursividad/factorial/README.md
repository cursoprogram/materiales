# Programa para calcular el factorial

## Objetivo

El objetivo de este ejercicio es profundizar en el uso de la recursividad, creando un programa que calcule el factorial de un número.

1. Profundizar en el uso del concepto de recursividad.
2. Identificar el caso base y el caso recursivo: Distinguir la condición que detiene la recursión y la parte de la función que se llama a sí misma.
3. Implementar una función recursiva simple

---

#### Descripción del Programa

El programa se compone de la siguientes función:

* `factorial(n)`: Recibe un entero n que será el número para el que hay que calcular n!.

#### Ejemplo de Uso

```commandline
python3 factorial.py numero
```