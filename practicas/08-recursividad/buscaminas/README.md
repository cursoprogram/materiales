### Buscaminas

El fichero `buscaminas.py` de esta carpeta será el punto de partida para esta práctica. En esta actividad, completarás las funciones que implementan recursividad, en particular, cuando una celda no contiene una mina ni hay ninguna mina alrededor, en cuyo caso, se revelan las todas las celdas adyacentes.

#### Descripción:

El objetivo es implementar un programa que permita jugar al juego de buscaminas.

#### Funciones a completar:

1. **`revelar_celdas(tablero, fila, columna)`**:
   - Indica la condición que se debe de cumplir para que haya recursividad (busca por "CONDICION" en el script)
   - Llama a la función de manera recursiva (busca por "FUNCION_RECURSIVA" en el script)

#### Ejemplo de ejecución:

```commandline
python3 buscaminas.py
```

#### Detalles:

* Usa las funciones provistas y respeta los comentarios que explican su funcionamiento.

#### Código:

[buscaminas.py](buscaminas.py)
