import random


def crear_tablero(filas, columnas, minas):
    """Crea un tablero de buscaminas aleatorio.

    Args:
        filas: Número de filas del tablero.
        columnas: Número de columnas del tablero.
        minas: Número de minas en el tablero.

    Returns:
        Una lista de listas representando el tablero.
    """
    tablero = []
    for i in range(filas):
        fila = []
        for j in range(columnas):
            fila.append('*')
        tablero.append(fila)
    posiciones_minas = random.sample(range(filas * columnas), minas)

    for pos in posiciones_minas:
        fila = pos // columnas
        columna = pos % columnas
        tablero[fila][columna] = 'X'

    return tablero

def contar_minas_alrededor(tablero, fila, columna):
    """Cuenta el número de minas alrededor de una celda.

    Args:
        tablero: El tablero de buscaminas.
        fila: Fila de la celda.
        columna: Columna de la celda.

    Returns:
        El número de minas alrededor de la celda.
    """
    filas, columnas = len(tablero), len(tablero[0])
    contador = 0
    for i in range(max(0, fila - 1), min(filas, fila + 2)):
        for j in range(max(0, columna - 1), min(columnas, columna + 2)):
            if i == fila and j == columna:
                continue
            if tablero[i][j] == 'X':
                contador += 1
    return contador

def revelar_celdas(tablero, fila, columna):
    """Revela las celdas vacías y las que contienen números alrededor de una celda.

    Args:
        tablero: El tablero de buscaminas.
        fila: Fila de la celda.
        columna: Columna de la celda.
    """

    filas, columnas = len(tablero), len(tablero[0])
    if fila < 0 or fila >= filas or columna < 0 or columna >= columnas:
        return
    if tablero[fila][columna] != '*':
        return
    
    minas_alrededor = contar_minas_alrededor(tablero, fila, columna)
    tablero[fila][columna] = str(minas_alrededor) if minas_alrededor else ' '

    if False: # CONDICION
        for i in range(max(0, fila - 1), min(filas, fila + 2)):
            for j in range(max(0, columna - 1), min(columnas, columna + 2)):
                pass # FUNCION_RECURSIVA

def imprimir_tablero(tablero):
    """Imprime el tablero
    
    Args:
        tablero: El tablero de buscaminas
    """
    for fila in tablero:
        for celda in fila:
            if celda == 'X':
                print('*', end=' ')
            else:
                print(celda, end=' ')
        print()


if __name__ == "__main__":
    filas = 10
    columnas = 10
    minas = 15
    tablero = crear_tablero(filas, columnas, minas)

    while True:
        imprimir_tablero(tablero)
        fila = int(input("Ingrese la fila (0-"+str(filas-1)+"): "))
        columna = int(input("Ingrese la columna (0-"+str(columnas-1)+"): "))

        if tablero[fila][columna] == 'X':
            print("¡Perdiste! Has encontrado una mina.")
            break  # Termina el juego

        revelar_celdas(tablero, fila, columna)