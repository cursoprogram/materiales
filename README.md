# Materiales (Informática I, ISAM, URJC)

Repositorio con el fuente de los principales materiales que estamos usando en la asignatura Informática I, del Grado en Ingeniería de Sistemas Audiovisuales y Multimedia, de la Universidad Rey Juan Carlos.

Estos materiales se van actualizando y mejorando según va avanzando la asignatura.

* [Programa](practicas/programa.md)
* [Transparencias](transpas.pdf)
* [Referencias generales](referencias.md)

