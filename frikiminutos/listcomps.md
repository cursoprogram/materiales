## List comprehensions


Las list comprehensions son una forma concisa y eficiente de crear listas en Python a partir de otras iterables (como listas, tuplas o cadenas). Su sintaxis es más compacta y legible que los bucles tradicionales.

Estructura básica:

```
[expresión for elemento in iterable]
```

Ejemplo:

```
numeros = [1, 2, 3, 4, 5]
cuadrados = [x**2 for x in numeros]
print(cuadrados)  # Output: [1, 4, 9, 16, 25]
```

En este ejemplo:

* Se crea una lista numeros.
* Se utiliza una list comprehension para crear una nueva lista cuadrados.
* Para cada elemento x en numeros, se eleva al cuadrado y se agrega a la lista cuadrados.

Las list comprehensions pueden incluir condiciones:

```
numeros_pares = [x for x in numeros if x % 2 == 0]
print(numeros_pares)  # Output: [2, 4]
```

Aquí se filtra la lista original para incluir solo los números pares.

