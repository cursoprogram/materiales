## El juego de la vida

Implementación del juego de la vida de Conway: [repositorio en GitHub](https://github.com/mwharrisjr/Game-of-Life)

Para ejecutarlo:

```commandline
git clone https://github.com/mwharrisjr/Game-of-Life
cd Game-of-Life
python3 script/main.py
```
Referencias:

* [Juego de la vida en Wikipedia](https://es.wikipedia.org/wiki/Juego_de_la_vida)
* [ConwayLife.com](https://conwaylife.com/)