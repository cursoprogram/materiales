## Generador de QRs

Genera un código QR para una url: [qr.py](qr.py) 

* Dependencias: [pyqrcode](https://github.com/mnooner256/pyqrcode), [pypng](https://gitlab.com/drj11/pypng)

Para instalar las dependencias, crea un entorno virtual, actívalo, e instala en él las dependencias.

```commandline
python3 -m venv venv-qr
source venv-qr/bin/activate
pip install pyqrcode pypng
```

Para ejecutar, recuerda activar primero el entorno virtual si no lo has hecho antes

```commandline
source venv-qr/bin/activate
python3 qr.py
```
