## Logo

El lenguaje de programación Logo fue diseñado en la década de 1960 por Wally Feurzeig, Seymour Papert, Cynthia Solomon y su equipo, en el Instituto Tecnológico de Massachusetts (MIT). Se utilizó sobre todo para enseñar conceptos de programación a niños y principiantes. Es un lenguaje conocido por su enfoque en la programación visual y la creación de gráficos. Su característica más distintiva es la capacidad de controlar una "tortuga" (un cursor en forma de triángulo) que se mueve por la pantalla, dejando un rastro detrás de sí, lo que permite crear gráficos y patrones geométricos.

Logo se ha utilizado para enseñar conceptos básicos de programación, como la secuencia de instrucciones, la iteración y la recursividad. También para enseñar conceptos matemáticos, como geometría o trigonometría. Aunque Logo no ha sido muy utilizado en la industria, su influencia en la educación informática es significativa. Muchos lenguajes de programación educativos modernos, como Scratch y Blockly, han sido influenciados en parte por su enfoque visual y accesible.

* [Wikipedia: El lenguaje de programación Logo](https://es.wikipedia.org/wiki/Logo_(lenguaje_de_programaci%C3%B3n))
* [Revista Byte: Número especial dedicado a Logo (1982)](https://ia600605.us.archive.org/14/items/byte-magazine-1982-08/1982_08_BYTE_07-08_Logo.pdf)
* [LOGO - Obsolete Programming Language #1](https://www.youtube.com/watch?v=3gSwllpDFgM) (video)