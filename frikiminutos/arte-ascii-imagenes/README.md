## Arte ASCII: imágenes y videos

Convierte imágenes y videos al estilo [ASCII Art](https://en.wikipedia.org/wiki/ASCII_art) [artdemo2.py](artdemo2.py).

* Dependencias: [asciipixels](https://github.com/UmActually/asciipixels/). Necesita también ImageMagick (para imágeners) y FFmpeg (para videos), que vienen como paquetes en Debian y Ubuntu.

```commandline
pip install asciipixels
```

```commandline
sudo apt install ffmpeg imagemagick
```
