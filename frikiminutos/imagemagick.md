## ImageMagick

ImageMagick es una biblioteca de software de imagen que permite realizar operaciones básicas de procesamiento de imágenes. Entre ellas, permite transformaciones entre formatos, y muchos tratamientos de imagen diferentes. En general, todos ellos se pueden realizar desde línea de comandos.

Más información:

* [ImageMagick](https://imagemagick.org/)
* [Ejemplos de uso de ImageMagick](https://usage.imagemagick.org/)