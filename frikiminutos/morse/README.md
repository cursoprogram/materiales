## Morse en Python

El código morse, también conocido como alfabeto morse o clave morse es un sistema de representación de letras y números mediante señales emitidas de forma intermitente.

Tienes un [traductor de/a Morse en Python](https://github.com/avinashkranjan/Amazing-Python-Scripts/blob/main/Morse_Code_Translator/Morse_Code_Translator.py)

Para aprenderlo de memoria se han buscado palabras que empiezan por cada una de las letras y que siguen esta regla Mnemotécnica:

Por ejemplo la B es _…   y cada signo será una vocal de la palabra (así que necesitamos una palabra con 4 vocales). Si hay una raya esa vocal tiene que ser una O, el resto de vocales equivalen todas a un punto. POr ejemplo se ha escogido “Bofetada” (oeaa = _…).

Recordad que estas palabras son sólo para memorizar los signos de cada letra.

Cada letra se transmite pos separado, esperando varios segundos entre letra y letra. Entre palabra y palabra hay que esperar más tiempo.

Podéis utilizar estas palabras, por ejemplo.

Recordad: la O equivale a una rayaa memorizarlo.

![Morse nmemotécnico](https://scoutslamerced.org/wp-content/uploads/2017/06/Palabras-clave-recordar-codigo-morse.jpg)