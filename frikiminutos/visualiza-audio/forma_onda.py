from scipy.io import wavfile
import matplotlib.pyplot as plt
import numpy as np

# Cargar el archivo de audio
sample_rate, data = wavfile.read("audio.wav")

# Crear el eje de tiempo
tiempo = np.linspace(0, len(data) / sample_rate, num=len(data))

# Graficar
plt.figure(figsize=(10, 4))
plt.plot(tiempo, data, color='green')
plt.title("Forma de onda del archivo WAV")
plt.xlabel("Tiempo (s)")
plt.ylabel("Amplitud")
plt.show()
