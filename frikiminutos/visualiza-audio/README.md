## Visualiza formas de onda de audio - Wave viewer

Existen muchos paquetes de procesamiento de audio en Python, hoy vemos como con la combinación de varios de estos, podemos sacar la forma de onda de cualquier audio: [forma_onda.py](forma_onda.py).

* Dependencias: [scipy](https://scipy.org/), [matplotlib](https://matplotlib.org/) y [numpy](https://numpy.org/)

```commandline
pip install scipy matplotlib numpy
```

Además de `libxcb-cursor0` (instalado en los laboratorios):

```commandline
sudo apt-get install libxcb-cursor0
```

* Listado de fonts disponibles:

```commandline
python -m forma_onda.py
```
