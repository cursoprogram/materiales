## Cambiar tamaño de imágenes

Programita para cambiar el tamaño de varias imágenes en un directorio a otro tamaño (y guardarlas en otro directorio): [resize.py](resize.py).

* Dependencias: [Pillow](https://gitlab.eif.urjc.es/cursoprogram/materiales/-/blob/main/frikiminutos/pillow.md)
* Imágenes de ejempo: [Imágenes](imagenes.zip)