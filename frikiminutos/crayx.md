## CrayX-MP

Fue el ordenador más potente entre 1983 y 1985:
* Rendimiento: 800 MFLOPS
* Peso: unas 5 toneladas
* Consumo: 345 KW
* Precio: unos 15 millones de USD
* Memoria: 128 Mbytes
* Almacenamiento: 38.4 Gbytes (32 discos)
* CPUs: 4, 105 MHz
* [Más detalles en Wikipedia](https://en.wikipedia.org/wiki/Cray_X-MP)

En la [novela "Parque Jurásico"](https://es.wikipedia.org/wiki/Parque_Jur%C3%A1sico_(novela)) tres ordenadores CrayX-MP son los encargados de hacer funcionar todo el software que controla el parque.
