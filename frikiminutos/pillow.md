## Pillow

Pillow es una biblioteca Python para trabajar con imágenes. Soporta muchos formatos, es eficiente, y permite realizar muchos tipos de procesamento de imágenes.

Más información:

* [Pillow](https://python-pillow.org/)
* [Documentación de Pillow](https://pillow.readthedocs.io/en/stable/)
* [Tutorial de Pillow](https://pillow.readthedocs.io/en/stable/handbook/tutorial.html)