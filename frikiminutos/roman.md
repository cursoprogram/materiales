## Transformador a números romanos

Programa en Python que transforma números arábigos a números romanos: [integer_to_roman_numeral.py](https://github.com/prathimacode-hub/Awesome_Python_Scripts/blob/main/BasicPythonScripts/Integer%20To%20Roman%20Numeral/integer%20_to_roman_numeral.py)