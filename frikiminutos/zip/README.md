## Ficheros ZIP

En Python tenemos un módulo que sirve para gestionar ficheros en formato zip. Es el [módulo zipfile](https://docs.python.org/3/library/zipfile.html).

Por ejemplo, poara listar los ficheros en un fichero comprimido con zip, y extraerlos: [unzip.py](unzip.py)
