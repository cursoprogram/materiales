## Reconocimiento de caracteres (OCR)

Reconoce caracteres en una imagen (en inglés y otros idiomas): [ocr.py](oct/ocr.py)

* Dependencias: [EasyOCR](https://github.com/JaidedAI/EasyOCR)

```commandline
pip install easyocr
```
