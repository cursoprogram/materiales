#! /usr/bin/env python3

import random

numero = int(input("Dime el número que juegas (entre 0 y 99999): "))
if numero < 0 or numero > 99999:
    print("El número debe estar entre 0 y 99999")
    exit(1)
toco = False
sorteos = 0

while not toco:
    sorteos += 1
    gordo = random.randint(0, 99999)
    if gordo == int(numero):
        toco = True

print(f"Han hecho falta {sorteos} sorteos para que te toque el gordo")