## Juegos de azar

Hay un módulo en Python que permite generar números pseudoaleatorios. Es el módulo 
[random](https://docs.python.org/3/library/random.html). Usándolo se pueden programar muchos juegos de azar. Por ejemplo:

* [Tirar un dado](dado.py)
* [Tirar dos dados](dos_dados.py)
* [¿Cuántas veces he de jugar a la lotería para que me toque el gordo?](loteria.py)