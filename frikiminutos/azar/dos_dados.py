#! /usr/bin/env python3

import random

dado1 = random.randint(1, 6)
dado2 = random.randint(1, 6)

print(f"Dado 1: {dado1}, dado 2: {dado2}, suma: {dado1 + dado2}")
