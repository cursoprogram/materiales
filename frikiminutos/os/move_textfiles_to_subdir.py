import os

def move_txt_files(source_dir, target_dir):
    """Moves all .txt files from source directory to target directory.

    Args:
        source_dir: The source directory.
        target_dir: The target directory.

    Creates the target directory if it doesn't exist.
    """

    if not os.path.exists(target_dir):
        os.makedirs(target_dir)

    for root, dirs, files in os.walk(source_dir):
        for file in files:
            if file.endswith(".txt"):
                source_file = os.path.join(root, file)
                destination_file = os.path.join(target_dir, file)
                os.rename(source_file, destination_file)

# Example usage:
source_directory = "your_source_directory"
target_directory = "txt_files"

move_txt_files(source_directory, target_directory)