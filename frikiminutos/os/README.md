## Scripts de administración

ay un módulo en Python que permite crear scripts de administración con los cuales crear directoris, copiar ficheros, etc. Es el módulo [os](https://docs.python.org/3/library/os.html). Usándolo se pueden hacer muchas cosas. Por ejemplo:

* [Renombrar múltiples ficheros](rename.py)
* [Mover todos los ficheros de texto a un subdirectorio](move_textfiles_to_subdir.py)