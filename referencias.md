## Referencias útiles para la asignatura

### Bibliografía básica

* "Head First Python", 2nd Edition, Paul Barry, November 2016, O'Reilly Media, Inc. ISBN: 978-1-491-91953-8. Disponible en formato electrónico vía la Biblioteca de la Universidad.

* "Dive into Python 3", Mark Pilgrim, [disponible libremente](https://diveintopython3.net/)

* "El libro de Python", [disponible libremente](https://ellibrodepython.com/)

### Bibliografía de consulta

* "Algorithms + Data Structures = Programs". Niklaus Wirth. Editorial Prentice Hall, 1992

* "The Practice of Programming. Brian W Kernighan", Rob Pike. Addison-Wesley. 1999.

* "Introduction to Algorithms", 3rd Edition, Thomas H. Cormen, Charles E. Leiserson, Ronald L. Rivest, Clifford Stein, 2009, ISBN 978-0-262-03384-8, MIT Press o 4th Edition, Thomas H. Cormen, Charles E. Leiserson, Ronald L. Rivest, Clifford Stein, 2022, ISBN 0-262-04630-X.

* "Introduction to Computer Science and Programming in Python", Fall 2016 Edition, Ana Bell, Eric Grimson, John Guttag, MIT OpenCourseWare, [disponible libremenet](https://ocw.mit.edu/courses/6-0001-introduction-to-computer-science-and-programming-in-python-fall-2016/)

* "Documentación en línea de Python" (incluyendo un Tutorial, los manuales de referencia, HOWTOS, etc), versión para Python 3.x, [disponible libremente](http://www.python.org/doc)
