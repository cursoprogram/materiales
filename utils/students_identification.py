#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Program to retrieve practices
Uses a CSV file with the list of students retrieved from Aulavirtual filtering by Student role:
"Nombre","Apellido(s)","Dirección de correo"

Example of how to run the script:
retrieve_repos.py --practice :all: --students ist-saro-2022.csv --cloning_dir ../../retrieved-2022

It will return always the folloing files:
- "not_founds.txt": file with those students with problems finding their fork
- "<students>_enriched.csv": csv enriched with the lab username and gitlab username (if found) of the students
"""

import argparse
import csv
import json
import os
import subprocess
import urllib.request
import re

from shutil import copyfile
from distutils.dir_util import copy_tree
from unicodedata import normalize

from git.repo.base import Repo
from git.exc import GitCommandError


def get_lab_username(student):
    # Command finger in etsit labs, removing tildes, extra spaces and "ñ"
    stdoutfinger = os.popen("finger {} | grep 'Name: '".format(student['nombre_apellidos_formatted'])).read().split("\n")
    for entry in stdoutfinger:
        alumno_name = entry.split("\t\t\tName: ")[-1].lower().replace(" ", "")
        if alumno_name in student['nombre_apellidos_formatted'].replace(" ", ""):
            # Return the lab username
            return entry.split("\n")[0].split("\t\t\tName: ")[0].split("Login: ")[-1].strip()

    return ""

def remove_tildes(name):
    # -> NFD y eliminar diacríticos
    name = re.sub(
        r"([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+", r"\1",
        normalize("NFD", name), 0, re.I
    )

    # -> NFC
    name = normalize('NFC', name)

    # ñ -> n
    name = name.replace("ñ", "n")

    return name

def export_csv_enriched(studentsfile, students):
    with open(studentsfile, 'w', newline='') as csvenriched:
        csvwriter = csv.writer(csvenriched)
        csvwriter.writerow(["Nombre", "Apellido(s)", "Dirección de correo", "Usuario Lab", "Usuario Gitlab"])
        for student in students.values():
            # Enriquecer con los datos obtenidos
            csvwriter.writerow([student["nombre"],
                                student["apellidos"],
                                student["usuario_correo_completo"],
                                student.get("usuario_lab", ""),
                                student.get("usuario_gitlab", "")])

def process(filename_in, filename_out):
    """Read the CSV file with names, email addresses, etc. and try to fill in lab ids

    Produces an output file, in CVS format, hopefully more complete.
    Input and output files may be the same."""
    students = {}
    file_modified = False
    with open(filename_in, 'r', newline='', encoding="utf-8") as cvsfile:
        rows = csv.DictReader(cvsfile)
        for row in rows:
            # We have info about the name, surname and email, we get the email as uid
            usuariocorreo = row['Dirección de correo'].split("@")[0]

            students[usuariocorreo] = {
                'usuario_correo_completo': row['Dirección de correo'],
                'usuario_correo': usuariocorreo,
                'apellidos': row['Apellido(s)'],
            }

            # Sometimes Nombre in the csv of aulavirtual is not well displayed
            if '\ufeffNombre' in row:
                students[usuariocorreo]['nombre'] = row['\ufeffNombre']
                students[usuariocorreo]['nombre_apellidos'] = "{} {}".format(row['\ufeffNombre'], row['Apellido(s)']).lower()
                students[usuariocorreo]['nombre_apellidos_formatted'] = remove_tildes(
                    students[usuariocorreo]['nombre_apellidos'])
            elif 'Nombre' in row:
                students[usuariocorreo]['nombre'] = row['Nombre']
                students[usuariocorreo]['nombre_apellidos'] = "{} {}".format(row['Nombre'], row['Apellido(s)']).lower()
                students[usuariocorreo]['nombre_apellidos_formatted'] = remove_tildes(
                    students[usuariocorreo]['nombre_apellidos'])

            # Let's get the lab usernames for all students (if not already available)
            if 'Usuario Lab' in row:
                students[usuariocorreo]['usuario_lab'] = row["Usuario Lab"]
            else:
                usuariolab = get_lab_username(students[usuariocorreo])
                students[usuariocorreo]['usuario_lab'] = usuariolab
                file_modified = True

            if 'Usuario Gitlab' in row and row['Usuario Gitlab']:
                students[usuariocorreo]['usuario_gitlab'] = row["Usuario Gitlab"]
                students[usuariocorreo]['foundingitlab'] = True

    if file_modified:
        print("Students file modified, writting new version")
        export_csv_enriched(filename_out, students)
    return students


def parse_args():
    parser = argparse.ArgumentParser(description='Evaluate practices.')
    parser.add_argument('--studentsin', required=True,
                        help="name of csv file with students, exported from Moodle")
    parser.add_argument('--studentsout', required=True,
                        help="name of csv file with students and lab ids, produced by this program")
    args = parser.parse_args()
    return(args)


if __name__ == "__main__":
    args = parse_args()
    students = process(args.studentsin, args.studentsout)
